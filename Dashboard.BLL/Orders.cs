﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;

namespace Dashboard.BLL
{
    public class Orders : BaseData, IReport
    {
        #region Orders

        public static Order GetOrderByOrderId(int orderId)
        {
            Order pageItem = null;
            string key = "Orders_GetOrderByOrderId_" + orderId;

            if (Cache[key] != null)
            {
                pageItem = (Order)Cache[key];
            }
            else
            {
                pageItem = DataAccess.Orders.GetOrderByOrderId(orderId);
                CacheData(key, pageItem);
            }
            return pageItem;
        }

        /// <summary>
        /// Retrieves Orders collection
        /// </summary>
        public static List<Order> GetPagedOrders(int startRowIndex, int maximumRows)
        {
            List<Order> tickets = null;
            string key = "Orders_GetPagedOrders_" + startRowIndex + "_" + maximumRows;

            if (Cache[key] != null)
            {
                tickets = (List<Order>)Cache[key];
            }
            else
            {
                tickets = DataAccess.Orders.GetPagedOrders(GetPageIndex(startRowIndex, maximumRows), maximumRows);
                CacheData(key, tickets);
            }
            return tickets;
        }

        /// <summary>
        /// Counts Order items
        /// </summary>
        public static int CountOrders()
        {
            int itemCount = 0;
            string key = "Orders_CountOrders";

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.Orders.CountOrders();
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int InsertOrder(Order order)
        {
            int returnValue = DataAccess.Orders.InsertOrder(order);
            RemoveFromCache("Orders_");
            return returnValue;
        }

        /// <summary>
        /// Deletes Order with underlying OrderItems By OrderId 
        /// </summary>
        public static bool DeleteOrderByOrderId(int orderId)
        {
            bool ret = DataAccess.Orders.DeleteOrderByOrderId(orderId);
            RemoveFromCache("Orders_");
            return ret;
        }

        #endregion

        #region OrderItems

        public static List<Order> GetPagedOrderItems(int startRowIndex, int maximumRows)
        {
            List<Order> tickets = null;
            string key = "Orders_GetPagedOrderItems_" + startRowIndex + "_" + maximumRows;

            if (Cache[key] != null)
            {
                tickets = (List<Order>)Cache[key];
            }
            else
            {
                tickets = DataAccess.Orders.GetPagedOrderItems(GetPageIndex(startRowIndex, maximumRows), maximumRows);
                CacheData(key, tickets);
            }
            return tickets;
        }

        public static List<Order> GroupOrderItemsOnDay(int providerId, int year)
        {
            List<Order> tickets = null;
            string key = "Orders_GroupOrderItemsOnDay_" + providerId + "_" + year;

            if (Cache[key] != null)
            {
                tickets = (List<Order>)Cache[key];
            }
            else
            {
                tickets = DataAccess.Orders.GroupOrderItemsOnDay(providerId, year);
                CacheData(key, tickets);
            }
            return tickets;
        }
        
        /// <summary>
        /// Retrieves Orders collection
        /// </summary>
        public static List<Order> GetOrderItemsByOrderId(int orderId)
        {
            List<Order> tickets = null;
            string key = "Orders_GetOrderItemsByOrderId_" + orderId;

            if (Cache[key] != null)
            {
                tickets = (List<Order>)Cache[key];
            }
            else
            {
                tickets = DataAccess.Orders.GetOrderItemsByOrderId(orderId);
                CacheData(key, tickets);
            }
            return tickets;
        }

        public static Order GetOrderItemByTicketId(int ticketId)
        {
            Order order = null;
            string key = "Orders_GetOrderItemByTicketId_" + ticketId;

            if (Cache[key] != null)
            {
                order = (Order)Cache[key];
            }
            else
            {
                order = DataAccess.Orders.GetOrderItemByTicketId(ticketId);
                CacheData(key, order);
            }
            return order;
        }

        /// <summary>
        /// Counts Order Items
        /// </summary>
        public static int CountOrderItems(int orderId)
        {
            int itemCount = 0;
            string key = "Orders_CountOrderItems_" + orderId;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.Orders.CountOrderItems(orderId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int InsertOrderItem(Order order)
        {
            int returnValue = DataAccess.Orders.InsertOrderItem(order);
            RemoveFromCache("Orders_");
            return returnValue;
        }

        /// <summary>
        /// Deletes OrderItems By TicketId
        /// </summary>
        public static bool DeleteOrderItemsByTicketId(int ticketId)
        {
            bool ret = DataAccess.Orders.DeleteOrderItemsByTicketId(ticketId);
            RemoveFromCache("Orders_");
            return ret;
        } 
        
        #endregion
    }
}
