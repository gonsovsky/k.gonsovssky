﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;

namespace Dashboard.BLL
{
    public class EventDates : BaseData, IReport
    {
        /// <summary>
        /// Gets EventDates By ActionId
        /// </summary>
        /// <param name="actionId">Optional: use actionId = 0 to retrieve all the EventDates</param>
        /// <returns></returns>
        public static List<EventDate> GetEventDatesByActionId(int actionId)
        {
            List<EventDate> showActions = null;
            string key = "EventDates_GetEventDatesByActionId_" + actionId;

            if (Cache[key] != null)
            {
                showActions = (List<EventDate>)Cache[key];
            }
            else
            {
                showActions = DataAccess.EventDates.GetEventDatesByActionId(actionId);
                CacheData(key, showActions);
            }
            return showActions;
        }

        public static EventDate GetEventDateById(int eventId)
        {
            EventDate eventDate = null;
            string key = "EventDates_GetEventDateById_" + eventId;

            if (Cache[key] != null)
            {
                eventDate = (EventDate)Cache[key];
            }
            else
            {
                eventDate = DataAccess.EventDates.GetEventDateById(eventId);
                CacheData(key, eventDate);
            }
            return eventDate;
        }

        /// <summary>
        /// Groups sortable EventDates on months, events count, total price for tickets 
        /// </summary>
        /// <param name="actionId">ShowAction Id or zero</param>
        /// <param name="statusId">StatusId Id or zero</param>
        /// <param name="year">Particular year - obligatory</param>
        /// <returns>DataTable</returns>
        public static DataTable GetSortableEventDatesOnMonths(int actionId, int statusId, int year)
        {
            DataTable dt;
            string key = "EventDates_GetSortableEventDatesOnMonths_" + actionId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                dt = (DataTable)Cache[key];
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add(new DataColumn("Id", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Name", typeof(String)));
                dt.Columns.Add(new DataColumn("Count", typeof(Int32)));
                dt.Columns.Add(new DataColumn("TotalCount", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Price", typeof(Decimal)));
                dt.Columns.Add(new DataColumn("AveragePrice", typeof(Decimal)));

                List<Group> groups = GroupEventDatesOnMonths(actionId, statusId, year);

                foreach (Group g in groups)
                {
                    DataRow dr = dt.NewRow();
                    dr["Id"] = g.Id;
                    dr["Name"] = g.Name;
                    dr["Count"] = g.Count;
                    dr["TotalCount"] = g.TotalCount;
                    dr["Price"] = g.Price;
                    dr["AveragePrice"] = g.AveragePrice;
                    dt.Rows.Add(dr);
                }

                CacheData(key, dt);
            }
            return dt;
        }

        /// <summary>
        /// Groups EventDates on months, events count, total price for tickets 
        /// </summary>
        /// <param name="actionId">ShowAction Id or zero</param>
        /// <param name="statusId">StatusId Id or zero</param>
        /// <param name="year">Particular year - obligatory</param>
        /// <returns>List Group as result</returns>
        public static List<Group> GroupEventDatesOnMonths(int actionId, int statusId, int year)
        {
            if (year == 0)
            {
                year = DateTime.Now.Year;
            }
            List<Group> groups = new List<Group>();
            string key = "EventDates_GroupEventDatesOnMonths_" + actionId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                groups = DataAccess.EventDates.GroupEventDatesOnMonths(actionId, statusId, year);

                foreach (Group g in groups)
                {
                    if (g.Price > 0)
                    {
                        g.PriceResult = decimal.Round((g.Price / 1000000), 2, MidpointRounding.AwayFromZero);
                    }
                }

                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Retrieves EventDates collection
        /// </summary>
        public static List<EventDate> GetPagedEventDates(int startRowIndex, int maximumRows, int memberId, int providerId, int actionId, int year)
        {
            List<EventDate> tickets = null;
            string key = "EventDates_GetPagedEventDates_" + startRowIndex + "_" + maximumRows + "_" + memberId + "_" + providerId + "_" + actionId + "_" + year;

            if (Cache[key] != null)
            {
                tickets = (List<EventDate>)Cache[key];
            }
            else
            {
                tickets = DataAccess.EventDates.GetPagedEventDates(GetPageIndex(startRowIndex, maximumRows), maximumRows, memberId, providerId, actionId, year);
                CacheData(key, tickets);
            }
            return tickets;
        }

        /// <summary>
        /// Gets aggregated EventDates Stats 
        /// </summary>
        public static DataTable GetEventDateSortableStats(int actionId, int statusId, int year)
        {
            DataTable dt;

            string key = "EventDates_GetEventDateSortableStats_" + actionId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                dt = (DataTable)Cache[key];
            }
            else
            {
                List<EventDate> providers = GetEventDateStats(actionId, statusId, year);
                dt = new DataTable();
                dt.Columns.Add(new DataColumn("Id", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Name", typeof(String)));
                dt.Columns.Add(new DataColumn("ActionDate", typeof(DateTime)));
                dt.Columns.Add(new DataColumn("TotalCount", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Count", typeof(Int32)));
                dt.Columns.Add(new DataColumn("CountResult", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Percent", typeof(Int32)));
                dt.Columns.Add(new DataColumn("TotalPrice", typeof(Decimal)));
                dt.Columns.Add(new DataColumn("Price", typeof(Int32)));
                dt.Columns.Add(new DataColumn("PriceResult", typeof(Int32)));

                foreach (EventDate p in providers)
                {
                    DataRow dr = dt.NewRow();
                    dr["Id"] = p.Id;
                    dr["Name"] = p.Name;
                    dr["ActionDate"] = p.ActionDate;
                    dr["TotalCount"] = p.TotalCount;
                    dr["Count"] = p.Count;
                    dr["CountResult"] = p.CountResult;
                    dr["Percent"] = p.Percent;
                    dr["TotalPrice"] = p.TotalPrice;
                    dr["Price"] = p.Price;
                    dr["PriceResult"] = p.PriceResult;
                    dt.Rows.Add(dr);
                }
                CacheData(key, dt);
            }

            return dt;
        }

        /// <summary>
        /// Gets aggregated EventDates Stats 
        /// </summary>
        public static List<EventDate> GetEventDateStats(int actionId, int statusId, int year)
        {
            List<EventDate> providers = new List<EventDate>();
            string key = "EventDates_GetEventDateStats_" + actionId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                providers = (List<EventDate>)Cache[key];
            }
            else
            {
                List<EventDate> issued = GroupEventDatesOnTickets(actionId, 0, year);
                List<EventDate> sold = GroupEventDatesOnTickets(actionId, statusId, year);

                foreach (EventDate i in issued)
                {
                    foreach (EventDate s in sold)
                    {
                        if (i.Id == s.Id)
                        {
                            i.CountResult = i.Count - s.Count;// Непродано, билетов
                            int percent = i.Count / 100;// Заполняемость зала
                            if (percent > 0 && i.Count > 0)
                            {
                                i.Percent = s.Count / percent;
                            }
                            i.Count = s.Count;// Продано, билетов
                            i.PriceResult = i.Price - s.Price;// Непродано, рублей
                            i.Price = s.Price;// Продано, рублей
                            providers.Add(i);
                            break;
                        }
                    }
                }

                CacheData(key, providers);
            }
            return providers;
        }

        /// <summary>
        /// Groups EventDates On Tickets 
        /// </summary>
        public static List<EventDate> GroupEventDatesOnTickets(int actionId, int statusId, int year)
        {
            List<EventDate> tickets = null;
            string key = "EventDates_GroupEventDatesOnTickets_" + actionId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                tickets = (List<EventDate>)Cache[key];
            }
            else
            {
                tickets = DataAccess.EventDates.GroupEventDatesOnTickets(actionId, statusId, year);
                CacheData(key, tickets);
            }
            return tickets;
        }

        /// <summary>
        /// Retrieves EventDates list with ticket count.
        /// Note, the result is dependant on member's provider list.
        /// Groups EventDates On Tickets By MemberId.
        /// </summary>
        /// <param name="actionId">Optional: use ActionId = 0 to retrieve all the EventDates</param>
        /// <param name="memberId">MemberId is obligatory</param>
        /// <param name="statusId">StatusId is obligatory</param>
        /// <param name="year">Year is obligatory</param>
        /// <param name="month">Month is obligatory</param>
        /// <returns>Returns grouped list of EventDateId, Name, Ticket count</returns>
        public static List<Group> GroupEventDatesOnTicketsByMemberId(int actionId, int memberId, int statusId, int year, int month)
        {
            List<Group> groups = new List<Group>();
            string key = "EventDates_GroupEventDatesOnTicketsByMemberId_" + actionId + "_" + memberId + "_" + statusId + "_" + year + "_" + month;

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                groups = DataAccess.EventDates.GroupEventDatesOnTicketsByMemberId(actionId, memberId, statusId, year, month);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Counts EventDates by providerId, actionId, year
        /// </summary>
        public static int CountEventDates(int memberId, int providerId, int actionId, int year)
        {
            int itemCount = 0;
            string key = "EventDates_CountEventDates_" + memberId + "_" + providerId + "_" + actionId + "_" + year;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.EventDates.CountEventDates(memberId, providerId, actionId, year);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int CountEventDatesByActionId(int actionId)
        {
            int itemCount = 0;
            string key = "EventDates_CountEventDatesByActionId_" + actionId;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.EventDates.CountEventDatesByActionId(actionId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }


        /// <summary>
        /// Gets Available Months
        /// </summary>
        public static List<Group> GetAvailableMonths(int year)
        {
            List<Group> months = new List<Group>();
            string key = "EventDates_GetAvailableMonths_" + year;

            if (Cache[key] != null)
            {
                months = (List<Group>)Cache[key];
            }
            else
            {
                months = DataAccess.EventDates.GetAvailableMonths(year);
                CacheData(key, months);
            }
            return months;
        }

    }
}
