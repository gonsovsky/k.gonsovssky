﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;

namespace Dashboard.BLL
{
    public class Logs : BaseData, IReport
    {
        public static List<Log> GetPagedLogs(int startRowIndex, int maximumRows, int memberId)
        {
            List<Log> logs = null;
            string key = "Logs_GetPagedLogs_" + startRowIndex + "_" + maximumRows + "_" + memberId;

            if (Cache[key] != null)
            {
                logs = (List<Log>)Cache[key];
            }
            else
            {
                logs = DataAccess.Logs.GetPagedLogs(GetPageIndex(startRowIndex, maximumRows), maximumRows, memberId);
                CacheData(key, logs);
            }
            return logs;
        }

        public static int CountLogs(int memberId)
        {
            int itemCount = 0;
            string key = "Logs_CountLogs_" + memberId;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.Logs.CountLogs(memberId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }
        
        public static int InsertLog(Log log)
        {
            int returnValue = DataAccess.Logs.InsertLog(log);
            RemoveFromCache("Logs_");
            return returnValue;
        }
        
        public static DateTime GetUpdatedTime()
        {
            return DataAccess.Logs.GetUpdatedTime();
        }
    }
}
