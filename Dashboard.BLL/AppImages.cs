﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Dashboard.Common;

namespace Dashboard.BLL
{
    public class AppImages
    {
        public static MemoryStream CreateTicket(Image barcodeImg)
        {
            string ticket = HttpContext.Current.Server.MapPath(WebConfigurationManager.AppSettings["Ticket"]);
            using (Bitmap bmImage = new Bitmap(ticket))
            {
                bmImage.SetResolution(72, 72);
                using (Graphics g = Graphics.FromImage(bmImage))
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.DrawImage(barcodeImg, new Rectangle(680, 26, barcodeImg.Width, barcodeImg.Height));
                    MemoryStream mm = new MemoryStream();
                    bmImage.Save(mm, ImageFormat.Png);
                    return mm;
                }
            }
        }

        public static MemoryStream EditTicket(Image barcodeImg, int formId, Stage stage)
        {
            Form template = Forms.GetFormByFormId(formId);
            List<Form> fields = Forms.GetFormFieldsByFormId(formId);
            IDictionary<string, object> vals = stage.AsDictionary();
            string image = HttpContext.Current.Server.MapPath(template.ImageUrl);
            using (Bitmap bmImage = new Bitmap(image))
            {
                bmImage.SetResolution(72, 72);
                using (Graphics g = Graphics.FromImage(bmImage))
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.DrawImage(barcodeImg, new Rectangle(680, 26, barcodeImg.Width, barcodeImg.Height));

                    foreach (Form form in fields)
                    {
                        foreach (KeyValuePair<string, object> val in vals)
                        {
                            if (form.Value == val.Key)
                            {
                                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                                Font font = new Font(form.FontFamily, form.FontSize, form.FontStyle);
                                SolidBrush brush = new SolidBrush(form.FontColor);
                                StringFormat stringFormat = new StringFormat();
                                stringFormat.FormatFlags = form.StringFormat;
                                PointF pointHead = new PointF(form.XPos, form.YPos);
                                string text = string.Format("{0} {1}", form.Name, val.Value);
                                g.DrawString(text, font, brush, pointHead, stringFormat);
                            }
                        }
                    }


                    MemoryStream mm = new MemoryStream();
                    bmImage.Save(mm, ImageFormat.Png);
                    return mm;
                }
            }
        }

        public static void WriteToFile(string filePath, byte[] bytes)
        {
            if (filePath != null)
            {
                using (FileStream full = File.Open(filePath, FileMode.Create))
                {
                    full.Write(bytes, 0, bytes.Length);
                    full.Flush();
                }
            }
        }

        public static void SaveImage(string filePath, Image image)
        {
            if (filePath != null)
            {
                ImageCodecInfo jpgInfo = ImageCodecInfo.GetImageEncoders().Where(codecInfo => codecInfo.MimeType == "image/jpeg").First();

                using (EncoderParameters encParams = new EncoderParameters(1))
                {
                    //quality should be in the range [0..100] .. 100 for max, 0 for min (0 best compression)
                    encParams.Param[0] = new EncoderParameter(Encoder.Quality, (long)100);
                    image.Save(filePath, jpgInfo, encParams);
                }
            }
        }

        /// <summary>
        /// Crops an image from center with desired width and height  
        /// </summary>
        /// <param name="image">System.Drawing.Image object</param>
        /// <param name="maxWidth">Max target image width</param>
        /// <param name="maxHeight">Max target image height</param>
        /// <param name="imageFormat">System.Drawing.ImageFormat of expected image</param>
        /// <returns>Byte array of saved image from MemoryStream</returns>
        public static byte[] CropImage(Image image, int maxWidth, int maxHeight, ImageFormat imageFormat)
        {
            using (Bitmap bitmap = new Bitmap(maxWidth, maxHeight, PixelFormat.Format32bppArgb))
            {
                int left = 0;
                int top = 0;
                int srcWidth = maxWidth;
                int srcHeight = maxHeight;

                double croppedHeightToWidth = (double)maxHeight / maxWidth;
                double croppedWidthToHeight = (double)maxWidth / maxHeight;

                if (image.Width > image.Height)
                {
                    srcWidth = (int)(Math.Round(image.Height * croppedWidthToHeight));
                    if (srcWidth < image.Width)
                    {
                        srcHeight = image.Height;
                        left = (image.Width - srcWidth) / 2;
                    }
                    else
                    {
                        srcHeight = (int)Math.Round(image.Height * ((double)image.Width / srcWidth));
                        srcWidth = image.Width;
                        top = (image.Height - srcHeight) / 2;
                    }
                }
                else
                {
                    srcHeight = (int)(Math.Round(image.Width * croppedHeightToWidth));
                    if (srcHeight < image.Height)
                    {
                        srcWidth = image.Width;
                        top = (image.Height - srcHeight) / 2;
                    }
                    else
                    {
                        srcWidth = (int)Math.Round(image.Width * ((double)image.Height / srcHeight));
                        srcHeight = image.Height;
                        left = (image.Width - srcWidth) / 2;
                    }
                }

                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.CompositingQuality = CompositingQuality.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.DrawImage(image, new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                        new Rectangle(left, top, srcWidth, srcHeight), GraphicsUnit.Pixel);
                    MemoryStream mm = new MemoryStream();
                    bitmap.Save(mm, imageFormat);
                    return mm.GetBuffer();
                }
            }
        }

        public static byte[] ResizeImage(Image imageFile, int targetW, int targetH)
        {
            using (Bitmap bmImage = new Bitmap(targetW, targetH, PixelFormat.Format32bppArgb))
            {
                bmImage.SetResolution(72, 72);
                using (Graphics g = Graphics.FromImage(bmImage))
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.Clear(Color.Transparent);
                    RectangleF sr = new RectangleF(0, 0, imageFile.Width, imageFile.Height);
                    Rectangle dr = new Rectangle(0, 0, targetW, targetH);
                    g.DrawImage(imageFile, dr, sr, GraphicsUnit.Pixel);
                    MemoryStream mm = new MemoryStream();
                    bmImage.Save(mm, ImageFormat.Jpeg);
                    return mm.GetBuffer();
                }
            }

        }

        public static void DeleteUserImage(int memberId)
        {
            string filePath = HttpContext.Current.Server.MapPath("../Images/Users/" + memberId + ".jpg");
            if (filePath != null)
            {
                if (File.Exists(filePath)) File.Delete(filePath);
            }
        }
    }
}