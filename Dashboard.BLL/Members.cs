﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Profile;
using System.Web.Security;
using Dashboard.Common;
using Dashboard.Data.Data;

namespace Dashboard.BLL
{
    public class Members : BaseData, IReport
    {
        #region Roles

        public static List<Member> GetRoles()
        {
            List<Member> members = null;
            string key = "Members_GetRoles_";

            if (Cache[key] != null)
            {
                members = (List<Member>)Cache[key];
            }
            else
            {
                members = DataAccess.Members.GetRoles();
                CacheData(key, members);
            }
            return members;
        }

        public static Member GetRoleByName(string roleName)
        {
            Member member = null;
            string key = "Members_GetRoleByName_" + roleName;

            if (Cache[key] != null)
            {
                member = (Member)Cache[key];
            }
            else
            {
                member = DataAccess.Members.GetRoleByName(roleName);
                CacheData(key, member);
            }
            return member;
        }

        public static Member GetRoleById(int roleId)
        {
            Member member = null;
            string key = "Members_GetRoleById_" + roleId;

            if (Cache[key] != null)
            {
                member = (Member)Cache[key];
            }
            else
            {
                member = DataAccess.Members.GetRoleById(roleId);
                CacheData(key, member);
            }
            return member;
        }

        #endregion

        #region Members

        public static Member GetMemberByMemberId(int memberId)
        {
            Member member = null;
            string key = "Members_GetMemberByMemberId_" + memberId;

            if (Cache[key] != null)
            {
                member = (Member)Cache[key];
            }
            else
            {
                member = DataAccess.Members.GetMemberByMemberId(memberId);
                CacheData(key, member);
            }
            return member;
        }

        /// <summary>
        /// Retrieves an ASP Member by login.
        /// ASP Member must be IsApproved and not IsLockedOut
        /// Used for remote authentication
        /// </summary>
        /// <param name="login">Member's Login, UserName, or Email </param>
        /// <returns>Member object</returns>
        public static Member GetMemberByLogin(string login)
        {
            Member member = null;
            string key = "Members_GetMemberByLogin_" + login;

            if (Cache[key] != null)
            {
                member = (Member)Cache[key];
            }
            else
            {
                member = DataAccess.Members.GetMemberByLogin(login);
                CacheData(key, member);
            }
            return member;
        }

        public static Member GetParentMemberByMemberId(int memberId)
        {
            Member member = null;
            string key = "Members_GetParentMemberByMemberId_" + memberId;

            if (Cache[key] != null)
            {
                member = (Member)Cache[key];
            }
            else
            {
                member = DataAccess.Members.GetParentMemberByMemberId(memberId);
                CacheData(key, member);
            }
            return member;
        }

        /// <summary>
        /// Groups Members On Months By Year
        /// </summary>
        public static List<Group> GroupMembersOnMonthsByYear(int year)
        {
            List<Group> months = new List<Group>();
            string key = "Members_GroupMembersOnMonthsByYear_" + year;

            if (Cache[key] != null)
            {
                months = (List<Group>)Cache[key];
            }
            else
            {
                months = DataAccess.Members.GroupMembersOnMonthsByYear(year);
                CacheData(key, months);
            }
            return months;
        }

        /// <summary>
        /// Get paged member list
        /// </summary>
        /// <param name="startRowIndex">Page number</param>
        /// <param name="maximumRows">Page rows</param>
        /// <param name="roleId">RoleId or zero if all roles selected</param>
        /// <param name="parentId">ParentId or zero if all users selected</param>
        /// <returns>Member List</returns>
        public static List<Member> GetPagedMembers(int startRowIndex, int maximumRows, int roleId, int parentId)
        {
            List<Member> members = null;
            string key = "Members_GetPagedMembers_" + startRowIndex + "_" + maximumRows + "_" + roleId + "_" + parentId;

            if (Cache[key] != null)
            {
                members = (List<Member>)Cache[key];
            }
            else
            {
                members = DataAccess.Members.GetPagedMembers(GetPageIndex(startRowIndex, maximumRows), maximumRows, roleId, parentId);
                CacheData(key, members);
            }
            return members;
        }

        public static List<Member> GetChildMembers(int parentId)
        {
            List<Member> members = null;
            string key = "Members_GetChildMembers_" + parentId;

            if (Cache[key] != null)
            {
                members = (List<Member>)Cache[key];
            }
            else
            {
                members = DataAccess.Members.GetChildMembers(parentId);
                CacheData(key, members);
            }
            return members;
        }

        public static int CountChildMembers(int parentId)
        {
            int itemCount = 0;
            string key = "Members_CountChildMembers_" + parentId;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.Members.CountChildMembers(parentId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        /// <summary>
        /// Groups Members On Tickets 
        /// </summary>
        public static List<Member> GroupMembersOnTickets(int providerId, int actionId, int eventId, int statusId)
        {
            List<Member> members = null;
            string key = "Members_GroupMembersOnTickets_" + providerId + "_" + actionId + "_" + eventId + "_" + statusId;

            if (Cache[key] != null)
            {
                members = (List<Member>)Cache[key];
            }
            else
            {
                members = DataAccess.Members.GroupMembersOnTickets(providerId, actionId, eventId, statusId);
                CacheData(key, members);
            }
            return members;
        }

        /// <summary>
        /// Counts Members by roleId or/and parentId
        /// </summary>
        public static int CountMembers(int roleId, int parentId)
        {
            int itemCount = 0;
            string key = "Members_CountMembers_" + roleId + "_" + parentId;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.Members.CountMembers(roleId, parentId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        /// <summary>
        /// Retrieves an ASP Member by login.
        /// ASP Member must be IsApproved and not IsLockedOut
        /// Used for remote authentication
        /// </summary>
        /// <param name="login">Member's Login, UserName, or Email </param>
        /// <param name="password">Member's Password</param>
        /// <returns>Member object</returns>
        public static Member Login(string login, string password)
        {
            Member member = new Member();
            if (Membership.ValidateUser(login, password))
            {
                member = GetMemberByLogin(login);
            }

            return member;
        }

        public static bool UpdateMember(Member member)
        {
            bool ret = DataAccess.Members.UpdateMember(member);
            RemoveFromCache("Members_");
            return ret;
        } 

        public static int InsertMember(Member member)
        {
            int ret = DataAccess.Members.InsertMember(member);
            RemoveFromCache("Members_");
            return ret;
        }

        public static bool DeleteMember(Member member)
        {
            Membership.DeleteUser(member.Email);
            AppImages.DeleteUserImage(member.Id);
            return DeleteMemberByMemberId(member.Id);
        }

        public static bool DeleteMemberByMemberId(int memberId)
        {
            bool ret = DataAccess.Members.DeleteMemberByMemberId(memberId);
            RemoveFromCache("Members_");
            return ret;
        }

        #endregion

        #region Member Providers

        public static List<Member> GetMemberProviders(int memberId)
        {
            List<Member> members = null;
            string key = "Members_GetMemberProviders_" + memberId;

            if (Cache[key] != null)
            {
                members = (List<Member>)Cache[key];
            }
            else
            {
                members = DataAccess.Members.GetMemberProviders(memberId);
                CacheData(key, members);
            }
            return members;
        }

        public static Member GetMemberProviderByMemberId(int memberId)
        {
            Member member = null;
            string key = "Members_GetMemberProviderByMemberId_" + memberId;

            if (Cache[key] != null)
            {
                member = (Member)Cache[key];
            }
            else
            {
                member = DataAccess.Members.GetMemberProviderByMemberId(memberId);
                CacheData(key, member);
            }
            return member;
        }

        public static int CountMemberProviders(int memberId)
        {
            int itemCount = 0;
            string key = "Members_CountMemberProviders_" + memberId;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.Members.CountMemberProviders(memberId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int InsertMemberProvider(Member member)
        {
            int ret = DataAccess.Members.InsertMemberProvider(member);
            RemoveFromCache("Members_");
            RemoveFromCache("Stages_");
            return ret;
        }

        public static bool DeleteMemberProviderById(int id)
        {
            bool ret = DataAccess.Members.DeleteMemberProviderById(id);
            RemoveFromCache("Members_");
            RemoveFromCache("Stages_");
            return ret;
        }

        #endregion
    }
}