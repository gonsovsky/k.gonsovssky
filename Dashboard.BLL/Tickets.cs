﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;

namespace Dashboard.BLL
{
    public class Tickets : BaseData, IReport
    {
        /// <summary>
        /// Gets Tickets By EventId
        /// </summary>
        public static List<Ticket> GetTicketsByEventId(int eventId)
        {
            List<Ticket> tickets = null;
            string key = "Tickets_GetTicketsByEventId_" + eventId;

            if (Cache[key] != null)
            {
                tickets = (List<Ticket>)Cache[key];
            }
            else
            {
                tickets = DataAccess.Tickets.GetTicketsByEventId(eventId);
                CacheData(key, tickets);
            }
            return tickets;
        }

        public static Ticket GetTicketByTicketId(int ticketId)
        {
            Ticket pageItem = null;
            string key = "Tickets_GetTicketByTicketId_" + ticketId;

            if (Cache[key] != null)
            {
                pageItem = (Ticket)Cache[key];
            }
            else
            {
                pageItem = DataAccess.Tickets.GetTicketByTicketId(ticketId);
                CacheData(key, pageItem);
            }
            return pageItem;
        }

        /// <summary>
        /// Groups Tickets On Months By ActionId and By Year
        /// </summary>
        [RestAttribute(Description = "Groups Tickets On Months By ActionId and Year")]
        public static List<Group> GroupTicketsOnMonthsByActionId(int actionId, int year)
        {
            List<Group> months = new List<Group>();
            string key = "Tickets_GroupTicketsOnMonthsByActionId_" + actionId + "_" + year;

            if (Cache[key] != null)
            {
                months = (List<Group>)Cache[key];
            }
            else
            {
                months = DataAccess.Tickets.GroupTicketsOnMonthsByActionId(actionId, year);
                CacheData(key, months);
            }
            return months;
        }

        /// <summary>
        /// Groups tickets average price on days
        /// </summary>
        public static List<Group> GroupTicketsOnDatesByProviderId(int providerId, int year)
        {
            List<Group> dates = new List<Group>();
            string key = "Tickets_GroupTicketsOnDatesByProviderId_" + providerId + "_" + year;

            if (Cache[key] != null)
            {
                dates = (List<Group>)Cache[key];
            }
            else
            {
                dates = DataAccess.Tickets.GroupTicketsOnDatesByProviderId(providerId, year);
                CacheData(key, dates);
            }
            return dates;
        }

        /// <summary>
        /// Groups sortable tickets on months by providerId, actionId, statusId, and year
        /// </summary>
        /// <param name="providerId">Provider Id or zero</param>
        /// <param name="actionId">Action Id or zero</param>
        /// <param name="eventId">EventId or zero</param>
        /// <param name="statusId">Status of tickets</param>
        /// <param name="year">Particular year is obligatory</param>
        /// <returns>List Group  as result</returns>
        public static DataTable GetSortableTicketsOnMonths(int providerId, int actionId, int eventId, int statusId, int year)
        {
            DataTable dt;
            string key = "Tickets_GetSortableTicketsOnMonths_" + providerId + "_" + actionId + "_" + eventId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                dt = (DataTable)Cache[key];
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add(new DataColumn("Id", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Name", typeof(String)));
                dt.Columns.Add(new DataColumn("TotalActions", typeof(Int32)));
                dt.Columns.Add(new DataColumn("TotalEvents", typeof(Int32)));
                dt.Columns.Add(new DataColumn("TotalTickets", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Price", typeof(Decimal)));
                dt.Columns.Add(new DataColumn("AveragePrice", typeof(Decimal)));

                List<Group> groups = GroupTicketsOnMonths(providerId, actionId, eventId, statusId, year);

                foreach (Group g in groups)
                {
                    DataRow dr = dt.NewRow();
                    dr["Id"] = g.Id;
                    dr["Name"] = g.Name;
                    dr["TotalActions"] = g.Count;
                    dr["TotalEvents"] = g.TotalCount;
                    dr["TotalTickets"] = g.CountResult;
                    dr["Price"] = g.Price;
                    dr["AveragePrice"] = g.AveragePrice;
                    dt.Rows.Add(dr);
                }

                CacheData(key, dt);
            }
            return dt;
        }

        /// <summary>
        /// Groups tickets on months by providerId, actionId, eventId, statusId, and year
        /// </summary>
        /// <param name="providerId">Provider Id or zero</param>
        /// <param name="actionId">Action Id or zero</param>
        /// <param name="eventId">EventId or zero</param>
        /// <param name="statusId">Status of tickets</param>
        /// <param name="year">Particular year is obligatory</param>
        /// <returns>List Group  as result</returns>
        public static List<Group> GroupTicketsOnMonths(int providerId, int actionId, int eventId, int statusId, int year)
        {
            if (year == 0)
            {
                year = DateTime.Now.Year;
            }

            List<Group> groups = new List<Group>();
            string key = "Tickets_GroupTicketsOnMonths_" + providerId + "_" + actionId + "_" + eventId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                groups = DataAccess.Tickets.GroupTicketsOnMonths(providerId, actionId, eventId, statusId, year, DateTime.MinValue, DateTime.MinValue);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Groups sortable tickets on days by providerId, actionId, statusId, and year
        /// </summary>
        /// <param name="providerId">Provider Id or zero</param>
        /// <param name="actionId">Action Id or zero</param>
        /// <param name="eventId">EventId or zero</param>
        /// <param name="statusId">Status of tickets</param>
        /// <param name="year">Particular year is obligatory</param>
        /// <returns>List Group  as result</returns>
        [RestAttribute(Description = "Groups Tickets On Days")]
        public static DataTable GetSortableTicketsOnDays(int providerId, int actionId, int eventId, int statusId, int year)
        {
            DataTable dt;
            string key = "Tickets_GetSortableTicketsOnDays_" + providerId + "_" + actionId + "_" + eventId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                dt = (DataTable)Cache[key];
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add(new DataColumn("CreatedDate", typeof(DateTime)));
                dt.Columns.Add(new DataColumn("TotalActions", typeof(Int32)));
                dt.Columns.Add(new DataColumn("TotalEvents", typeof(Int32)));
                dt.Columns.Add(new DataColumn("TotalTickets", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Price", typeof(Decimal)));
                dt.Columns.Add(new DataColumn("DiscretPrice", typeof(Decimal)));
                dt.Columns.Add(new DataColumn("AveragePrice", typeof(Decimal)));

                List<Group> groups = GroupTicketsOnDays(providerId, actionId, eventId, statusId, year);

                foreach (Group g in groups)
                {
                    DataRow dr = dt.NewRow();
                    dr["CreatedDate"] = g.CreatedDate;
                    dr["TotalActions"] = g.Count;
                    dr["TotalEvents"] = g.TotalCount;
                    dr["TotalTickets"] = g.CountResult;
                    dr["Price"] = g.Price;
                    dr["DiscretPrice"] = g.Price / 1000;
                    dr["AveragePrice"] = g.AveragePrice;
                    dt.Rows.Add(dr);
                }

                CacheData(key, dt);
            }
            return dt;
        }

        /// <summary>
        /// Groups tickets on days by providerId, actionId, eventId, statusId, and year
        /// </summary>
        /// <param name="providerId">Provider Id or zero</param>
        /// <param name="actionId">Action Id or zero</param>
        /// <param name="eventId">EventId or zero</param>
        /// <param name="statusId">Status of tickets</param>
        /// <param name="year">Particular year is obligatory</param>
        /// <returns>List Group  as result</returns>
        public static List<Group> GroupTicketsOnDays(int providerId, int actionId, int eventId, int statusId, int year)
        {
            List<Group> groups = new List<Group>();
            string key = "Tickets_GroupTicketsOnDays_" + providerId + "_" + actionId + "_" + eventId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                groups = DataAccess.Tickets.GroupTicketsOnDays(providerId, actionId, eventId, statusId, year, DateTime.MinValue, DateTime.MinValue);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Groups tickets on months by providerId, actionId, eventId, statusId, and year.
        /// If StartDate and EndDate defined the selection only depends on a period.
        /// </summary>
        /// <param name="providerId">Provider Id or zero</param>
        /// <param name="actionId">Action Id or zero</param>
        /// <param name="eventId">EventId or zero</param>
        /// <param name="statusId">Status of tickets</param>
        /// <param name="startDate">StartDate of period otherwise MinValue</param>
        /// <param name="endDate">EndDate of period otherwise MinValue</param>
        /// <returns>List Group  as result</returns>
        private static List<Group> GroupTicketsOnMonths(int providerId, int actionId, int eventId, int statusId, DateTime startDate, DateTime endDate)
        {
            string start = string.Format("{0}_{1}_{2}", startDate.Day, startDate.Month, startDate.Year);
            string end = string.Format("{0}_{1}_{2}", startDate.Day, startDate.Month, startDate.Year);
            List<Group> groups = new List<Group>();
            string key = "Tickets_GroupTicketsOnMonths_" + providerId + "_" + actionId + "_" + statusId + "_" + start + "_" + end;

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                groups = DataAccess.Tickets.GroupTicketsOnMonths(providerId, actionId, eventId, statusId, 0, startDate, endDate);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Groups tickets on status by EventId
        /// </summary>
        /// <param name="eventId">EventId</param>
        /// <returns>List Groups</returns>
        public static List<Group> GroupTicketsOnStatusByEventId(int eventId)
        {
            List<Group> groups = new List<Group>();
            string key = "Tickets_GroupTicketsOnStatusByEventId_" + eventId;

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                groups = DataAccess.Tickets.GroupTicketsOnStatusByEventId(eventId);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Groups ticket sales by weeks
        /// </summary>
        /// <param name="providerId">Ticket provider</param>
        /// <param name="statusId">Status of the ticket</param>
        /// <param name="year">Year number</param>
        /// <returns></returns>
        public static List<Group> RunningTotals(int providerId, int statusId, int year)
        {
            List<Group> groups = new List<Group>();
            string key = "Tickets_RunningTotals_" + providerId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                groups = DataAccess.Tickets.RunningTotals(providerId, statusId, year);
                decimal result = 0;
                int count = 0;
                foreach (Group g in groups)
                {
                    result += g.Price;
                    g.PriceResult = result;
                    count += g.Count;
                    g.CountResult = count;
                }

                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Retrieves Tickets collection
        /// </summary>
        public static List<Ticket> GetPagedTickets(int startRowIndex, int maximumRows, int memberId, int eventId)
        {
            List<Ticket> tickets = null;
            string key = "Tickets_GetPagedTickets_" + startRowIndex + "_" + maximumRows + "_" + memberId + "_" + eventId;

            if (Cache[key] != null)
            {
                tickets = (List<Ticket>)Cache[key];
            }
            else
            {
                tickets = DataAccess.Tickets.GetPagedTickets(GetPageIndex(startRowIndex, maximumRows), maximumRows, memberId, eventId);
                CacheData(key, tickets);
            }
            return tickets;
        }

        /// <summary>
        /// Counts Ticket items
        /// </summary>
        public static int CountTickets(int memberId, int eventId)
        {
            int itemCount = 0;
            string key = "Tickets_CountTickets_" + memberId + "_" + eventId;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.Tickets.CountTickets(memberId, eventId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        /// <summary>
        /// Sums Sold Tickets on Count and Price
        /// </summary>
        public static Group SumSoldTickets(int providerId, int actionId, int statusId, int year)
        {
            Group group = new Group();
            string key = "Tickets_SumSoldTickets_" + providerId + "_" + actionId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                group = (Group)Cache[key];
            }
            else
            {
                group = DataAccess.Tickets.SumSoldTickets(providerId, actionId, statusId, year);
                CacheData(key, group);
            }
            return group;
        }

        /// <summary>
        /// Get Ticket Statuses
        /// </summary>
        public static List<Group> GetTicketStatus()
        {
            List<Group> groups = new List<Group>();
            string key = "Tickets_GetTicketStatus";

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                groups = DataAccess.Tickets.GetTicketStatus();
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Inserts a new Ticket
        /// </summary>
        public static int InsertTicket(Ticket ticket)
        {
            int returnValue = DataAccess.Tickets.InsertTicket(ticket);
            RemoveFromCache("Tickets_");
            RemoveFromCache("Stages_");
            return returnValue;
        }

        /// <summary>
        /// Updates a Ticket
        /// </summary>
        public static bool UpdateTicket(Ticket ticket)
        {
            bool ret = DataAccess.Tickets.UpdateTicket(ticket);
            RemoveFromCache("Tickets_");
            RemoveFromCache("Stages_");
            return ret;
        }

        /// <summary>
        /// Deletes Ticket By TicketId
        /// </summary>
        public static bool DeleteTicketByTicketId(int ticketId)
        {
            bool ret = DataAccess.Tickets.DeleteTicketByTicketId(ticketId);
            RemoveFromCache("Tickets_");
            return ret;
        }

        /// <summary>
        /// Delete Tickets By EventId
        /// </summary>
        public static bool DeleteTicketsByEventId(int eventId)
        {
            bool ret = DataAccess.Tickets.DeleteTicketsByEventId(eventId);
            RemoveFromCache("Tickets_");
            return ret;
        }
    }
}
