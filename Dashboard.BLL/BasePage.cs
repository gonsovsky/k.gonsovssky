﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace Dashboard.BLL
{
    public class BasePage:Page
    {
        protected void RequestLogin()
        {
            Response.Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=" + Request.Url.PathAndQuery);
        }

        private static Dictionary<decimal, Color> Colors;
        public static KnownColor Color(decimal price)
        {
            if (Colors == null)
            {
                Colors = new Dictionary<decimal, Color>();
                Colors.Add(500, System.Drawing.Color.CornflowerBlue);
                Colors.Add(600, System.Drawing.Color.DodgerBlue);
                Colors.Add(700, System.Drawing.Color.Navy);
                Colors.Add(800, System.Drawing.Color.YellowGreen);
                Colors.Add(900, System.Drawing.Color.ForestGreen);
                Colors.Add(1000, System.Drawing.Color.Goldenrod);
                Colors.Add(1300, System.Drawing.Color.DarkOrange);
                Colors.Add(1200, System.Drawing.Color.Orchid);
                Colors.Add(1500, System.Drawing.Color.Red);
                Colors.Add(1600, System.Drawing.Color.Crimson);
                Colors.Add(1700, System.Drawing.Color.Crimson);
                Colors.Add(1800, System.Drawing.Color.Crimson);
                Colors.Add(1900, System.Drawing.Color.Crimson);
                Colors.Add(2000, System.Drawing.Color.Crimson);
                Colors.Add(2100, System.Drawing.Color.Crimson);
                Colors.Add(2200, System.Drawing.Color.Crimson);
                Colors.Add(2300, System.Drawing.Color.Crimson);
                Colors.Add(2400, System.Drawing.Color.Crimson);
                Colors.Add(2500, System.Drawing.Color.Crimson);
                Colors.Add(2600, System.Drawing.Color.Crimson);
                Colors.Add(2700, System.Drawing.Color.Crimson);
                Colors.Add(2800, System.Drawing.Color.Crimson);
                Colors.Add(2900, System.Drawing.Color.Crimson);
                Colors.Add(3000, System.Drawing.Color.Crimson);
                Colors.Add(3100, System.Drawing.Color.Crimson);
                Colors.Add(3200, System.Drawing.Color.Crimson);
                Colors.Add(3300, System.Drawing.Color.Crimson);
                Colors.Add(3400, System.Drawing.Color.Crimson);
                Colors.Add(3500, System.Drawing.Color.Crimson);
            }

            KnownColor color = System.Drawing.Color.Blue.ToKnownColor();
            try
            {
                color = Colors[price].ToKnownColor();
            }
            catch {}
            return color;
        }
        
        public static bool GetInt(HttpRequest request, string name, out int result)
        {
            bool success = false;
            result = -1;

            if (request.QueryString[name] != null)
            {
                string key = request.QueryString[name];
                int value;
                if (Int32.TryParse(key, out value))
                {
                    result = value;
                    success = true;
                }
            }

            return success;
        }

        public static bool GetDate(HttpRequest request, string name, out DateTime result)
        {
            bool success = false;
            result = DateTime.MinValue;

            if (request.QueryString[name] != null)
            {
                string key = request.QueryString[name];
                DateTime value;
                if (DateTime.TryParse(key, out value))
                {
                    result = value;
                    success = true;
                }
            }

            return success;
        }

        public static string GenerateNum(int index)
        {
            Random random = new Random();
            string num = string.Empty;
            for (int i = 0; i < index; i++)
            {
                num += random.Next(0, 9);
            }
            return num;
        }
    }
}