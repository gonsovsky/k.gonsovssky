﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dashboard.Common;
using Dashboard.Data.Data;

namespace Dashboard.BLL
{
    public class Forms : BaseData
    {
        #region FormTypes

        public static List<Form> GetFormTypes()
        {
            List<Form> tickets = null;
            string key = "Forms_GetFormTypes_";

            if (Cache[key] != null)
            {
                tickets = (List<Form>)Cache[key];
            }
            else
            {
                tickets = DataAccess.Forms.GetFormTypes();
                CacheData(key, tickets);
            }
            return tickets;
        }

        #endregion

        #region Forms

        public static List<Form> GetFormsByType(int typeId)
        {
            List<Form> tickets = null;
            string key = "Forms_GetFormsByType_" + typeId;

            if (Cache[key] != null)
            {
                tickets = (List<Form>)Cache[key];
            }
            else
            {
                tickets = DataAccess.Forms.GetFormsByType(typeId);
                CacheData(key, tickets);
            }
            return tickets;
        }

        public static Form GetFormByFormId(int id)
        {
            Form form = null;
            string key = "Forms_GetFormByFormId_" + id;

            if (Cache[key] != null)
            {
                form = (Form)Cache[key];
            }
            else
            {
                form = DataAccess.Forms.GetFormByFormId(id);
                CacheData(key, form);
            }
            return form;
        }
        
        public static int CountForms()
        {
            int itemCount = 0;
            string key = "Forms_CountForms";

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.Forms.CountForms();
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int InsertForm(Form form)
        {
            int returnValue = DataAccess.Forms.InsertForm(form);
            RemoveFromCache("Forms_");
            return returnValue;
        }

        public static bool DeleteFormByFormId(int id)
        {
            bool ret = DataAccess.Forms.DeleteFormByFormId(id);
            RemoveFromCache("Forms_");
            return ret;
        }

        #endregion

        #region FormFields

        public static List<Form> GetFormFieldsByFormId(int formId)
        {
            List<Form> fields = DataAccess.Forms.GetFormFieldsByFormId(formId);
            //string key = "Forms_GetFormFieldsByFormId_" + formId;

            //if (Cache[key] != null)
            //{
            //    fields = (List<Form>)Cache[key];
            //}
            //else
            //{
            //    fields = DataAccess.Forms.GetFormFieldsByFormId(formId);
            //    CacheData(key, fields);
            //}
            return fields;
        }

        public static Form GetFormFieldById(int id)
        {
            Form form = null;
            string key = "Forms_GetFormFieldById_" + id;

            if (Cache[key] != null)
            {
                form = (Form)Cache[key];
            }
            else
            {
                form = DataAccess.Forms.GetFormFieldById(id);
                CacheData(key, form);
            }
            return form;
        }

        public static int CountFormFieldsByFormId(int formId)
        {
            int itemCount = 0;
            string key = "Forms_CountFormFieldsByFormId_" + formId;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.Forms.CountFormFieldsByFormId(formId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int InsertFormField(Form form)
        {
            int returnValue = DataAccess.Forms.InsertFormField(form);
            RemoveFromCache("Forms_");
            return returnValue;
        }

        public static bool UpdateFormField(Form form)
        {
            bool ret = DataAccess.Forms.UpdateFormField(form);
            RemoveFromCache("Forms_");
            return ret;
        }

        public static bool DeleteFormFieldById(int id)
        {
            bool ret = DataAccess.Forms.DeleteFormFieldById(id);
            RemoveFromCache("Forms_");
            return ret;
        } 
        
        #endregion
    }
}
