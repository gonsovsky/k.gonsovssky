﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;

namespace Dashboard.BLL
{
    public class ShowActions : BaseData, IReport
    {
        /// <summary>
        /// Gets actual Actions with dates
        /// </summary>
        /// <returns>ItemModel for web view</returns>
        public static ItemModel GetActualActions()
        {
            DateTime dt = DateTime.Now;
            ItemModel itemModel = new ItemModel();
            string key = "ShowActions_GetActualActions_" + dt.Year + "_" + dt.Month + "_" + dt.Day;

            if (Cache[key] != null)
            {
                itemModel = (ItemModel)Cache[key];
            }
            else
            {
                List<Group> groups = GroupShowActionsOnEventsByDate(dt.Year, dt.Month, dt.Day, dt.Hour);
                var ids = groups.GroupBy(g => g.Id);
                foreach (var id in ids)
                {
                    Group g = groups.FirstOrDefault(x => x.Id == id.Key);
                    if (g != null)
                    {
                        Item item = new Item();
                        item.Group.Id = g.Id;
                        item.Group.Name = g.Name;
                        item.Group.Description = g.Description.Length > 160 ? g.Description.Substring(0, 160) + "..." : g.Description;
                        int keyId = id.Key;
                        IEnumerable<Group> gps = groups.Where(x => x.Id == keyId).Select(x => x);
                        item = GetUniqueDates(gps, item, 5);
                        itemModel.Items.Add(item);
                    }
                }

                CacheData(key, itemModel);
            }

            return itemModel;
        }

        /// <summary>
        /// Gets Unique Dates
        /// </summary>
        /// <param name="groups">Set of dates</param>
        /// <param name="item">Item to be populated and returned</param>
        /// <param name="count">Number of dates in output</param>
        /// <returns>Returns Item with unique dates limited by length</returns>
        public static Item GetUniqueDates(IEnumerable<Group> groups, Item item, int count)
        {
            int counter = 0;
            foreach (Group g in groups)
            {
                bool add = true;

                foreach (Group date in item.Dates)
                {
                    if (date.StartDate.Year == g.StartDate.Year & date.StartDate.Month == g.StartDate.Month & date.StartDate.Day == g.StartDate.Day)
                    {
                        add = false;
                    }
                }

                if (add)
                {
                    item.Dates.Add(g);
                    counter++;
                }

                if (item.Dates.Count == count)
                {
                    break;
                }
            }

            return item;
        }

        /// <summary>
        /// Parameters assure that Cache keeps results during an hour
        /// </summary>
        public static List<Group> GroupShowActionsOnEventsByDate(int year, int month, int day, int hour)
        {
            List<Group> groups = new List<Group>();
            string key = "ShowActions_GroupShowActionsOnEventsByDate_" + year + "_" + month + "_" + day + "_" + hour;

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                DateTime date = new DateTime(year, month, day, hour, 0, 0);
                groups = DataAccess.ShowActions.GroupShowActionsOnEventsByDate(date);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Retrieves ShowActions list with event count and ticket count.
        /// Note, the result is dependant on member's provider list.
        /// Groups ShowActions On Events By MemberId.
        /// </summary>
        /// <param name="providerId">Optional: use ProviderId = 0 to retrieve all the ShowActions</param>
        /// <param name="memberId">MemberId is obligatory</param>
        /// <param name="statusId">StatusId is obligatory</param>
        /// <param name="year">Year is obligatory</param>
        /// <param name="month">Month is obligatory</param>
        /// <returns>Returns grouped list of ShowActionId, Name, Events count, Ticket count</returns>
        public static List<Group> GroupShowActionsOnEventsByMemberId(int providerId, int memberId, int statusId, int year, int month)
        {
            List<Group> months = new List<Group>();
            string key = "ShowActions_GroupShowActionsOnEventsByMemberId_" + providerId + "_" + memberId + "_" + statusId + "_" + year + "_" + month;

            if (Cache[key] != null)
            {
                months = (List<Group>)Cache[key];
            }
            else
            {
                months = DataAccess.ShowActions.GroupShowActionsOnEventsByMemberId(providerId, memberId, statusId, year, month);
                CacheData(key, months);
            }
            return months;
        }

        public static ShowAction GetShowActionByActionId(int actionId)
        {
            ShowAction pageItem = null;
            string key = "ShowActions_GetShowActionByActionId_" + actionId;

            if (Cache[key] != null)
            {
                pageItem = (ShowAction)Cache[key];
            }
            else
            {
                pageItem = DataAccess.ShowActions.GetShowActionByActionId(actionId);
                CacheData(key, pageItem);
            }
            return pageItem;
        }

        /// <summary>
        /// Gets ShowActions By ProviderId
        /// </summary>
        /// <param name="providerId">Optional: use providerId = 0 to retrieve all the ShowActions</param>
        /// <returns></returns>
        public static List<ShowAction> GetShowActionsByProviderId(int providerId, int year)
        {
            List<ShowAction> showActions = null;
            string key = "ShowActions_GetShowActionsByProviderId_" + providerId + "_" + year;

            if (Cache[key] != null)
            {
                showActions = (List<ShowAction>)Cache[key];
            }
            else
            {
                showActions = DataAccess.ShowActions.GetShowActionsByProviderId(providerId, year);
                CacheData(key, showActions);
            }
            return showActions;
        }

        /// <summary>
        /// Groups ShowActions On Months By Year
        /// </summary>
        public static List<Group> GroupShowActionsOnMonthsByYear(int year)
        {
            List<Group> months = new List<Group>();
            string key = "ShowActions_GroupShowActionsOnMonthsByYear_" + year;

            if (Cache[key] != null)
            {
                months = (List<Group>)Cache[key];
            }
            else
            {
                months = DataAccess.ShowActions.GroupShowActionsOnMonthsByYear(year);
                CacheData(key, months);
            }
            return months;
        }

        /// <summary>
        /// Gets Paged ShowActions
        /// </summary>
        public static List<ShowAction> GetPagedShowActions(int startRowIndex, int maximumRows, int memberId, int providerId, int year)
        {
            List<ShowAction> showActions = null;
            string key = "ShowActions_GetPagedShowActions_" + startRowIndex + "_" + maximumRows + "_" + memberId + "_" + providerId + "_" + year;

            if (Cache[key] != null)
            {
                showActions = (List<ShowAction>)Cache[key];
            }
            else
            {
                showActions = DataAccess.ShowActions.GetPagedShowActions(GetPageIndex(startRowIndex, maximumRows), maximumRows, memberId, providerId, year);
                CacheData(key, showActions);
            }
            return showActions;
        }

        /// <summary>
        /// Gets aggregated ShowActions Stats 
        /// </summary>
        public static DataTable GetShowActionSortableStats(int providerId, int statusId, int year)
        {
            DataTable dt;

            string key = "ShowActions_GetShowActionSortableStats_" + providerId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                dt = (DataTable)Cache[key];
            }
            else
            {
                List<ShowAction> showActions = GetShowActionStats(providerId, statusId, year);
                dt = new DataTable("ShowActions");
                dt.Columns.Add(new DataColumn("Id", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Name", typeof(String)));
                dt.Columns.Add(new DataColumn("StartDate", typeof(DateTime)));
                dt.Columns.Add(new DataColumn("EndDate", typeof(DateTime)));
                dt.Columns.Add(new DataColumn("TotalCount", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Count", typeof(Int32)));
                dt.Columns.Add(new DataColumn("CountResult", typeof(Int32)));
                dt.Columns.Add(new DataColumn("Percent", typeof(Int32)));
                dt.Columns.Add(new DataColumn("TotalPrice", typeof(Decimal)));
                dt.Columns.Add(new DataColumn("Price", typeof(Decimal)));
                dt.Columns.Add(new DataColumn("PriceResult", typeof(Decimal)));

                foreach (ShowAction sa in showActions)
                {
                    DataRow dr = dt.NewRow();
                    dr["Id"] = sa.Id;
                    dr["Name"] = sa.Name;
                    dr["StartDate"] = sa.StartDate;
                    dr["EndDate"] = sa.EndDate;
                    dr["TotalCount"] = EventDates.CountEventDates(0, 0, sa.Id, year);
                    dr["Count"] = sa.Count;
                    dr["CountResult"] = sa.CountResult;
                    dr["Percent"] = sa.Percent;
                    dr["TotalPrice"] = sa.TotalPrice;
                    dr["Price"] = sa.Price;
                    dr["PriceResult"] = sa.PriceResult;
                    dt.Rows.Add(dr);
                }
                dt.DefaultView.Sort = "Price DESC";
                dt = dt.DefaultView.ToTable();
                CacheData(key, dt);
            }

            return dt;
        }

        /// <summary>
        /// Gets aggregated ShowActions Stats 
        /// </summary>
        public static List<ShowAction> GetShowActionStats(int providerId, int statusId, int year)
        {
            List<ShowAction> showActions = new List<ShowAction>();
            string key = "ShowActions_GetShowActionStats_" + providerId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                showActions = (List<ShowAction>)Cache[key];
            }
            else
            {
                List<ShowAction> issued = GroupShowActionsOnTickets(providerId, 0, year);
                List<ShowAction> sold = GroupShowActionsOnTickets(providerId, statusId, year);

                foreach (ShowAction i in issued)
                {
                    foreach (ShowAction s in sold)
                    {
                        if (i.Id == s.Id)
                        {

                            i.CountResult = i.Count - s.Count; // Непродано, билетов

                            int percent = i.Count / 100; // Заполняемость зала
                            if (percent > 0 && i.Count > 0)
                            {
                                i.Percent = s.Count / percent;
                            }

                            i.Count = s.Count; // Продано, билетов
                            i.PriceResult = i.Price - s.Price;// Непродано, рублей
                            i.Price = s.Price; // Продано, рублей
                            showActions.Add(i);
                            break;
                        }
                    }
                }

                CacheData(key, showActions);
            }
            return showActions;
        }

        /// <summary>
        /// Groups ShowActions On Tickets 
        /// </summary>
        public static List<ShowAction> GroupShowActionsOnTickets(int providerId, int statusId, int year)
        {
            List<ShowAction> tickets = null;
            string key = "ShowActions_GroupShowActionsOnTickets_" + providerId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                tickets = (List<ShowAction>)Cache[key];
            }
            else
            {
                tickets = DataAccess.ShowActions.GroupShowActionsOnTickets(providerId, statusId, year);
                CacheData(key, tickets);
            }
            return tickets;
        }

        public static List<Group> GroupShowActionsOnEvents(int providerId, int statusId, int year)
        {
            List<Group> groups = null;
            string key = "ShowActions_GroupShowActionsOnEvents_" + providerId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                groups = DataAccess.ShowActions.GroupShowActionsOnEvents(providerId, statusId, year);
                CacheData(key, groups);
            }
            return groups;
        }

        public static IEnumerable<Group> GetTopShowActions(int top, int statusId, int year)
        {
            if (year == 0)
            {
                year = DateTime.Now.Year;
            }
            IEnumerable<Group> groups = new List<Group>();

            string key = "ShowActions_GetTopShowActions_" + top + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                groups = (IEnumerable<Group>)Cache[key];
            }
            else
            {
                Group group = Tickets.SumSoldTickets(0, 0, statusId, year);
                int percent = (int)group.Price / 100;
                List<Group> grps = GroupShowActionsOnPrice(statusId, year);

                foreach (Group g in grps)
                {
                    g.Percent = (int)g.Price / percent;
                }

                groups = grps.OrderByDescending(x => x.PriceResult).Take(top);
                CacheData(key, groups);
            }

            return groups;

        }

        /// <summary>
        /// Groups ShowActions On Price 
        /// </summary>
        /// <param name="statusId">Status of the tickets</param>
        /// <param name="year">Required Year</param>
        /// <returns>Returns list of total by overall price and count grouped by ShowAction</returns>
        public static List<Group> GroupShowActionsOnPrice(int statusId, int year)
        {
            List<Group> groups = null;
            string key = "ShowActions_GroupShowActionsOnPrice_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                groups = (List<Group>)Cache[key];
            }
            else
            {
                groups = DataAccess.ShowActions.GroupShowActionsOnPrice(statusId, year);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Counts ShowAction items
        /// </summary>
        public static int CountShowActions(int memberId, int providerId, int year)
        {
            int itemCount = 0;
            string key = "ShowActions_CountShowActions_" + memberId + "_" + providerId + "_" + year;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.ShowActions.CountShowActions(memberId, providerId, year);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        /// <summary>
        /// Gets Available Years
        /// </summary>
        public static List<int> GetAvailableYears()
        {
            List<int> years = new List<int>();
            string key = "ShowActions_GetAvailableYears";

            if (Cache[key] != null)
            {
                years = (List<int>)Cache[key];
            }
            else
            {
                years = DataAccess.ShowActions.GetAvailableYears();
                CacheData(key, years);
            }
            return years;
        }
    }
}
