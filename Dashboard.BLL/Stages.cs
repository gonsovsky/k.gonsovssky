﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;

namespace Dashboard.BLL
{
    public class Stages : BaseData, IReport
    {
        #region City

        public static List<City> GetCities()
        {
            List<City> tickets = null;
            string key = "Stages_GetCities";

            if (Cache[key] != null)
            {
                tickets = (List<City>) Cache[key];
            }
            else
            {
                tickets = DataAccess.Stages.GetCities();
                CacheData(key, tickets);
            }
            return tickets;
        }

        #endregion

        #region Providers

        public static List<Provider> GetProvidersByMemberId(int memberId)
        {
            List<Provider> members = null;
            string key = "Stages_GetProvidersByMemberId_" + memberId;

            if (Cache[key] != null)
            {
                members = (List<Provider>) Cache[key];
            }
            else
            {
                members = DataAccess.Stages.GetProvidersByMemberId(memberId);
                CacheData(key, members);
            }
            return members;
        }

        /// <summary>
        /// Gets providers which do not enlisted in member's provider list
        /// </summary>
        public static List<Provider> GetProviders(int memberId)
        {
            List<Provider> members = null;
            string key = "Stages_GetProviders_" + memberId;

            if (Cache[key] != null)
            {
                members = (List<Provider>) Cache[key];
            }
            else
            {
                members = DataAccess.Stages.GetProviders(memberId);
                CacheData(key, members);
            }
            return members;
        }

        /// <summary>
        /// Retrieves Providers list with show action count and event count.
        /// Note, the result is dependant on member's provider list.
        /// Groups Providers On Events By MemberId.
        /// </summary>
        /// <param name="memberId">MemberId is obligatory</param>
        /// <param name="year">Year is obligatory</param>
        /// <param name="month">Month is obligatory</param>
        /// <returns>Returns grouped list of Providers, Name, ShowActions count, Events count</returns>
        public static List<Group> GroupProvidersOnEventsByMemberId(int memberId, int year, int month)
        {
            List<Group> providers = new List<Group>();
            string key = "Stages_GroupProvidersOnEventsByMemberId_" + memberId + "_" + year + "_" + month;

            if (Cache[key] != null)
            {
                providers = (List<Group>) Cache[key];
            }
            else
            {
                providers = DataAccess.Stages.GroupProvidersOnEventsByMemberId(memberId, year, month);
                CacheData(key, providers);
            }
            return providers;
        }

        public static Provider GetProviderByProviderId(int providerId)
        {
            Provider stage = null;
            string key = "Stages_GetProviderByProviderId_" + providerId;

            if (Cache[key] != null)
            {
                stage = (Provider) Cache[key];
            }
            else
            {
                stage = DataAccess.Stages.GetProviderByProviderId(providerId);
                CacheData(key, stage);
            }
            return stage;
        }

        /// <summary>
        /// Gets aggregated Providers Stats 
        /// </summary>
        public static DataTable GetProviderSortableStats(int statusId, int year)
        {
            DataTable dt;

            string key = "Stages_GetProviderSortableStats_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                dt = (DataTable) Cache[key];
            }
            else
            {
                List<Provider> providers = GetProviderStats(statusId, year);
                List<Group> events = GroupProvidersOnShowActions(0, 4, year);
                dt = new DataTable();
                dt.Columns.Add(new DataColumn("Id", typeof (Int32)));
                dt.Columns.Add(new DataColumn("Title", typeof (String)));
                dt.Columns.Add(new DataColumn("TotalCount", typeof (Int32)));
                dt.Columns.Add(new DataColumn("Count", typeof (Int32)));
                dt.Columns.Add(new DataColumn("CountResult", typeof (Int32)));
                dt.Columns.Add(new DataColumn("Percent", typeof (Int32)));
                dt.Columns.Add(new DataColumn("TotalPrice", typeof (Decimal)));
                dt.Columns.Add(new DataColumn("Price", typeof (Int32)));
                dt.Columns.Add(new DataColumn("PriceResult", typeof (Int32)));
                dt.Columns.Add(new DataColumn("ActionCount", typeof (Int32)));
                dt.Columns.Add(new DataColumn("EventCount", typeof (Int32)));
                dt.Columns.Add(new DataColumn("TicketCount", typeof (Int32)));
                dt.Columns.Add(new DataColumn("AveragePrice", typeof (Decimal)));

                foreach (Provider p in providers)
                {
                    DataRow dr = dt.NewRow();
                    dr["Id"] = p.Id;
                    dr["Title"] = p.Name;
                    dr["TotalCount"] = p.TotalCount;
                    dr["Count"] = p.Count;
                    dr["CountResult"] = p.CountResult;
                    dr["Percent"] = p.Percent;
                    dr["TotalPrice"] = p.TotalPrice;
                    dr["Price"] = p.Price;
                    dr["PriceResult"] = p.PriceResult;

                    foreach (Group g in events)
                    {
                        if (g.Id == p.Id)
                        {
                            dr["ActionCount"] = g.Count;
                            dr["EventCount"] = g.TotalCount;
                            dr["TicketCount"] = g.CountResult;
                            dr["AveragePrice"] = g.AveragePrice;
                            break;
                        }
                    }


                    dt.Rows.Add(dr);
                }



                CacheData(key, dt);
            }

            return dt;
        }

        /// <summary>
        /// Gets aggregated Providers Stats 
        /// </summary>
        public static List<Provider> GetProviderStats(int statusId, int year)
        {
            List<Provider> providers = new List<Provider>();
            string key = "Stages_GetProviderStats_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                providers = (List<Provider>) Cache[key];
            }
            else
            {
                int month = 0;
                DateTime dt = DateTime.Now;
                if (dt.Year == year)
                {
                    month = dt.Month;
                }

                List<Provider> issued = GroupProvidersOnTickets(0, year, month);
                List<Provider> sold = GroupProvidersOnTickets(statusId, year, month);

                foreach (Provider i in issued)
                {
                    foreach (Provider s in sold)
                    {
                        if (i.Id == s.Id)
                        {
                            i.TotalCount = i.Count; // TotalCount - tickets in total
                            i.Count = s.Count; // Продано, билетов
                            i.CountResult = i.TotalCount - s.Count; // Непродано, билетов

                            int percent = i.TotalCount/100; // Заполняемость зала
                            if (percent > 0 && i.Count > 0)
                            {
                                i.Percent = i.Count/percent; // Процент проданных и непроданных билетов
                            }
                            i.PriceResult = i.Price - s.Price; // Непродано, рублей
                            i.Price = s.Price; // Продано, рублей
                            providers.Add(i);
                            break;
                        }
                    }
                }

                CacheData(key, providers);
            }
            return providers;
        }

        /// <summary>
        /// Groups Providers on Tickets by status and year
        /// </summary>
        public static List<Provider> GroupProvidersOnTickets(int statusId, int year, int month)
        {
            List<Provider> tickets = null;
            string key = "Stages_GroupProvidersOnTickets_" + statusId + "_" + year + "_" + month;

            if (Cache[key] != null)
            {
                tickets = (List<Provider>) Cache[key];
            }
            else
            {
                tickets = DataAccess.Stages.GroupProvidersOnTickets(statusId, year, month);
                CacheData(key, tickets);
            }
            return tickets;
        }

        /// <summary>
        /// Groups Providers on Months by tickets 
        /// </summary>
        public static List<Group> GroupProviderOnMonths(int providerId, int statusId, int year)
        {
            List<Group> tickets = null;
            string key = "Stages_GroupProviderOnMonths_" + providerId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                tickets = (List<Group>) Cache[key];
            }
            else
            {
                tickets = DataAccess.Stages.GroupProviderOnMonths(providerId, statusId, year);
                CacheData(key, tickets);
            }
            return tickets;
        }

        /// <summary>
        /// Groups Providers On ShowActions, EventDates, Tickets
        /// </summary>
        /// <param name="providerId">Provider</param>
        /// <param name="statusId">Status of the ticket</param>
        /// <param name="year">Year</param>
        /// <returns>Returns ProviderId, ProviderName, ShowActions count, EventDates count, Tickets count</returns>
        public static List<Group> GroupProvidersOnShowActions(int providerId, int statusId, int year)
        {
            List<Group> groups = null;
            string key = "Stages_GroupProvidersOnShowActions_" + providerId + "_" + statusId + "_" + year;

            if (Cache[key] != null)
            {
                groups = (List<Group>) Cache[key];
            }
            else
            {
                groups = DataAccess.Stages.GroupProvidersOnShowActions(providerId, statusId, year);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Retrieves Providers collection
        /// </summary>
        public static List<Provider> GetPagedProviders(int startRowIndex, int maximumRows, int memberId, int cityId)
        {
            List<Provider> tickets = null;
            string key = "Stages_GetPagedProviders_" + startRowIndex + "_" + maximumRows + "_" + memberId + "_" + cityId;

            if (Cache[key] != null)
            {
                tickets = (List<Provider>) Cache[key];
            }
            else
            {
                tickets = DataAccess.Stages.GetPagedProviders(GetPageIndex(startRowIndex, maximumRows), maximumRows,
                    memberId, cityId);
                CacheData(key, tickets);
            }
            return tickets;
        }

        /// <summary>
        /// Counts Provider items
        /// </summary>
        public static int CountProviders(int memberId, int cityId)
        {
            int itemCount = 0;
            string key = "Stages_CountProviders_" + memberId + "_" + cityId;

            if (Cache[key] != null)
            {
                itemCount = (int) Cache[key];
            }
            else
            {
                itemCount = DataAccess.Stages.CountProviders(memberId, cityId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }



        #endregion

        #region Stages

        public static Stage GetStageByStageId(int stageId)
        {
            Stage stage = null;
            string key = "Stages_GetStageByStageId_" + stageId;

            if (Cache[key] != null)
            {
                stage = (Stage) Cache[key];
            }
            else
            {
                stage = DataAccess.Stages.GetStageByStageId(stageId);
                CacheData(key, stage);
            }
            return stage;
        }

        public static Stage GetSideBySideId(int sideId)
        {
            Stage stage = null;
            string key = "Stages_GetSideBySideId_" + sideId;

            if (Cache[key] != null)
            {
                stage = (Stage) Cache[key];
            }
            else
            {
                stage = DataAccess.Stages.GetSideBySideId(sideId);
                CacheData(key, stage);
            }
            return stage;
        }

        public static Stage GetSectorBySectorId(int sectorId)
        {
            Stage stage = null;
            string key = "Stages_GetSectorBySectorId_" + sectorId;

            if (Cache[key] != null)
            {
                stage = (Stage) Cache[key];
            }
            else
            {
                stage = DataAccess.Stages.GetSectorBySectorId(sectorId);
                CacheData(key, stage);
            }
            return stage;
        }

        public static Stage GetSeatBySeatId(int seatId)
        {
            Stage stage = null;
            string key = "Stages_GetSeatBySeatId_" + seatId;

            if (Cache[key] != null)
            {
                stage = (Stage) Cache[key];
            }
            else
            {
                stage = DataAccess.Stages.GetSeatBySeatId(seatId);
                CacheData(key, stage);
            }
            return stage;
        }

        /// <summary>
        /// Groups Stages on Sides, Sectors, and Seats
        /// The result is dependent on parameter values
        /// </summary>
        /// <param name="providerId">Optional: use ProviderId = 0 to retrieve all the stages</param>
        /// <param name="stageId">Optional: use StageId = 0 to retrieve all the stages</param>
        /// <returns>List Group</returns>
        public static List<Group> GroupStagesOnSidesSectorsSeat(int providerId, int stageId)
        {
            List<Group> groups = new List<Group>();
            string key = "Stages_GroupStagesOnSidesSectorsSeat_" + providerId + "_" + stageId;

            if (Cache[key] != null)
            {
                groups = (List<Group>) Cache[key];
            }
            else
            {
                groups = DataAccess.Stages.GroupStagesOnSidesSectorsSeat(providerId, stageId);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Groups StageSides On Sectors and Seats
        /// </summary>
        /// <param name="stageId">Stageid</param>
        /// <returns>List Group</returns>
        public static List<Stage> GetStageSidesByStageId(int stageId)
        {
            List<Stage> groups = new List<Stage>();
            string key = "Stages_GetStageSidesByStageId_" + stageId;

            if (Cache[key] != null)
            {
                groups = (List<Stage>) Cache[key];
            }
            else
            {
                groups = DataAccess.Stages.GetStageSidesByStageId(stageId);
                CacheData(key, groups);
            }
            return groups;
        }

        public static List<Stage> GetSeatsByStageId(int stageId)
        {
            List<Stage> groups = new List<Stage>();
            string key = "Stages_GetSeatsByStageId_" + stageId;

            if (Cache[key] != null)
            {
                groups = (List<Stage>) Cache[key];
            }
            else
            {
                groups = DataAccess.Stages.GetSeatsByStageId(stageId);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Groups StageSectors On Seats
        /// </summary>
        /// <param name="sideId">SideId</param>
        /// <returns>List Group</returns>
        public static List<Group> GroupStageSectorsOnSeats(int sideId)
        {
            List<Group> groups = new List<Group>();
            string key = "Stages_GroupStageSectorsOnSeats_" + sideId;

            if (Cache[key] != null)
            {
                groups = (List<Group>) Cache[key];
            }
            else
            {
                groups = DataAccess.Stages.GroupStageSectorsOnSeats(sideId);
                CacheData(key, groups);
            }
            return groups;
        }

        public static List<Group> GroupSeatsOnPrice(int stageId)
        {
            List<Group> groups = new List<Group>();
            string key = "Stages_GroupSeatsOnPrice_" + stageId;

            if (Cache[key] != null)
            {
                groups = (List<Group>) Cache[key];
            }
            else
            {
                groups = DataAccess.Stages.GroupSeatsOnPrice(stageId);
                CacheData(key, groups);
            }
            return groups;
        }

        public static List<Group> GroupTicketSeatsOnPrice(int eventId)
        {
            List<Group> groups = new List<Group>();
            string key = "Stages_GroupTicketSeatsOnPrice_" + eventId;

            if (Cache[key] != null)
            {
                groups = (List<Group>) Cache[key];
            }
            else
            {
                groups = DataAccess.Stages.GroupTicketSeatsOnPrice(eventId);
                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Retrieves Stages collection
        /// </summary>
        public static List<Stage> GetPagedStages(int startRowIndex, int maximumRows, int memberId, int providerId)
        {
            List<Stage> stages = null;
            string key = "Stages_GetPagedStages_" + startRowIndex + "_" + maximumRows + "_" + memberId + "_" +
                         providerId;

            if (Cache[key] != null)
            {
                stages = (List<Stage>) Cache[key];
            }
            else
            {
                stages = DataAccess.Stages.GetPagedStages(GetPageIndex(startRowIndex, maximumRows), maximumRows,
                    memberId, providerId);
                CacheData(key, stages);
            }
            return stages;
        }

        public static List<Stage> GetRowsBySectorId(int sectorId)
        {
            List<Stage> groups = new List<Stage>();
            string key = "Stages_GetRowsBySectorId_" + sectorId;

            if (Cache[key] != null)
            {
                groups = (List<Stage>) Cache[key];
            }
            else
            {
                groups = DataAccess.Stages.GetRowsBySectorId(sectorId);
                CacheData(key, groups);
            }
            return groups;
        }

        public static List<Stage> GetSeatsByRowNum(int sectorId, int rowNum)
        {
            List<Stage> groups = new List<Stage>();
            string key = "Stages_GetSeatsByRowNum_" + sectorId + "_" + rowNum;

            if (Cache[key] != null)
            {
                groups = (List<Stage>) Cache[key];
            }
            else
            {
                groups = DataAccess.Stages.GetSeatsByRowNum(sectorId, rowNum);
                CacheData(key, groups);
            }
            return groups;
        }

        public static List<Stage> GetSeatPointsBySectorId(int sectorId)
        {
            List<Stage> stages = new List<Stage>();
            string key = "Stages_GetSeatPointsBySectorId_" + sectorId;

            if (Cache[key] != null)
            {
                stages = (List<Stage>) Cache[key];
            }
            else
            {
                stages = DataAccess.Stages.GetSeatPointsBySectorId(sectorId);
                CacheData(key, stages);
            }
            return stages;
        }

        public static List<Stage> GetSeatsByIds(string ids)
        {
            return DataAccess.Stages.GetSeatsByIds(ids);
        }

        public static List<Stage> GroupTicketSeatsByEventId(int memberId, int eventId)
        {
            List<Stage> stages = new List<Stage>();
            string key = "Stages_GroupTicketSeatsByEventId_" + eventId;

            if (Cache[key] != null)
            {
                stages = (List<Stage>) Cache[key];
            }
            else
            {
                stages = DataAccess.Stages.GroupTicketSeatsByEventId(eventId);
                CacheData(key, stages);
            }
            return stages;
        }

        public static List<Stage> GetTicketSeatsByEventId(int memberId, int eventId, int sideId, int sectorId)
        {
            List<Stage> stages = new List<Stage>();
            string key = "Stages_GetTicketSeatsByEventId_" + eventId + "_" + sideId + "_" + sectorId;

            if (Cache[key] != null)
            {
                stages = (List<Stage>) Cache[key];
            }
            else
            {
                stages = DataAccess.Stages.GetTicketSeatsByEventId(eventId, sideId, sectorId);
                CacheData(key, stages);
            }
            return stages;
        }

        /// <summary>
        /// Counts Stage items
        /// </summary>
        public static int CountStages(int memberId, int providerId)
        {
            int itemCount = 0;
            string key = "Stages_CountStages_" + memberId + "_" + providerId;

            if (Cache[key] != null)
            {
                itemCount = (int) Cache[key];
            }
            else
            {
                itemCount = DataAccess.Stages.CountStages(memberId, providerId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int CountSidesByStageId(int stageId)
        {
            int itemCount = 0;
            string key = "Stages_CountSidesByStageId_" + stageId;

            if (Cache[key] != null)
            {
                itemCount = (int) Cache[key];
            }
            else
            {
                itemCount = DataAccess.Stages.CountSidesByStageId(stageId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int CountSectorsBySideId(int sideId)
        {
            int itemCount = 0;
            string key = "Stages_CountSectorsBySideId_" + sideId;

            if (Cache[key] != null)
            {
                itemCount = (int) Cache[key];
            }
            else
            {
                itemCount = DataAccess.Stages.CountSectorsBySideId(sideId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int CountSectorsByStageId(int stageId)
        {
            int itemCount = 0;
            string key = "Stages_CountSectorsByStageId_" + stageId;

            if (Cache[key] != null)
            {
                itemCount = (int) Cache[key];
            }
            else
            {
                itemCount = DataAccess.Stages.CountSectorsByStageId(stageId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int CountSeatsBySideId(int sideId)
        {
            int itemCount = 0;
            string key = "Stages_CountSeatsBySideId_" + sideId;

            if (Cache[key] != null)
            {
                itemCount = (int) Cache[key];
            }
            else
            {
                itemCount = DataAccess.Stages.CountSeatsBySideId(sideId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int CountSeatsByStageId(int stageId)
        {
            int itemCount = 0;
            string key = "Stages_CountSeatsByStageId_" + stageId;

            if (Cache[key] != null)
            {
                itemCount = (int) Cache[key];
            }
            else
            {
                itemCount = DataAccess.Stages.CountSeatsByStageId(stageId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static int CountSeatsBySectorId(int sectorId)
        {
            int itemCount = 0;
            string key = "Stages_CountSeatsBySectorId_" + sectorId;

            if (Cache[key] != null)
            {
                itemCount = (int) Cache[key];
            }
            else
            {
                itemCount = DataAccess.Stages.CountSeatsBySectorId(sectorId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        public static List<Stage> GetSidesByStageId(int stageId)
        {
            List<Stage> stages = null;
            string key = "Stages_GetSidesByStageId_" + stageId;

            if (Cache[key] != null)
            {
                stages = (List<Stage>) Cache[key];
            }
            else
            {
                stages = DataAccess.Stages.GetSidesByStageId(stageId);
                CacheData(key, stages);
            }
            return stages;
        }

        public static List<Stage> GetSectorsBySideId(int sideId)
        {
            List<Stage> stages = null;
            string key = "Stages_GetSectorsBySideId_" + sideId;

            if (Cache[key] != null)
            {
                stages = (List<Stage>) Cache[key];
            }
            else
            {
                stages = DataAccess.Stages.GetSectorsBySideId(sideId);
                CacheData(key, stages);
            }
            return stages;
        }

        #endregion
    }
}
