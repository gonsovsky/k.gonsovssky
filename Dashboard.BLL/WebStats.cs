﻿using System;
using System.Collections.Generic;
using Dashboard.Common;
using Dashboard.Data.Data;

namespace Dashboard.BLL
{
    public class WebStats : BaseData
    {
        #region Sessions

        public static bool OrganizeData()
        {
            List<WebStat> webStats = GetSessionsByDeviceType(DeviceType.None);
            foreach (WebStat webStat in webStats)
            {
                if (string.IsNullOrEmpty(webStat.UserAgent))
                {
                    webStat.DeviceType = DeviceType.None;
                    UpdateSession(webStat);
                    continue;
                }

                DefineBot(webStat);
                webStat.DeviceType = DeviceType.None;
                if (webStat.BotId == 0)
                {
                    if (webStat.UserAgent.Contains("Android"))
                    {
                        webStat.DeviceType = DeviceType.Android;
                    }
                    if (webStat.UserAgent.Contains("Windows"))
                    {
                        webStat.DeviceType = DeviceType.Windows;
                    }
                    if (webStat.UserAgent.Contains("iPhone"))
                    {
                        webStat.DeviceType = DeviceType.ApplePhone;
                    }
                    if (webStat.UserAgent.Contains("iPad"))
                    {
                        webStat.DeviceType = DeviceType.ApplePad;
                    }
                    if (webStat.UserAgent.Contains("Macintosh"))
                    {
                        webStat.DeviceType = DeviceType.Macintosh;
                    }
                }

                UpdateSession(webStat);
            }
            return true;
        }

        public static WebStat DefineBot(WebStat webStat)
        {
            List<WebStat> bots = GetBots();
            foreach (WebStat bot in bots)
            {
                if (bot.UserAgent == webStat.UserAgent)
                {
                    webStat.BotId = bot.Id;
                    break;
                }
            }
            return webStat;
        }

        public static List<WebStat> GroupUserDevices()
        {
            List<WebStat> groups = null;
            string key = "Stats_GroupUserDevices_";

            if (Cache[key] != null)
            {
                groups = (List<WebStat>)Cache[key];
            }
            else
            {
                groups = DataAccess.WebStats.GroupUserDevices();

                foreach (WebStat group in groups)
                {
                    group.Name = StringEnum.GetStringValue(group.DeviceType);
                }

                CacheData(key, groups);
            }
            return groups;
        }

        /// <summary>
        /// Gets Sessions by single date
        /// </summary>
        public static List<WebStat> GetSessionsByDate(DateTime date)
        {
            List<WebStat> stats = new List<WebStat>();
            TimeSpan ts = DateTime.Now - date;
            if (ts.Days == 0)
            {
                stats = DataAccess.WebStats.GetSessionsByDate(date);
            }
            else
            {
                string key = string.Format("Stats_GetSessionsByDate_{0}_{1}_{2}", date.Day, date.Month, date.Year);

                if (Cache[key] != null)
                {
                    stats = (List<WebStat>) Cache[key];
                }
                else
                {
                    stats = DataAccess.WebStats.GetSessionsByDate(date);
                    CacheData(key, stats);
                }
            }
            return stats;
        }

        /// <summary>
        /// Gets Requests By Date
        /// </summary>
        public static List<WebStat> GetRequestsByDate(DateTime date, int sessionId)
        {
            List<WebStat> stats = new List<WebStat>();
            TimeSpan ts = DateTime.Now - date;
            if (ts.Days == 0)
            {
                stats = DataAccess.WebStats.GetRequestsByDate(date, sessionId);
            }
            else
            {
                string key = string.Format("Stats_GetRequestsByDate_{0}_{1}_{2}_{3}", date.Day, date.Month, date.Year, sessionId);
                if (Cache[key] != null)
                {
                    stats = (List<WebStat>)Cache[key];
                }
                else
                {
                    stats = DataAccess.WebStats.GetRequestsByDate(date, sessionId);
                    CacheData(key, stats);
                } 
            }
            
            return stats;
        }

        public static List<WebStat> GetSessionsByDeviceType(DeviceType deviceType)
        {
            List<WebStat> stats = new List<WebStat>();
            string key = string.Format("Stats_GetSessionsByDeviceType_{0}", deviceType);
            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GetSessionsByDeviceType(deviceType);
                CacheData(key, stats);
            }
            return stats;
        }

        /// <summary>
        /// Gets Sessions by month ahead
        /// </summary>
        public static List<WebStat> GetSessionsByMonth(DateTime date)
        {
            List<WebStat> stats = new List<WebStat>();
            string key = string.Format("Stats_GetSessionsByMonth_{0}_{1}", date.Month, date.Year);
            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GetSessionsByMonth(date.Month, date.Year);
                CacheData(key, stats);
            }
            return stats;
        }

        public static List<WebStat> GetBots()
        {
            List<WebStat> groups = null;
            string key = "Stats_GetBots_";

            if (Cache[key] != null)
            {
                groups = (List<WebStat>)Cache[key];
            }
            else
            {
                groups = DataAccess.WebStats.GetBots();
                CacheData(key, groups);
            }
            return groups;
        }

        public static bool UpdateSession(WebStat stat)
        {
            bool ret = DataAccess.WebStats.UpdateSession(stat);
            RemoveFromCache("Stats_");
            return ret;
        }
        
        #endregion

        #region Requests

        public static List<WebStat> GetPagedSales(int startRowIndex, int maximumRows)
        {
            List<WebStat> tickets = null;
            string key = "Stats_GetPagedSales_" + startRowIndex + "_" + maximumRows;

            if (Cache[key] != null)
            {
                tickets = (List<WebStat>)Cache[key];
            }
            else
            {
                tickets = DataAccess.WebStats.GetPagedSales(GetPageIndex(startRowIndex, maximumRows), maximumRows);
                CacheData(key, tickets);
            }
            return tickets;
        }

        public static int CountSales()
        {
            int itemCount = 0;
            string key = "Stats_CountSales_";

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.WebStats.CountSales();
                CacheData(key, itemCount);
            }
            return itemCount;
        }


        /// <summary>
        /// Gets Requests By Month ahead
        /// </summary>
        public static List<WebStat> GetRequestsByMonth(DateTime date)
        {
            List<WebStat> stats = new List<WebStat>();
            string key = string.Format("Stats_GetRequestsByMonth_{0}_{1}", date.Month, date.Year);
            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GetRequestsByMonth(date.Month, date.Year);
                CacheData(key, stats);
            }
            return stats;
        }

        /// <summary>
        /// Gets Site By SiteId
        /// </summary>
        public static WebStat GetSiteBySiteId(int siteId)
        {
            WebStat stat = null;
            string key = "Stats_GetSiteBySiteId_" + siteId;

            if (Cache[key] != null)
            {
                stat = (WebStat)Cache[key];
            }
            else
            {
                stat = DataAccess.WebStats.GetSiteBySiteId(siteId);
                CacheData(key, stat);
            }
            return stat;
        }

        /// <summary>
        /// Gets Requests By SessionId
        /// </summary>
        public static List<WebStat> GetRequestsBySessionId(int sessionId)
        {
            List<WebStat> stats = new List<WebStat>();
            string key = "Stats_GetRequestsBySessionId_" + sessionId;

            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GetRequestsBySessionId(sessionId);
                CacheData(key, stats);
            }
            return stats;
        }

        public static int CountSessionsByDate(DateTime date)
        {
            int itemCount = 0;
            string key = string.Format("Stats_CountRequestsByDate_{0}_{1}_{2}", date.Date, date.Month, date.Year);
            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.WebStats.CountSessionsByDate(date);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        /// <summary>
        /// Counts Requests by date
        /// </summary>
        public static int CountRequestsByDate(DateTime date)
        {
            int itemCount = 0;

            string key = string.Format("Stats_CountRequestsByDate_{0}_{1}_{2}", date.Date, date.Month, date.Year);
            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.WebStats.CountRequestsByDate(date);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        /// <summary>
        /// Gets Page By PageId
        /// </summary>
        public static WebStat GetPageByPageId(int pageId)
        {
            WebStat stat = null;
            string key = "Stats_GetPageByPageId_" + pageId;

            if (Cache[key] != null)
            {
                stat = (WebStat)Cache[key];
            }
            else
            {
                stat = DataAccess.WebStats.GetPageByPageId(pageId);
                CacheData(key, stat);
            }
            return stat;
        }

        /// <summary>
        /// Counts Requests by SessionId
        /// </summary>
        public static int CountRequestsBySessionId(int sessionId)
        {
            int itemCount = 0;
            string key = "Stats_CountRequestsBySessionId_" + sessionId;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.WebStats.CountRequestsBySessionId(sessionId);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        /// <summary>
        /// Gets Available Years
        /// </summary>
        public static List<int> GetAvailableYears()
        {
            List<int> years = new List<int>();
            string key = "Stats_GetAvailableYears";

            if (Cache[key] != null)
            {
                years = (List<int>)Cache[key];
            }
            else
            {
                years = DataAccess.WebStats.GetAvailableYears();
                CacheData(key, years);
            }
            return years;
        }

        /// <summary>
        /// Gets Available Months
        /// </summary>
        public static List<Group> GetAvailableMonths(int year)
        {
            List<Group> months = new List<Group>();
            string key = "Stats_GetAvailableMonths_" + year;

            if (Cache[key] != null)
            {
                months = (List<Group>)Cache[key];
            }
            else
            {
                months = DataAccess.WebStats.GetAvailableMonths(year);
                CacheData(key, months);
            }
            return months;
        }

        /// <summary>
        /// Groups KeyWords by year
        /// </summary>
        public static List<WebStat> GroupKeyWordsByYear(int year)
        {
            List<WebStat> stats = new List<WebStat>();
            string key = "Stats_GroupByKeyWord_" + year;

            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GroupKeyWordsByYear(year);
                CacheData(key, stats);
            }
            return stats;
        }

        /// <summary>
        /// Groups KeyWords
        /// </summary>
        public static List<WebStat> GroupByKeyWord()
        {
            List<WebStat> stats = new List<WebStat>();
            string key = "Stats_GroupByKeyWord";

            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GroupByKeyWord();
                CacheData(key, stats);
            }
            return stats;
        }

        /// <summary>
        /// Groups sessions by bot for year
        /// </summary>
        public static List<WebStat> GroupByBot(int year)
        {
            List<WebStat> stats = new List<WebStat>();
            string key = "Stats_GroupByBot_" + year;

            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GroupByBot(year);
                CacheData(key, stats);
            }
            return stats;
        }

        /// <summary>
        /// Counts sessions by BotId for month
        /// </summary>
        public static int CountBotsByMonth(int botId, int month, int year)
        {
            int itemCount = 0;
            string key = "Stats_CountBotsByMonth_" + botId + "_" + month + "_" + year;

            if (Cache[key] != null)
            {
                itemCount = (int)Cache[key];
            }
            else
            {
                itemCount = DataAccess.WebStats.CountBotsByMonth(botId, month, year);
                CacheData(key, itemCount);
            }
            return itemCount;
        }

        /// <summary>
        /// Groups sessions by visitor' IP addresses for every single month for a year
        /// </summary>
        public static List<Group> GroupByVisitors(int year)
        {
            List<Group> months = new List<Group>();
            string key = "Stats_GroupByVisitors_" + year;

            if (Cache[key] != null)
            {
                months = (List<Group>)Cache[key];
            }
            else
            {
                months = DataAccess.WebStats.GroupByVisitors(year);
                CacheData(key, months);
            }
            return months;
        }

        /// <summary>
        /// Groups total Sessions By Day
        /// </summary>
        public static List<WebStat> GroupSessionsByDay(DateTime date)
        {
            List<WebStat> stats = new List<WebStat>();
            string key = string.Format("Stats_GroupSessionsByDay_{0}_{1}", date.Month, date.Year);
            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GroupSessionsByDay(date.Month, date.Year);
                CacheData(key, stats);
            }
            return stats;
        }

        /// <summary>
        /// Groups total Requests By Day
        /// </summary>
        public static List<WebStat> GroupRequestsByDay(DateTime date)
        {
            List<WebStat> stats = new List<WebStat>();
            string key = string.Format("Stats_GroupRequestsByDay_{0}_{1}", date.Month, date.Year);
            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GroupRequestsByDay(date.Month, date.Year);
                CacheData(key, stats);
            }
            return stats;
        }

        /// <summary>
        /// Groups site members by month
        /// </summary>
        public static List<Group> GroupMembersByMonth(int year)
        {
            List<Group> stats = new List<Group>();
            string key = "Stats_GroupMembersByMonth_" + year;

            if (Cache[key] != null)
            {
                stats = (List<Group>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GroupMembersByMonth(year);
                CacheData(key, stats);
            }
            return stats;
        }
        
        /// <summary>
        /// Groups data By WaitTime
        /// </summary>
        public static List<WebStat> GroupByWaitTime(int year)
        {
            List<WebStat> stats = new List<WebStat>();
            string key = "Stats_GroupByWaitTime_" + year;

            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GroupByWaitTime(year);
                CacheData(key, stats);
            }
            return stats;
        }

        /// <summary>
        /// Groups data By VisitType
        /// </summary>
        public static List<WebStat> GroupByVisitType(int year)
        {
            List<WebStat> stats = new List<WebStat>();
            string key = "Stats_GroupByVisitType_" + year;

            if (Cache[key] != null)
            {
                stats = (List<WebStat>)Cache[key];
            }
            else
            {
                stats = DataAccess.WebStats.GroupByVisitType(year);
                CacheData(key, stats);
            }
            return stats;
        }

        /// <summary>
        /// Returns a Stat by statId
        /// </summary>
        public static WebStat GetStatByStatId(int statId)
        {
            WebStat stat = null;
            string key = "Stats_GetStatByStatId_" + statId;

            if (Cache[key] != null)
            {
                stat = (WebStat)Cache[key];
            }
            else
            {
                stat = DataAccess.WebStats.GetWebStatByWebStatId(statId);
                CacheData(key, stat);
            }
            return stat;
        }

        /// <summary>
        /// Deletes KeyWord By KeyWord
        /// </summary>
        public static bool DeleteKeyWordByKeyWord(string keyWord)
        {
            RemoveFromCache("Stats_GroupByKeyWord");
            RemoveFromCache("Stats_GroupByKeyWord_");
            bool ret = DataAccess.WebStats.DeleteKeyWordByKeyWord(keyWord);
            return ret;
        }

        /// <summary>
        /// Deletes KeyWord By KeyWordId
        /// </summary>
        public static bool DeleteKeyWordByKeyWordId(int keywordId)
        {
            RemoveFromCache("Stats_GroupByKeyWord");
            RemoveFromCache("Stats_GroupByKeyWord_");
            bool ret = DataAccess.WebStats.DeleteKeyWordByKeyWordId(keywordId);
            return ret;
        }

        /// <summary>
        /// Deletes Stat By StatId
        /// </summary>
        public static bool DeleteStatByStatId(int statId)
        {
            bool ret = DataAccess.WebStats.DeleteWebStatByWebStatId(statId);
            RemoveFromCache("Stats_GetAvailableYears");
            RemoveFromCache("Stats_GroupByVisitYear_");
            RemoveFromCache("Stats_GroupByVisitorAge_");
            RemoveFromCache("Stats_GroupByReason_");
            RemoveFromCache("Stats_GroupVisitsByDay_");
            RemoveFromCache("Stats_GroupByRegistry_");
            RemoveFromCache("Stats_VisitorStatus_");
            RemoveFromCache("Stats_GroupByWaitTime_");
            RemoveFromCache("Stats_GroupByVisitType_");
            return ret;
        }

        /// <summary>
        /// Inserts a new Stat
        /// </summary>
        public static int InsertRequest(WebStat stat)
        {
            int returnValue = DataAccess.WebStats.InsertWebStat(stat);
            RemoveFromCache("Stats_");
            return returnValue;
        }
        
        

        #endregion
    }
}