﻿namespace Dashboard.Common.Base
{
    public class UserGroup
    {
        public UserGroup() { }

        public UserGroup(string roleTitle, int countUsers)
        {
            RoleTitle = roleTitle;
            CountUsers = countUsers;
        }
        
        public string RoleTitle { get; set; }
        public int CountUsers{ get; set; }
    }
}