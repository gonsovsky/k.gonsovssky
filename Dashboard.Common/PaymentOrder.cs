﻿using System;
using System.Xml.Serialization;

namespace Dashboard.Common
{
    [Serializable()]
    [XmlType(AnonymousType = true)]
    public class PaymentOrder
    {
        /// <remarks/>
        [XmlAttribute("shopId")]
        public string ShopId { get; set; }

        /// <remarks/>
        [XmlAttribute("shopName")]
        public string ShopName { get; set; }

        /// <remarks/>
        [XmlAttribute("articleId")]
        public string ArticleId { get; set; }

        /// <remarks/>
        [XmlAttribute("articleName")]
        public string ArticleName { get; set; }

        /// <remarks/>
        [XmlAttribute("invoiceId")]
        public string InvoiceId { get; set; }

        /// <remarks/>
        [XmlAttribute("orderNumber")]
        public int OrderNumber { get; set; }

        /// <remarks/>
        [XmlAttribute("paymentSystemOrderNumber")]
        public string PaymentSystemOrderNumber { get; set; }

        /// <remarks/>
        [XmlAttribute("customerNumber")]
        public string CustomerNumber { get; set; }

        /// 9/>
        [XmlAttribute("createdDatetime")]
        public DateTime CreatedDatetime { get; set; }

        /// 10/>
        [XmlAttribute("paid")]
        public bool Paid { get; set; }

        /// <remarks/>
        [XmlAttribute("orderSumAmount")]
        public decimal OrderSumAmount { get; set; }

        /// <remarks/>
        [XmlAttribute("orderSumCurrencyPaycash")]
        public string OrderSumCurrencyPaycash { get; set; }

        /// <remarks/>
        [XmlAttribute("orderSumBankPaycash")]
        public string OrderSumBankPaycash { get; set; }

        /// <remarks/>
        [XmlAttribute("paidSumAmount")]
        public decimal PaidSumAmount { get; set; }

        /// <remarks/>
        [XmlAttribute("paidSumCurrencyPaycash")]
        public string PaidSumCurrencyPaycash { get; set; }

        /// <remarks/>
        [XmlAttribute("paidSumBankPaycash")]
        public string PaidSumBankPaycash { get; set; }

        /// <remarks/>
        [XmlAttribute("receivedSumAmount")]
        public decimal ReceivedSumAmount { get; set; }

        /// <remarks/>
        [XmlAttribute("receivedSumCurrencyPaycash")]
        public string ReceivedSumCurrencyPaycash { get; set; }

        /// 19 />
        [XmlAttribute("receivedSumBankPaycash")]
        public string ReceivedSumBankPaycash { get; set; }

        /// 20 />
        [XmlAttribute("shopSumAmount")]
        public decimal ShopSumAmount { get; set; }

        /// <remarks/>
        [XmlAttribute("shopSumCurrencyPaycash")]
        public string ShopSumCurrencyPaycash { get; set; }

        /// <remarks/>
        [XmlAttribute("shopSumBankPaycash")]
        public string ShopSumBankPaycash { get; set; }

        /// <remarks/>
        [XmlAttribute("paymentDatetime")]
        public DateTime PaymentDatetime { get; set; }

        /// <remarks/>
        [XmlAttribute("paymentAuthorizationTime")]
        public string PaymentAuthorizationTime { get; set; }

        /// <remarks/>
        [XmlAttribute("payerCode")]
        public string PayerCode { get; set; }

        /// <remarks/>
        [XmlAttribute("payerAddress")]
        public string PayerAddress { get; set; }

        /// <remarks/>
        [XmlAttribute("payeeCode")]
        public string PayeeCode { get; set; }

        /// <remarks/>
        [XmlAttribute("paymentSystemDatetime")]
        public DateTime PaymentSystemDatetime { get; set; }

        /// 29 />
        [XmlAttribute("avisoReceivedDatetime")]
        public DateTime AvisoReceivedDatetime { get; set; }

        /// 30 />
        [XmlAttribute("avisoStatus")]
        public string AvisoStatus { get; set; }

        /// <remarks/>
        [XmlAttribute("paymentType")]
        public string PaymentType { get; set; }

        /// <remarks/>
        [XmlAttribute("agentId")]
        public string AgentId { get; set; }

        /// <remarks/>
        [XmlAttribute("uniLabel")]
        public string UniLabel { get; set; }

        // 34
        public decimal OperatorFee { get; set; }
    }
}