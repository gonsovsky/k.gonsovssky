﻿using System;
using System.Collections.Generic;

namespace Dashboard.Common
{
    public class Order:Group
    {
        public int TicketId { get; set; }
        public int MemberId { get; set; }
        public int ControlDigit { get; set; }
        public int OrderId { get; set; }
        public decimal SaleComission { get; set; }
        public decimal BookComission { get; set; }
        public decimal ExtraCharge { get; set; }
        public int ComissionPercent { get; set; }
        public int ExtraChargePercent { get; set; }
        public bool FreeIssue { get; set; }
        public int DiscountCardId { get; set; }
        public decimal DecreaseDeposit { get; set; }
        public decimal Comission { get; set; }
        public decimal SalerCommision { get; set; }
        public Int64 AliasId { get; set; }
        public int TrackId { get; set; }
        
    }
}