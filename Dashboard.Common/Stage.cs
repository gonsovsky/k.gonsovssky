﻿using System;
using Newtonsoft.Json;

namespace Dashboard.Common
{
    public class Stage : Group
    {
        [JsonIgnore]
        public int ProviderId { get; set; }
        [JsonIgnore]
        public string Address { get; set; }
        [JsonIgnore]
        public string ImageMap { get; set; }
        [JsonIgnore]
        public int MapWidth { get; set; }
        [JsonIgnore]
        public int MapHeight { get; set; }
        [JsonIgnore]
        public double Longitude { get; set; }
        [JsonIgnore]
        public double Latitude { get; set; }
        [JsonIgnore]
        public int StageId { get; set; }
        [JsonIgnore]
        public int SideId { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int SeatId { get; set; }

        [JsonIgnore]
        public int XPos { get; set; }

        [JsonIgnore]
        public int YPos { get; set; }

        [JsonProperty(PropertyName = "side")]
        public string SideName { get; set; }

        [JsonProperty(PropertyName = "sector")]
        public string SectorName { get; set; }

        [JsonIgnore]
        public int SectorId { get; set; }

        [JsonProperty(PropertyName = "row")]
        public int RowNum { get; set; }

        [JsonProperty(PropertyName = "seat")]
        public string SeatNum { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "color")]
        public string TintColor { get; set; }

        //[JsonProperty(PropertyName = "price")]
        //public decimal SeatPrice { get; set; }

        [JsonProperty(PropertyName = "left")]
        public string LeftPos { get; set; }

        [JsonProperty(PropertyName = "top")]
        public string TopPos { get; set; }
    }
}