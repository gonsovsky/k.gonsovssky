﻿using System;
using System.Collections.Generic;

namespace Dashboard.Common
{
    public class Seat : Group
    {
        // Created for Seat Points
        public int MapId { get; set; }
        public int SeatId { get; set; }
        public string SeatNumber { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int SectionId { get; set; }
        public string SideName { get; set; }
        public string SectionName { get; set; }

        public int HallSideId { get; set; }
        public int UseRecord { get; set; }
        public int Visible { get; set; }
        public int StageId { get; set; }
        public int HallSectionId { get; set; }
        public string Seats { get; set; }
        public int RowNum { get; set; }
        public string SeatsMin { get; set; }
        public string SeatsMax { get; set; }
        public string SeatType { get; set; }
    }
}