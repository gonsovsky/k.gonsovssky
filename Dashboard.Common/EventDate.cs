﻿using System;

namespace Dashboard.Common
{
    public class EventDate : Group
    {
        private DateTime _actionDate = DateTime.MinValue;
        public DateTime ActionDate
        {
            get { return _actionDate; }
            set { _actionDate = value; }
        }

        public int StageId { get; set; }

        private string _eventInfo = string.Empty;
        public string EventInfo
        {
            get { return _eventInfo; }
            set { _eventInfo = value; }
        }


        private DateTime _endESaleDate = DateTime.MinValue;
        public DateTime EndESaleDate
        {
            get { return _endESaleDate; }
            set { _endESaleDate = value; }
        }

        /// <summary>
        /// Legacy ID
        /// </summary>
        public Int64 AliasId { get; set; }
        /// <summary>
        /// Legacy ActionID
        /// </summary>
        public int TrackId { get; set; }
    }
}