﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Dashboard.Common.Domain
{
    public class Schedule
    {
        public virtual int ScheduleId { get; set; }
        public virtual int HallId { get; set; }
        [JsonIgnore]
        public virtual DateTime StartDate { get; set; }
        public virtual string Date { get; set; }

        public virtual void Stringify()
        {
            Date = StartDate.Day + " " + StartDate.Month + " " + StartDate.ToString("t");
        }
    }
}