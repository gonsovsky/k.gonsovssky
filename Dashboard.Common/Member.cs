﻿using System;

namespace Dashboard.Common
{
    public class Member:Group
    {
        public int ParentId { get; set; }
        public int RoleId { get; set; }
        public string Pass { get; set; }
        public string Email { get; set; }

        private string _firstName = string.Empty;
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _middleName = string.Empty;
        public string MiddleName
        {
            get { return _middleName; }
            set { _middleName = value; }
        }

        private string _lastName = string.Empty;
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _phone = string.Empty;
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        private string _about = string.Empty;
        public string About
        {
            get { return _about; }
            set { _about = value; }
        }

        

        public int ProviderId { get; set; }
        public int StageId { get; set; }
    }
}