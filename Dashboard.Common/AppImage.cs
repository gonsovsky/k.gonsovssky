﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Common
{
    public class AppImage
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Название")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }
        public byte[] ImageBytes { get; set; }
    }
}