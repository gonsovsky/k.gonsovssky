﻿namespace Dashboard.Common
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Sort { get; set; }
        public int Visible { get; set; }
        public int ParentId { get; set; }
    }
}