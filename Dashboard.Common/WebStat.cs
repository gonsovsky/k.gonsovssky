﻿using System;

namespace Dashboard.Common
{
    public class WebStat : Group
    {
        // Request
        public int RequestId { get; set; }
        public int SessionId { get; set; }
        public string UserName { get; set; }
        public string Uri { get; set; }
        public string InputData { get; set; }
        public string OutputData { get; set; }
        public string HttpMethod { get; set; }
        public string UrlReferrer { get; set; }
        public string UrlReferrerHost { get; set; }
        public DeviceType DeviceType { get; set; }
        

        public int KeyWordId { get; set; }
        public int SiteId { get; set; }
        public string IPAddress { get; set; }
        public string UserAgent { get; set; }
        public int BotId { get; set; }
        public int PageId { get; set; }
        public string PageUrl { get; set; }
        public string KeyWord { get; set; }
        public string BotName { get; set; }
    }
}