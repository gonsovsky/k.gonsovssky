﻿using System;

namespace Dashboard.Common
{
    [AttributeUsage(AttributeTargets.All)]
    public class DataAttribute : Attribute
    {
        public string Name { get; set; }
        public DataAttribute()
        {
            
        }

        public DataAttribute(string name): this()
        {
            Name = name;
        }
    }
}