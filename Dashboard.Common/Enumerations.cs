﻿namespace Dashboard.Common
{
    public enum ImageType
    {
        User,
        Action,
        Form,
        Map
    }

    public enum ItemStatus
    {
        [StringValue("Неопределен")]
        None = 0,
        [StringValue("В продаже")]
        OnSale = 1,
        [StringValue("Заблокирован")]
        Locked = 2,
        [StringValue("Зарезервирован")]
        Reserved = 3,
        [StringValue("Продан")]
        Sold = 4,
        [StringValue("Списан")]
        Removed = 5,
        [StringValue("Возвращен")]
        Returned = 6,
        [StringValue("Аннулирован")]
        Cancelled = 7,
        [StringValue("На реализации")]
        OnRetail = 8,
        [StringValue("На складе")]
        OnStock = 9,
        [StringValue("Удален")]
        Deleted = 10,
        [StringValue("Распечатан")]
        Printed = 11,
        [StringValue("Изменена дата резерва")]
        ReserveDateChanged = 12,
        [StringValue("Изменена бронь")]
        BookingChanged = 13,
        [StringValue("В печать")]
        ToBePrinted = 14,
        [StringValue("Действительный")]
        Valid = 15
    }

    public enum DeviceType
    {
        /// <summary>
        /// Devise type is undefined
        /// </summary>
        [StringValue("Данные отсутствуют")]
        None = 0,
        /// <summary>
        /// Devise type is unknown type
        /// </summary>
        [StringValue("Неизвестное устройство")]
        Unknown = 1,
        [StringValue("Андроид")]
        Android = 2,
        [StringValue("Windows")]
        Windows = 3,
        [StringValue("iPhone")]
        ApplePhone = 4,
        [StringValue("iPad")]
        ApplePad = 5,
        [StringValue("Macintosh")]
        Macintosh = 6
    }
}