﻿using System;

namespace Dashboard.Common
{
    /// <summary>
    /// http://stackoverflow.com/questions/152774/is-there-a-better-way-to-trim-a-datetime-to-a-specific-precision
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Usage:
        /// DateTime.Now.Trim(TimeSpan.TicksPerDay);
        /// DateTime.Now.Trim(TimeSpan.TicksPerHour);
        /// DateTime.Now.Trim(TimeSpan.TicksPerMillisecond);
        /// DateTime.Now.Trim(TimeSpan.TicksPerMinute);
        /// DateTime.Now.Trim(TimeSpan.TicksPerSecond);
        /// </summary>
        /// <param name="date"></param>
        /// <param name="roundTicks"></param>
        /// <returns></returns>
        public static DateTime Trim(this DateTime date, long roundTicks)
        {
            return new DateTime(date.Ticks - date.Ticks % roundTicks);
        }

        public static DateTime FloorToDay(this DateTime dt)
        {
            return dt.AddTicks(-1 * (dt.Ticks % TimeSpan.TicksPerDay));
        }
    }
}