﻿using System;
using System.Xml.Serialization;

namespace Dashboard.Common
{
    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "returnPaymentRequest", Namespace = "", IsNullable = false)]
    public class ReturnPaymentRequest
    {
        /// <summary>
        /// Уникальный идентификатор операции возврата. Рекомендуемые значения: целое, положительное, линейно нарастающее десятичное число
        /// </summary>
        [XmlAttribute("clientOrderId")]
        public string ClientOrderId { get; set; }

        /// <summary>
        /// Время формирования запроса на выполнение операции по часам ИС Контрагента.
        /// </summary>
        [XmlAttribute("requestDT")]
        public string RequestDate { get; set; }

        /// <summary>
        /// Номер транзакции возвращаемого перевода.
        /// </summary>
        [XmlAttribute("invoiceId")]
        public string InvoiceId { get; set; }

        /// <summary>
        /// Идентификатор Контрагента, присвоенный Оператором.
        /// </summary>
        [XmlAttribute("shopId")]
        public string ShopId { get; set; }

        /// <summary>
        /// Сумма, которую необходимо вернуть на счет плательщика.
        /// </summary>
        [XmlAttribute("amount")]
        public string Amount { get; set; }

        /// <summary>
        /// Код валюты возвращаемого перевода.
        /// </summary>
        [XmlAttribute("currency")]
        public string Currency { get; set; }

        /// <summary>
        /// Описание причины возврата.
        /// </summary>
        [XmlAttribute("cause")]
        public string Cause { get; set; }
    }
}