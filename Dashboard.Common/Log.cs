﻿using System;

namespace Dashboard.Common
{
    public class Log:Group
    {
        public int MemberId { get; set; }
        public int TicketId { get; set; }
        public string LogText { get; set; }
    }
}