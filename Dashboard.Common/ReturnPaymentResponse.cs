﻿using System;
using System.Xml.Serialization;

namespace Dashboard.Common
{
    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "returnPaymentResponse", Namespace = "", IsNullable = false)]
    public class ReturnPaymentResponse
    {
        /// <summary>
        /// Уникальный идентификатор операции возврата.
        /// </summary>
        [XmlAttribute("clientOrderId")]
        public string ClientOrderId { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [XmlAttribute("status")]
        public string Status { get; set; }

        /// <summary>
        /// Error
        /// </summary>
        [XmlAttribute("error")]
        public string Error { get; set; }

        /// <summary>
        /// Опциональное поле. Может содержать дополнительный поясняющий текст к ответам сервера.
        /// </summary>
        [XmlAttribute("techMessage")]
        public string TechMessage { get; set; }

        /// <summary>
        /// Время выполнения запроса
        /// </summary>
        [XmlAttribute("processedDT")]
        public DateTime ProcessedDate { get; set; }
    }
}