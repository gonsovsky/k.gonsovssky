﻿using System;

namespace Dashboard.Common
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class RestAttribute : Attribute
    {
        //public SessionStateBehavior SessionMode { get; set; }

        //public string ContentType { get; set; }
        //public HttpMethod HttpMethod { get; set; }
        //public string ContentDisposition { get; set; }
        public string Description { get; set; }

        //public JsonSerializer JsonSerializer { get; set; }

        public RestAttribute()
        {
            //SessionMode = SessionStateBehavior.Disabled;
            //ContentType = "text/html";
            //ContentDisposition = string.Empty;
            //JsonSerializer = JsonSerializer.JavaScriptSerializer;
        }

        public RestAttribute(string description): this()
        {
            Description = description;
        }

        //public RestAttribute(string description, JsonSerializer jsonSerializer)
        //    : this()
        //{
        //    Description = description;
        //    JsonSerializer = jsonSerializer;
        //}

        //public RestAttribute(string description, JsonSerializer jsonSerializer, SessionStateBehavior sessionMode)
        //    : this()
        //{
        //    Description = description;
        //    JsonSerializer = jsonSerializer;
        //    SessionMode = sessionMode;
        //}

        //public RestAttribute(SessionStateBehavior sessionMode)
        //    : this()
        //{
        //    SessionMode = sessionMode;
        //}
    }
}