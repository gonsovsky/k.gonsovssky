﻿using System;
using System.Collections.Generic;

namespace Dashboard.Common
{
    public class Item
    {
        public Item()
        {
            Dates = new List<Group>();
            Group = new Group();
        }
        public List<Group> Dates { get; set; }
        public Group Group { get; set; }
    }
}