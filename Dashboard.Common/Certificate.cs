﻿using System;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;

namespace Dashboard.Common
{
    public class Certificate
    {
        public string Name { get; set; }
        public string ThumbPrint { get; set; }
        public string Password { get; set; }
        public byte[] CertBytes { get; set; }

        public byte[] GetBytes()
        {
            byte[] cb = null;
            X509Certificate2 cert = GetFromStore();
            cb = cert.Export(X509ContentType.Pfx, Password);
            if (cb == null)
            {
                throw new ConfigurationErrorsException("Certificate not found");
            }
            return cb;
        }

        private X509Certificate2 GetFromStore()
        {
            X509Store store1 = new X509Store(StoreName.CertificateAuthority, StoreLocation.CurrentUser);
            store1.Open(OpenFlags.MaxAllowed);
            X509Store store2 = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store2.Open(OpenFlags.MaxAllowed);
            X509Certificate2Collection certificates = store1.Certificates;
            certificates.AddRange(store2.Certificates);

            if (certificates.Count > 0)
            {
                for (int i = 0; i < certificates.Count; i++)
                {
                    X509Certificate2 certificate = certificates[i];
                    if (String.Compare(certificate.Thumbprint, ThumbPrint, StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        return certificates[i];
                    }
                }
            }
            return null;
        }
    }
}