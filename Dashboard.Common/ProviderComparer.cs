﻿using System;
using System.Collections.Generic;

namespace Dashboard.Common
{
    public class GroupComparer : IEqualityComparer<Group>
    {
        private Func<Group, object> KeySelector { get; set; }

        public GroupComparer(Func<Group, object> expr)
        {
            KeySelector = expr;
        }

        public bool Equals(Group x, Group y)
        {
            return KeySelector(x).Equals(KeySelector(y));
        }

        public int GetHashCode(Group obj)
        {
            return KeySelector(obj).GetHashCode();
        }
    }
}