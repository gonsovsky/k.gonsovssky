﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace Dashboard.Common
{
    public static class ObjectEx
    {
        public static byte[] SerializeToXmlBytes<T>(this T value)
        {
            try
            {
                var xmlserializer = new XmlSerializer(typeof(T));
                using (var memStream = new MemoryStream())
                {
                    xmlserializer.Serialize(memStream, value);
                    return memStream.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred SerializeToXmlBytes", ex);
            }
        }

        public static string SerializeToXml<T>(this T value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            try
            {
                var xmlserializer = new XmlSerializer(typeof(T));
                var stringWriter = new StringWriter();
                using (var writer = XmlWriter.Create(stringWriter))
                {
                    xmlserializer.Serialize(writer, value);
                    return stringWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred SerializeToXml", ex);
            }
        }

        public static string ToQueryString(this object request, string separator = ",")
        {
            if (request == null)
                throw new ArgumentNullException("request");
           
            // Get all properties on the object
            var properties = request.GetType().GetProperties()
                .Where(x => x.CanRead)
                .Where(x => x.GetValue(request, null) != null)
                .ToDictionary(x => x.Name, x => x.GetValue(request, null));

            // Get names for all IEnumerable properties (excl. string)
            //var propertyNames = properties
            //    .Where(x => !(x.Value is string) && x.Value is IEnumerable)
            //    .Select(x => x.Key)
            //    .ToList();

            // Concat all IEnumerable properties into a comma separated string
            //foreach (var key in propertyNames)
            //{
            //    var valueType = properties[key].GetType();
            //    var valueElemType = valueType.IsGenericType
            //                            ? valueType.GetGenericArguments()[0]
            //                            : valueType.GetElementType();
            //    if (valueElemType.IsPrimitive || valueElemType == typeof(string))
            //    {
            //        var enumerable = properties[key] as IEnumerable<>;
            //        properties[key] = string.Join(separator, enumerable.Cast<object>());
            //    }
            //}

            // Concat all key/value pairs into a string separated by ampersand
            return string.Join("&", properties.Select(x => string.Concat(Uri.EscapeDataString(x.Key), "=",Uri.EscapeDataString(x.Value.ToString()))));
        }
 
    }
}