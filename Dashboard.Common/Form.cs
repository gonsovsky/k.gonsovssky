﻿using System.Drawing;

namespace Dashboard.Common
{
    public class Form
    {
        public int Id { get; set; }
        public int FormTypeId { get; set; }
        public int FormId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string ImageUrl { get; set; }
        public string FontFamily { get; set; }
        public FontStyle FontStyle { get; set; }
        public Color FontColor { get; set; }
        public StringFormatFlags StringFormat { get; set; }
        public int FontSize { get; set; }
        public int XPos { get; set; }
        public int YPos { get; set; }
    }
}