﻿using System;

namespace Dashboard.Common
{
    public class Ticket : Group
    {
        /// <summary>
        /// Identifier of an event. EventId is a foreign key for EventDates table 
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Identifier of an seat.  
        /// </summary>
        public int SeatId { get; set; }

        public DateTime ActionDate { get; set; }

        private string _rowNum = string.Empty;

        public string RowNum
        {
            get { return _rowNum; }
            set { _rowNum = value; }
        }

        private string _seatNum = string.Empty;

        public string SeatNum
        {
            get { return _seatNum; }
            set { _seatNum = value; }
        }

        

        private string _externalBarCode = string.Empty;

        public string ExternalBarCode
        {
            get { return _externalBarCode; }
            set { _externalBarCode = value; }
        }


        public int ReservType { get; set; }
        public int ReservAmount { get; set; }
        public DateTime ReservedTill { get; set; }
        public int ProhibitPrint { get; set; }
        public int SaleRuleId { get; set; }
        public int MultiStatus { get; set; }
        public int ExternalActionTicketId { get; set; }
        public Int64 AliasId { get; set; }
        public int TrackId { get; set; }
    }
}