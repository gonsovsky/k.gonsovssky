﻿using System;
using Newtonsoft.Json;

namespace Dashboard.Common
{
    [Serializable]
    public class Group : IComparable<Group>
    {
        public Group()
        {
        }

        public override int GetHashCode()
        {
            return Id;
        }


        public Group(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public Group(int id, string name, int count)
        {
            Id = id;
            Name = name;
            Count = count;
        }

        public Group(int id, string name, int count, decimal price)
        {
            Id = id;
            Name = name;
            Count = count;
            Price = price;
        }

        [JsonIgnore]
        public int Id { get; set; }

        [JsonIgnore]
        public int ActionId { get; set; }

        [JsonIgnore]
        public string Name { get; set; }

        [JsonIgnore]
        public string Month { get; set; }

        private string _barCode = string.Empty;
        [JsonIgnore]
        public string BarCode
        {
            get { return _barCode; }
            set { _barCode = value; }
        }

        private string _description = string.Empty;
        [JsonIgnore]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Ticket StatusId
        /// </summary>
        [JsonIgnore]
        public ItemStatus StatusId { get; set; }

        /// <summary>
        /// Counted in particularly context
        /// </summary>
        [JsonIgnore]
        public int Count { get; set; }

        /// <summary>
        /// Counted in total is always bigger than Count
        /// </summary>
        [JsonIgnore]
        public int TotalCount { get; set; }

        /// <summary>
        /// Calculation result
        /// </summary>
        [JsonIgnore]
        public int CountResult { get; set; }

        /// <summary>
        /// Price in the particular context
        /// </summary>
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }

        /// <summary>
        /// AveragePrice
        /// </summary>
        [JsonIgnore]
        public decimal AveragePrice { get; set; }

        /// <summary>
        /// Priced in total is always bigger than Price
        /// </summary>
        [JsonIgnore]
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Calculation result
        /// </summary>
        [JsonIgnore]
        public decimal PriceResult { get; set; }

        /// <summary>
        /// Calculation percent
        /// </summary>
        [JsonIgnore]
        public int Percent { get; set; }

        [JsonIgnore]
        public DateTime StartDate { get; set; }

        [JsonIgnore]
        public DateTime EndDate { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }


        public static string GetWeekDay(DateTime dt)
        {
            DayOfWeek dow = dt.DayOfWeek;
            switch (dow)
            {
                case DayOfWeek.Monday:
                    return "Понедельник";
                case DayOfWeek.Tuesday:
                    return "Вторник";
                case DayOfWeek.Wednesday:
                    return "Среда";
                case DayOfWeek.Thursday:
                    return "Четверг";
                case DayOfWeek.Friday:
                    return "Пятница";
                case DayOfWeek.Saturday:
                    return "Суббота";
                case DayOfWeek.Sunday:
                    return "Воскресенье";

            }
            return string.Empty;
        }

        /// <summary>
        /// Gets Month Name By Index
        /// </summary>
        public static string GetMonthName(int i)
        {
            switch (i)
            {
                case 0:
                    return "Все месяцы";
                case 1:
                    return "Январь";
                case 2:
                    return "Февраль";
                case 3:
                    return "Март";
                case 4:
                    return "Апрель";
                case 5:
                    return "Май";
                case 6:
                    return "Июнь";
                case 7:
                    return "Июль";
                case 8:
                    return "Август";
                case 9:
                    return "Сентябрь";
                case 10:
                    return "Октябрь";
                case 11:
                    return "Ноябрь";
                case 12:
                    return "Декабрь";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public int CompareTo(Group other)
        {
            if (Price > other.Price) return -1;
            if (Price == other.Price) return 0;
            return 1;
        }
    }
}