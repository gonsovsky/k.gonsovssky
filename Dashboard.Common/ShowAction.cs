﻿using System;
using System.Xml.Serialization;

namespace Dashboard.Common
{
    public class ShowAction : Group
    {
        public int ProviderId { get; set; }

        private string _info = string.Empty;
        public string Info
        {
            get { return _info; }
            set { _info = value; }
        }
        public string AgeLimit { get; set; }
        public int AliasId { get; set; }
    }
}