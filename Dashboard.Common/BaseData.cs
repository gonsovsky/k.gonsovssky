﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using NLog;

namespace Dashboard.Common
{
    public abstract class BaseData
    {
        public static Logger Logger = LogManager.GetCurrentClassLogger();

        protected static ObjectCache Cache
        {
            get { return MemoryCache.Default; }
        }

        ObjectCache cache = MemoryCache.Default;

        /// <summary>
        /// Remove from cache all items whose key starts with the input prefix
        /// </summary>
        protected static void RemoveFromCache(string key)
        {
            key = key.ToLower();
            List<string> items = new List<string>();

            items = Cache.Select(kvp => kvp.Key).ToList();

            foreach (string item in items)
            {
                if (item.ToLower().StartsWith(key))
                {
                    Cache.Remove(item);
                }
            }
        }

        public static void ClearCache()
        {
            var allKeys = Cache.Select(o => o.Key);
            Parallel.ForEach(allKeys, key => Cache.Remove(key));
        }

        /// <summary>
        /// Cache the input data
        /// </summary>
        protected static void CacheData(string key, object data)
        {
            if (data != null)
            {
                Cache.Add(key, data, DateTime.Now.AddDays(1), null);
            }
        }

        protected static int GetPageIndex(int pageIndex, int pageSize)
        {
            if (pageSize <= 0)
                return 0;
            else
                return (int)Math.Floor((double)pageIndex / (double)pageSize);
        }

        public static string GetMonthName(int month)
        {
            switch (month)
            {
                case 1:
                    return "Январь";
                case 2:
                    return "Февраль";
                case 3:
                    return "Март";
                case 4:
                    return "Апрель";
                case 5:
                    return "Май";
                case 6:
                    return "Июнь";
                case 7:
                    return "Июль";
                case 8:
                    return "Август";
                case 9:
                    return "Сентябрь";
                case 10:
                    return "Октябрь";
                case 11:
                    return "Ноябрь";
                case 12:
                    return "Декабрь";

            }
            return string.Empty;
        }

        public static string GetShortMonthName(int month)
        {
            switch (month)
            {
                case 1:
                    return "янв";
                case 2:
                    return "фев";
                case 3:
                    return "мар";
                case 4:
                    return "апр";
                case 5:
                    return "май";
                case 6:
                    return "июн";
                case 7:
                    return "июл";
                case 8:
                    return "авг";
                case 9:
                    return "сен";
                case 10:
                    return "окт";
                case 11:
                    return "ноя";
                case 12:
                    return "дек";

            }
            return string.Empty;
        }

        public static string GetWeekDay(DateTime dt)
        {
            DayOfWeek dow = dt.DayOfWeek;
            switch (dow)
            {
                case DayOfWeek.Monday:
                    return "Понедельник";
                case DayOfWeek.Tuesday:
                    return "Вторник";
                case DayOfWeek.Wednesday:
                    return "Среда";
                case DayOfWeek.Thursday:
                    return "Четверг";
                case DayOfWeek.Friday:
                    return "Пятница";
                case DayOfWeek.Saturday:
                    return "Суббота";
                case DayOfWeek.Sunday:
                    return "Воскресенье";

            }
            return string.Empty;
        }
    }
}