﻿using System;

namespace Dashboard.Common
{
    public class Payment
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int OrderId { get; set; }
        public int TicketId { get; set; }
        public string InvoiceId { get; set; }
        public decimal Amount { get; set; }
        public string Reason { get; set; }
        public DateTime RequestDate { get; set; }

        public string PaymentStatus { get; set; }
        public string Error { get; set; }
        public string TechMessage { get; set; }
        public DateTime ResponseDate { get; set; }
    }
}