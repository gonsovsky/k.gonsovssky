﻿using System.Collections.Generic;

namespace Dashboard.Common
{
    public class ItemModel
    {
        public ItemModel()
        {
            Items = new List<Item>();
        }
        public List<Item> Items { get; set; }
        
    }
}