﻿using System;
using Dashboard.Common.Base;

namespace Dashboard.Common
{
    public class Provider : Group
    {
        public string Address { get; set; }
        public int CityId { get; set; }
        public int AliasId { get; set; }
        public int TrackId { get; set; }
    }
}