﻿<%@ Page Language="C#" Title="Нарастающий итог" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master" CodeBehind="TicketTotals.aspx.cs" Inherits="Dashboard.Web.TicketTotals" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">
    
    
    <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
            <h2>Нарастающий итог</h2>
            <p><asp:Literal ID="ltrTitle" runat="server"></asp:Literal></p>                
        </div>
    <div class="row">
            <div class="col-md-3">
                <p>Выберите год:</p>
                <asp:DropDownList ID="ddlYears" Width="200px" runat="server" AutoPostBack="True"
                    DataSourceID="odsYears" class="btn btn-default" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="odsYears" runat="server" SelectMethod="GetAvailableYears"
                    TypeName="Dashboard.BLL.ShowActions"></asp:ObjectDataSource>
            </div>
            <div class="col-md-8">
                <p>Выберите цирк:</p>
                <asp:DropDownList ID="ddlProviders" Width="400px" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="Id"
                    class="btn btn-default" OnSelectedIndexChanged="ddlProviders_SelectedIndexChanged">
                </asp:DropDownList>

                 </div>
        </div>
       
       <br/>
       
            <asp:GridView ID="gdvTickets" CssClass="grid-view" OnRowDataBound="gdvTickets_OnRowDataBound" OnSorting="gdvTickets_OnSorting" runat="server" DataSourceID="odsTickets" 
        DataKeyNames="Id" EnableViewState="False" ShowFooter="True" AutoGenerateColumns="False" PageSize="100" PagerSettings-Position="TopAndBottom" AllowSorting="True" 
        PagerSettings-Mode="Numeric" PagerSettings-PageButtonCount="10" AllowPaging="True">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>C</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrStartDate" Text='<%# Eval("StartDate", "{0:D}") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>По</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrEndDate" Text='<%# Eval("EndDate", "{0:D}") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>Продано, билетов</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrCount" Text='<%# Eval("Count") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Итого, билетов</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrCountResult" Text='<%# Eval("CountResult") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Продано, рублей</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrPrice" Text='<%# "+ " + Eval("Price", "{0:C0}") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>Итого, рублей</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrPriceResult" Text='<%# Eval("PriceResult", "{0:C0}") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                <h4><asp:Literal ID="ltrEmptyData" runat="server" Text="Нет"></asp:Literal></h4>
            </EmptyDataTemplate>
            <HeaderStyle CssClass="th-row-style" />
            <RowStyle CssClass="row-style" />
            <AlternatingRowStyle CssClass="alt-row-style" />
            <PagerStyle CssClass="pager-style"/>
            <FooterStyle CssClass="footer-style" />
        </asp:GridView>

        <asp:ObjectDataSource ID="odsTickets" runat="server" SelectMethod="RunningTotals" TypeName="Dashboard.BLL.Tickets">
           <SelectParameters>
               <asp:ControlParameter ControlID="ddlProviders" PropertyName="SelectedValue" DefaultValue="0" Name="providerId" Type="Int32" />
               <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
               <asp:ControlParameter ControlID="ddlYears" PropertyName="SelectedValue" DefaultValue="0" Name="year" Type="Int32" />
           </SelectParameters>
        </asp:ObjectDataSource>
        <br/>
         <br/>

   <script type="text/javascript">
       //alert("You clicked!");
       $('.grid-view').on('click', function (e) {

           //e.preventDefault();
           alert("You clicked!");
       });
//       $('.grid-view').on('click', function () {
//           alert("You clicked!");
//       });
   </script>
</asp:Content>
