﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;

namespace Dashboard.Web
{
    public partial class EventDates : System.Web.UI.Page
    {
        private int year;
        private int cellIndex = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                DateTime dt = DateTime.Now;
                year = dt.Year;
                ddlYears.DataBind();
                ddlYears.SelectedValue = year.ToString();

                if (Context.Request.QueryString["stageId"] != null)
                {
                    string actId = Context.Request.QueryString["stageId"];
                    //if (Int32.TryParse(actId, out stageId))
                    //{
                    //    Stage stage = BLL.Stages.GetStageByStageId(stageId);
                    //    //stage.CreatedDate.ToString("F")
                    //    ltrTitle.Text = string.Format("Даты выступлений \"{0}\"", stage.StageName);
                    //    //ltrCount.Text = string.Format("Всего <b>{0}</b> билетов", BLL.Tickets.CountTickets(providerId).ToString("#### ###"));
                    //}
                }
            }
        }

        protected void DataBindCharts(int actionId, int year)
        {
            //List<Group> groups = BLL.EventDates.GroupEventDatesOnMonths(actionId, 3, year);

            //providers = BLL.Stages.GroupProviderOnMonths(0, 3, year);
            //groups = providers.Distinct<Group>(new GroupComparer(x => x.Id)).ToList();
            //DataBindControls();
            //ddlProviders.DataSource = groups;
            //ddlProviders.DataBind();
            //ListItem listItem = new ListItem();
            //listItem.Text = "Все цирки";
            //listItem.Value = "0";
            //ddlProviders.Items.Insert(0, listItem);

            //foreach (Group p in groups)
            //{
                
            //        Series s = new Series(p.Name);
            //        s.ChartType = SeriesChartType.Spline;
            //        s.BorderWidth = 3;
            //        s.ShadowColor = Color.Gray;
            //        s.ShadowOffset = 2;
            //        s.LegendText = p.Name;
            //        s.LabelToolTip = p.Name;
            //        s.Points.AddY(0);
            //        foreach (Group m in providers)
            //        {
            //            if (m.Id == p.Id)
            //            {
            //                s.Points.AddY(m.Price);
            //            }

            //        }
            //        chtProviders.Series.Add(s);
                
            //}

            //chtProviders.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(new CustomLabel(0, 1, "", 0,
            //    LabelMarkStyle.None, GridTickTypes.None));
            //groups = providers.Distinct<Group>(new GroupComparer(x => x.Month)).ToList();
            //groups = groups.OrderBy(x => x.MonthId).ToList();

            //for (int i = 0; i < groups.Count(); i++)
            //{
            //    CustomLabel cl = new CustomLabel(
            //        i + 1, i + 2, groups[i].Month, 0, LabelMarkStyle.None, GridTickTypes.None);
            //    chtProviders.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(cl);
            //}


        }

        private int count = 0;
        private decimal price = 0;
        private decimal averigePrice = 0;

        protected void gdvEventDates_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRow row = ((DataRowView)e.Row.DataItem).Row;
                if (row != null)
                {
                    count += (int)row.ItemArray[2];
                    price += (decimal)row.ItemArray[3];
                    if (averigePrice > 0)
                    {
                        averigePrice = ((averigePrice + (decimal)row.ItemArray[4]) / 2);
                    }
                    else
                    {
                        averigePrice = (decimal)row.ItemArray[4];
                    }
                }

                if (cellIndex > -1)
                {
                    e.Row.Cells[cellIndex].Font.Bold = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                
                e.Row.Cells[0].Text = "Итого:";
                e.Row.Cells[1].Text = count.ToString("### ### ###");
                e.Row.Cells[2].Text = price.ToString("C0");
                e.Row.Cells[3].Text = averigePrice.ToString("C0");
            }
        }

        protected void gdvEventDates_OnSorting(object sender, GridViewSortEventArgs e)
        {
            foreach (var column in gdvEventDates.Columns)
            {
                DataControlField dcf = (DataControlField)column;
                if (dcf.SortExpression == e.SortExpression)
                {
                    cellIndex = gdvEventDates.Columns.IndexOf(dcf);
                }
            }
        }


        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            //DataBindCharts(year);
        }
    }
}