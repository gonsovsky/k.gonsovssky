﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Security;
using Dashboard.Reports;

namespace Dashboard.Web
{
    /// <summary>
    /// Summary description for ReportHandler
    /// </summary>
    public class ReportHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            NameValueCollection nvc = context.Request.QueryString;
            string fileName = nvc["type"];
            if (context.Request.IsAuthenticated)
            {
                //bool fg = true;
                string userName = context.User.Identity.Name;
                Roles.GetRolesForUser(userName);
                if(context.User.IsInRole("Reporters"))
                {
                    
                }
            }
            context.Response.Expires = -1;
            context.Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            context.Response.AppendHeader("content-disposition", "attachment; filename=" + fileName + ".xlsx");
            context.Response.BinaryWrite(BaseReport.Instance.CreateReport(nvc));
            //context.Response.OutputStream.Write();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}