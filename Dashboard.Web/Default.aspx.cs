﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;
using Dashboard.Common.Base;

namespace Dashboard.Web
{
    public partial class DefaultPage : System.Web.UI.Page
    {
        private List<Group> providers;
        private List<Group> groups;

        protected int ProviderId
        {
            get
            {
                if (ViewState["ProviderId"] != null)
                {
                    return (int)ViewState["ProviderId"];
                }
                else
                {
                    return 0;
                }
            }
            set { ViewState["ProviderId"] = value; }
        }
 
        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            int year = dt.Year;
            if (!IsPostBack)
            {
                ddlYears.DataBind();
                ddlYears.SelectedValue = year.ToString();
                DataBindControls(year);
                DataBindCharts(ProviderId, year);
                
            }
        }

        protected void DataBindControls(int year)
        {
            providers = BLL.Stages.GroupProviderOnMonths(0, 3, year);
            groups = providers.Distinct<Group>(new GroupComparer(x => x.Id)).ToList();
            ddlProviders.DataSource = groups.OrderBy(x => x.GetType().GetProperty("Name").GetValue(x, null));
            ddlProviders.DataBind();
            ListItem li = ddlProviders.Items.FindByValue(ProviderId.ToString());
            if (ddlProviders.Items.FindByValue(ProviderId.ToString()) != null)
            {
                ddlProviders.SelectedValue = ProviderId.ToString();
            }
            else
            {
                ProviderId = 0;
            }
            ListItem listItem = new ListItem();
            listItem.Text = "Все цирки";
            listItem.Value = "0";
            ddlProviders.Items.Insert(0, listItem);
            chtProviders.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "C0";
            chtProviders.ChartAreas["ChartArea1"].AxisX.LabelStyle.IsEndLabelVisible = true;
        }

        protected void DataBindCharts(int providerId, int year)
        {
            SeriesChartType chartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), ddlChartType.SelectedItem.Text, true);

            foreach (Group p in groups)
            {
                if (providerId == 0 || p.Id == providerId)
                {
                    Series s = new Series(p.Name);
                    s.IsValueShownAsLabel = chbValues.Checked;
                    s.LabelFormat = "C0";
                    s.ChartType = chartType;
                    s.BorderWidth = 3;
                    s.ShadowColor = Color.Gray;
                    s.ShadowOffset = 2;
                    s.LegendText = p.Name; 
                    s.ToolTip = p.Name;
                    s.LabelToolTip = p.Name;
                    s.Points.AddY(0);
                    foreach (Group m in providers)
                    {
                        if (m.Id == p.Id)
                        {
                            s.Points.AddY(m.Price);
                        }

                    }
                    chtProviders.Series.Add(s);
                }
            }

            chtProviders.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(new CustomLabel(0, 1, "", 0,
                LabelMarkStyle.None, GridTickTypes.None));
            groups = providers.Distinct<Group>(new GroupComparer(x => x.Month)).ToList();
            groups = groups.OrderBy(x => x.ActionId).ToList();

            for (int i = 0; i < groups.Count(); i++)
            {
                CustomLabel cl = new CustomLabel(
                    i + 1, i + 2, groups[i].Month, 0, LabelMarkStyle.None, GridTickTypes.None);
                chtProviders.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(cl);
            }


        }

        protected void ddlProviders_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProviderId = Convert.ToInt32(ddlProviders.SelectedValue);
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindControls(year);
            DataBindCharts(ProviderId, year);
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindControls(year);
            DataBindCharts(ProviderId, year);
        }

        protected void chbValues_CheckedChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindControls(year);
            DataBindCharts(ProviderId, year);
        }

        protected void ddlChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProviderId = Convert.ToInt32(ddlProviders.SelectedValue);
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindControls(year);
            DataBindCharts(ProviderId, year);
        }
    }
}