﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master"
    CodeBehind="TicketPrice.aspx.cs" Inherits="Dashboard.Web.TicketPrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="Server">
    <div class="container">
        <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
            <h2>Динамика средней цены на билеты</h2>            
        </div>
        <div class="row">
            <div class="col-md-3">
                <p>Выберите год:</p>
                <asp:DropDownList ID="ddlYears" Width="200px" runat="server" AutoPostBack="True"
                    DataSourceID="odsYears" class="btn btn-default" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="odsYears" runat="server" SelectMethod="GetAvailableYears"
                    TypeName="Dashboard.BLL.ShowActions"></asp:ObjectDataSource>
            </div>
            <div class="col-md-4">
                     <p>Выберите цирк:</p>
                <asp:DropDownList ID="ddlProviders" Width="240px" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="Id"
                    class="btn btn-default" OnSelectedIndexChanged="ddlProviders_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-2">
                <p>Значения:</p>
                <asp:CheckBox ID="chbValues" AutoPostBack="True" runat="server" OnCheckedChanged="chbValues_CheckedChanged" />
           </div>
           <div class="col-md-3">
                     <p>Тип графика:</p>
                <asp:DropDownList ID="ddlChartType" Width="200px" runat="server" AutoPostBack="True" 
                    class="btn btn-default" OnSelectedIndexChanged="ddlChartType_SelectedIndexChanged">
                    <asp:ListItem Selected="True">StepLine</asp:ListItem>
                    <asp:ListItem>Spline</asp:ListItem>
                    <asp:ListItem>Line</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        
        <asp:Chart ID="chtProviders" runat="server" DataSourceID="odsDates" ImageLocation="Temp/ChartPic_#SEQ(290,3)"
            Height="500px" Width="1024px" BorderWidth="0px" BackColor="#FFFFFF" BackGradientStyle="None"
            BorderColor="Transparent" BorderlineDashStyle="NotSet">
            <Legends>
                <asp:Legend Enabled="True" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                    BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold">
                </asp:Legend>
            </Legends>
            <BorderSkin SkinStyle="None"></BorderSkin>
            <Series>
                <asp:Series Name="Средняя цена" ChartType="Line" XValueMember="StartDate" XValueType="Date"
                    YValueMembers="AveragePrice" YValueType="Double" ShadowColor="Black" BorderColor="180, 26, 59, 105"
                    BorderWidth="2" LabelFormat="C0" LabelForeColor="#000000" Color="224, 64, 10">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BackColor="#FFFFFF">
                    <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="False">
                        <LabelStyle Font="Trebuchet MS, 10pt" />
                        <MajorGrid LineColor="64, 64, 64, 64" />
                    </AxisY>
                    <AxisX LineColor="64, 64, 64, 64" IsLabelAutoFit="False">
                        <LabelStyle Font="Trebuchet MS, 10pt" />
                        <MajorGrid LineColor="64, 64, 64, 64" />
                    </AxisX>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
        <asp:ObjectDataSource ID="odsDates" runat="server" SelectMethod="GroupTicketsOnDatesByProviderId"
            TypeName="Dashboard.BLL.Tickets">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlYears" DefaultValue="0" Name="year" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="ddlProviders" DefaultValue="0" Name="providerId"
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
