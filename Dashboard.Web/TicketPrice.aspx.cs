﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.BLL;
using Dashboard.Common;

namespace Dashboard.Web
{
    public partial class TicketPrice : BasePage
    {
        private List<Group> providers;
        private List<Group> groups;

        protected int ProviderId
        {
            get
            {
                if (ViewState["ProviderId"] != null)
                {
                    return (int)ViewState["ProviderId"];
                }
                else
                {
                    return 0;
                }
            }
            set { ViewState["ProviderId"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            int year = dt.Year;
            if (!IsPostBack)
            {
                ddlYears.DataBind();
                ddlYears.SelectedValue = year.ToString();
                DataBindControls(year);
            }
        }

        protected void DataBindControls(int year)
        {
            providers = BLL.Stages.GroupProviderOnMonths(0, 3, year);
            groups = providers.Distinct<Group>(new GroupComparer(x => x.Id)).ToList();
            ddlProviders.DataSource = groups;
            ddlProviders.DataBind();
            ListItem li = ddlProviders.Items.FindByValue(ProviderId.ToString());
            if (ddlProviders.Items.FindByValue(ProviderId.ToString()) != null)
            {
                ddlProviders.SelectedValue = ProviderId.ToString();
            }
            else
            {
                ProviderId = 0;
            }
            ListItem listItem = new ListItem();
            listItem.Text = "Все цирки";
            listItem.Value = "0";
            ddlProviders.Items.Insert(0, listItem);
            chtProviders.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "C0";
            chtProviders.ChartAreas["ChartArea1"].AxisX.LabelStyle.IsEndLabelVisible = true;
            chtProviders.Series["Средняя цена"].IsValueShownAsLabel = chbValues.Checked;
            chtProviders.Series["Средняя цена"].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), ddlChartType.SelectedItem.Text, true);
        }

        protected void ddlProviders_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProviderId = Convert.ToInt32(ddlProviders.SelectedValue);
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindControls(year);
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindControls(year);
        }

        protected void chbValues_CheckedChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindControls(year);
        }

        protected void ddlChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProviderId = Convert.ToInt32(ddlProviders.SelectedValue);
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindControls(year);
        }
    }
}