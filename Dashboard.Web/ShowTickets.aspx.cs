﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;

namespace Dashboard.Web
{
    public partial class ShowTickets : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime dt = DateTime.Now;
                int year = dt.Year;
                ddlYears.DataBind();
                ddlYears.SelectedValue = year.ToString();
            }
        }


        private int totalActions = 0;
        private int totalEvents = 0;
        private int totalTickets = 0;
        private decimal price = 0;
        private decimal averigePrice = 0;

        protected void gdvTickets_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Group group = e.Row.DataItem as Group;
                if (group != null)
                {
                    totalActions += group.Count;
                    totalEvents += group.TotalCount;
                    totalTickets += group.CountResult;
                    price += group.Price;
                    if (averigePrice > 0)
                    {
                        averigePrice = ((averigePrice + group.AveragePrice) / 2);
                    }
                    else
                    {
                        averigePrice = group.AveragePrice;
                    }
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Итого:";
                e.Row.Cells[1].Text = totalActions.ToString("### ### ###");
                e.Row.Cells[2].Text = totalEvents.ToString("### ### ###");
                e.Row.Cells[3].Text = totalTickets.ToString("### ### ###");
                e.Row.Cells[4].Text = price.ToString("C0");
                e.Row.Cells[5].Text = averigePrice.ToString("C0");
            }
        }
    }
}