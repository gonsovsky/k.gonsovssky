﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master" CodeBehind="Default.aspx.cs" Inherits="Dashboard.Web.DefaultPage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">
     <div class="container">
    <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
            <h2>Оборот по циркам</h2>
            <p>Более подробная статистика представлена по адресу <a href="http://stat.zirk.ru" target="_blank">http://stat.zirk.ru/</a></p>
        </div>
    <div class="row">
            <div class="col-md-3">
                <p>Выберите год:</p>
                <asp:DropDownList ID="ddlYears" Width="200px" runat="server" AutoPostBack="True"
                    DataSourceID="odsYears" class="btn btn-default" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="odsYears" runat="server" SelectMethod="GetAvailableYears"
                    TypeName="Dashboard.BLL.ShowActions"></asp:ObjectDataSource>
            </div>
            <div class="col-md-4">
                     <p>Выберите цирк:</p>
                <asp:DropDownList ID="ddlProviders" Width="240px" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="Id"
                    class="btn btn-default" OnSelectedIndexChanged="ddlProviders_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-2">
                <p>Значения:</p>
                <asp:CheckBox ID="chbValues" AutoPostBack="True" runat="server" OnCheckedChanged="chbValues_CheckedChanged" />
           </div>
           <div class="col-md-3">
                     <p>Тип графика:</p>
                <asp:DropDownList ID="ddlChartType" Width="200px" runat="server" AutoPostBack="True" 
                    class="btn btn-default" OnSelectedIndexChanged="ddlChartType_SelectedIndexChanged">
                    <asp:ListItem>StepLine</asp:ListItem>
                    <asp:ListItem Selected="True">Spline</asp:ListItem>
                    <asp:ListItem>Line</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        
    <asp:Chart ID="chtProviders" runat="server" ImageLocation="Temp/ChartPic_#SEQ(290,3)"
            Height="500px" Width="1024px" BorderWidth="0px" BackColor="#FFFFFF" BackGradientStyle="None"
            BorderColor="Transparent" BorderlineDashStyle="NotSet">
             <Legends>
                 <asp:Legend TitleFont="Microsoft Sans Serif, 10pt" BackColor="Transparent" Docking="Bottom"
                     IsEquallySpacedItems="True" Font="Trebuchet MS, 10pt" IsTextAutoFit="True"
                     Name="Default">
                 </asp:Legend>
             </Legends>
             <BorderSkin SkinStyle="None"></BorderSkin>
              <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" BackColor="#FFFFFF">
                        <AxisY LineColor="64, 64, 64, 64">
                            <LabelStyle Font="Trebuchet MS, 10pt" />
                            <MajorGrid LineColor="64, 64, 64, 64" />
                        </AxisY>
                        <AxisX IsMarginVisible="False" LineColor="64, 64, 64, 64">
                            <LabelStyle Font="Trebuchet MS, 10pt" />
                            <MajorGrid LineColor="64, 64, 64, 64" />
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
         </asp:Chart>

         
                        <br/>
       
    </div>
</asp:Content>
