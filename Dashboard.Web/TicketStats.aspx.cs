﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;
using Dashboard.Common.Base;

namespace Dashboard.Web
{
    public partial class TicketStats : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            chrVisits.Series["Series1"]["DrawingStyle"] = "Cylinder";

            if (!IsPostBack)
            {
                ddlYears.DataBind();
                ddlMonths.DataBind();
                DateTime dt = DateTime.Now;
                ddlYears.SelectedValue = dt.Year.ToString();
                ddlMonths.SelectedValue = dt.Month.ToString();
                DataBindCharts(dt.Year);
            }
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindCharts(year);
        }

        protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindCharts(year);
        }

        protected void DataBindCharts(int year)
        {
            hplGroupTicketsOnMonths.NavigateUrl = string.Format("ReportHandler.ashx?type=Tickets.GroupTicketsOnMonths&providerId=0&actionId=0&statusId=3&year={0}", year);
            //List<Month> list1 = BLL.Tickets.GroupTicketsOnMonths(2015);
            //List<Month> list2 = BLL.Tickets.GroupTicketsOnMonths(2014);

            //foreach (Month m in list1)
            //{
            //    Chart1.Series["Series1"].Points.AddY(m.Count);
            //}

            //foreach (Month m in list2)
            //{
            //    Chart1.Series["Series2"].Points.AddY(m.Count);
            //}
        }
    }
}