﻿using System;
using System.Web.Security;

namespace Dashboard.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string username = "k.gonsovsky@zirk.ru";
            //string password = "abc123";
            //MembershipUser mu = Membership.GetUser(username);
            //mu.ChangePassword(mu.ResetPassword(), password);

            if (Page.User.Identity.IsAuthenticated)
            {
                Response.Redirect("Default.aspx");
            }
        }
    }
}