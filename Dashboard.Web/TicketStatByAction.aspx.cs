﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;
using Dashboard.Common.Base;

namespace Dashboard.Web
{
    public partial class TicketStatByAction : System.Web.UI.Page
    {
        protected int ActionId 
        {
            get
            {
                if (ViewState["ActionId"] != null)
                {
                    return (int)ViewState["ActionId"];
                }
                else
                {
                    return 0;
                }
                
            }
            set { ViewState["ActionId"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            chrCountTickets.Series["Series1"]["DrawingStyle"] = "Cylinder";

            if (!IsPostBack)
            {
                if (Context.Request.QueryString["actionId"] != null)
                {
                    string actId = Context.Request.QueryString["actionId"];
                    int actionId;
                    if (Int32.TryParse(actId, out actionId))
                    {
                        ActionId = actionId;
                        ShowAction showAction = BLL.ShowActions.GetShowActionByActionId(actionId);
                        string title = showAction.Name.Replace("\"", "");
                        ltrTitle.Text = string.Format("Отчет по билетам \"{0}\"", title);
                        ltrCount.Text = string.Format("Всего <b>{0}</b> билетов", BLL.Tickets.CountTickets(0, actionId).ToString("#### ###"));
                    }
                }

                ddlYears.DataBind();
                DateTime dt = DateTime.Now;
                ddlYears.SelectedValue = dt.Year.ToString();
                DataBindCharts(dt.Year);
            }
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindCharts(year);
        }

        protected void DataBindCharts(int year)
        {
            hplGroupTicketsOnMonthsByActionId.NavigateUrl = string.Format("ReportHandler.ashx?type=Tickets.GroupTicketsOnMonthsByActionId&actionId={0}&year={1}", ActionId, year);
        }
    }
}