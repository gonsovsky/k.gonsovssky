﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;

namespace Dashboard.Web
{
    public partial class TicketFilter : System.Web.UI.Page
    {
        private int cellIndex = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                ddlProviders.DataSource = BLL.Stages.GetPagedProviders(0, 100, 0, 0);
                ddlProviders.DataBind();
                ListItem listItem = new ListItem();
                listItem.Text = "Все цирки";
                listItem.Value = "0";
                ddlProviders.Items.Insert(0, listItem);
                ddlActions.Enabled = false;
                ddlEventDates.Enabled = false;
            }
        }

        protected void ddlProviders_SelectedIndexChanged(object sender, EventArgs e)
        {
            int providerId = Convert.ToInt32(ddlProviders.SelectedValue);
            List<ShowAction> actions = BLL.ShowActions.GetShowActionsByProviderId(providerId, 2016);
            if (providerId > 0)
            {
                if (actions.Count > 0)
                {
                    ddlActions.Enabled = true;
                    ddlActions.DataSource = actions;
                    ddlActions.DataBind();
                    ListItem actionItem = new ListItem();
                    actionItem.Text = "Все программы";
                    actionItem.Value = "0";
                    ddlActions.Items.Insert(0, actionItem);

                    int actionId = Convert.ToInt32(ddlActions.SelectedValue);
                    List<EventDate> eventDates = BLL.EventDates.GetEventDatesByActionId(actionId);
                    if (eventDates.Count > 0)
                    {
                        ddlEventDates.Enabled = true;
                        ddlEventDates.DataSource = eventDates;
                        ddlEventDates.DataBind();
                        ListItem eventItem = new ListItem();
                        eventItem.Text = "Все выступления";
                        eventItem.Value = "0";
                        ddlEventDates.Items.Insert(0, eventItem);
                    }
                    else
                    {
                        ddlEventDates.Items.Clear();
                        ddlEventDates.Enabled = false;
                    }
                }
                else
                {
                    ddlActions.Items.Clear();
                    ddlActions.Enabled = false;

                    ddlEventDates.Items.Clear();
                    ddlEventDates.Enabled = false;
                }
            }
            else
            {
                ddlActions.Items.Clear();
                ddlActions.Enabled = false;

                ddlEventDates.Items.Clear();
                ddlEventDates.Enabled = false;
            }
            
        }

        protected void ddlActions_SelectedIndexChanged(object sender, EventArgs e)
        {
            int actionId = Convert.ToInt32(ddlActions.SelectedValue);
            List<EventDate> eventDates = BLL.EventDates.GetEventDatesByActionId(actionId);

            if (eventDates.Count > 0)
            {
                ddlEventDates.Enabled = true;
                ddlEventDates.DataSource = eventDates;
                ddlEventDates.DataBind();
                ListItem eventItem = new ListItem();
                eventItem.Text = "Все выступления";
                eventItem.Value = "0";
                ddlEventDates.Items.Insert(0, eventItem);
            }
            else
            {
                ddlEventDates.Items.Clear();
                ddlEventDates.Enabled = false;
            }
        }

        protected void ddlEventDates_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(ddlEventDates.SelectedValue);
            //List<EventDate> eventDates = BLL.EventDates.GetEventDatesByActionId(eventId);
            //ddlEventDates.DataSource = eventDates;
            //ddlEventDates.DataBind();
        }

        private int totalActions = 0;
        private int totalEvents = 0;
        private int totalTickets = 0;
        private decimal price = 0;
        private decimal averigePrice = 0;

        protected void gdvTickets_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRow row = ((DataRowView)e.Row.DataItem).Row;
                if (row != null)
                {
                    totalActions += (int)row.ItemArray[2];
                    totalEvents += (int)row.ItemArray[3];
                    totalTickets += (int)row.ItemArray[4];
                    price += (decimal)row.ItemArray[5];
                    if (averigePrice > 0)
                    {
                        averigePrice = ((averigePrice + (decimal)row.ItemArray[6]) / 2);
                    }
                    else
                    {
                        averigePrice = (decimal)row.ItemArray[6];
                    }
                }

                if (cellIndex > -1)
                {
                    e.Row.Cells[cellIndex].Font.Bold = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Итого:";
                e.Row.Cells[1].Text = totalActions.ToString("### ### ###");
                e.Row.Cells[2].Text = totalEvents.ToString("### ### ###");
                e.Row.Cells[3].Text = totalTickets.ToString("### ### ###");
                e.Row.Cells[4].Text = price.ToString("C0");
                e.Row.Cells[5].Text = averigePrice.ToString("C0");
            }
        }

        protected void gdvTickets_OnSorting(object sender, GridViewSortEventArgs e)
        {
            foreach (var column in gdvTickets.Columns)
            {
                DataControlField dcf = (DataControlField)column;
                if (dcf.SortExpression == e.SortExpression)
                {
                    cellIndex = gdvTickets.Columns.IndexOf(dcf);
                }
            }
        }
        
        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int year = Convert.ToInt32(ddlYears.SelectedValue);
            //DataBindCharts(year);
        }
    }
}