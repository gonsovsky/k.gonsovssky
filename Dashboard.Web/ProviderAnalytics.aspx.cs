﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;
using Dashboard.Common.Base;

namespace Dashboard.Web
{
    public partial class ProviderAnalytics : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            if (!IsPostBack)
            {
                ddlYears.DataBind();

                ddlYears.SelectedValue = dt.Year.ToString();
            }

            DataBindCharts(dt.Year);
        }

        protected void gdvProviders_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //MedItem medItem = e.Row.DataItem as MedItem;
            }
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindCharts(year);
        }

        protected void DataBindCharts(int year)
        {
            string chartTypeName = ChartTypeList.SelectedItem.Text;

            List<Provider> pr = BLL.Stages.GetProviderStats(0, year);
            List<Provider> providers = pr.OrderBy(p => p.Count).ToList();
            //int counter = 1;

            //Chart1.Series.Add(new Series("Выпущено"));
            Chart1.Series["Выпущено"].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), chartTypeName, true);
            //Chart1.Series.Add(new Series("Продано"));
            Chart1.Series["Продано"].ChartType =
                (SeriesChartType) Enum.Parse(typeof (SeriesChartType), chartTypeName, true);
            //Chart1.Series.Add(new Series("Непродано"));
            Chart1.Series["Непродано"].ChartType =
                (SeriesChartType) Enum.Parse(typeof (SeriesChartType), chartTypeName, true);

            foreach (Provider p in providers)
            {
                Chart1.Series["Выпущено"].Points.AddY(p.TotalCount);
                Chart1.Series["Продано"].Points.AddY(p.Count);
                Chart1.Series["Непродано"].Points.AddY(p.CountResult);
            }

            for (int i = 0; i < providers.Count(); i++)
            {
                CustomLabel cl = new CustomLabel(i, i + 2, providers[i].Name, 0, LabelMarkStyle.None,
                    GridTickTypes.None);
                Chart1.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(cl);
            }
        }
    }
}