﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;
using Dashboard.Common.Base;

namespace Dashboard.Web
{
    public partial class ProvidersStat : System.Web.UI.Page
    {
        private int cellIndex = -1;
        private int year;
        private Group group;

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            year = dt.Year;

            if (!IsPostBack)
            {
                ddlYears.DataBind();
                ddlYears.SelectedValue = year.ToString();
            }
            DataBindCharts(year);
        }

        protected void DataBindCharts(int year)
        {
            group = BLL.Tickets.SumSoldTickets(0, 0, 3, year);
            ltrTitle.Text = string.Format("За <b>{0}</b> год продано <b>{1}</b> билетов на сумму <b>{2}</b>", year,
                group.Count.ToString("### ### ###"), group.Price.ToString("C"));

            StripLine stripLow = new StripLine();
            stripLow.IntervalOffset = 150000000;
            stripLow.StripWidth = 50000000;
            stripLow.BackColor = ColorTranslator.FromHtml("#DFF0D8");

            StripLine stripMed = new StripLine();
            stripMed.IntervalOffset = 100000000;
            stripMed.StripWidth = 50000000;
            stripMed.BackColor = ColorTranslator.FromHtml("#FCF8E3");

            StripLine stripHigh = new StripLine();
            stripHigh.IntervalOffset = 0;
            stripHigh.StripWidth = 50000000;
            stripHigh.BackColor = ColorTranslator.FromHtml("#F2DEDE");

            Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripLow);
            Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripMed);
            Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripHigh);
        }
        
        protected void gdvProviders_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRow row = ((DataRowView)e.Row.DataItem).Row;
                string v = e.Row.Cells[6].Text;
                e.Row.Cells[6].Text = v + "%";

                if (cellIndex > -1)
                {
                    e.Row.Cells[cellIndex].Font.Bold = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Итого:";
                e.Row.Cells[3].Text = group.Count.ToString("### ### ###");
                e.Row.Cells[4].Text = group.Price.ToString("C");
            }
        }
        
        protected void gdvProviders_OnSorting(object sender, GridViewSortEventArgs e)
        {
            foreach (var column in gdvProviders.Columns)
            {
                DataControlField dcf = (DataControlField) column;
                if (dcf.SortExpression == e.SortExpression)
                {
                    cellIndex = gdvProviders.Columns.IndexOf(dcf);
                }
            }
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindCharts(year);
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}