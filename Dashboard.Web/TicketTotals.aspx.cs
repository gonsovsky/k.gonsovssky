﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;
using Dashboard.Common.Base;

namespace Dashboard.Web
{
    public partial class TicketTotals : System.Web.UI.Page
    {
        private int cellIndex = -1;
        private int year;
        private Group group;

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            year = dt.Year;

            if (!IsPostBack)
            {
                ddlYears.DataBind();
                ddlYears.SelectedValue = year.ToString();
                ddlProviders.DataSource = BLL.Stages.GetPagedProviders(0, 100, 0, 0);
                ddlProviders.DataBind();
                ListItem listItem = new ListItem();
                listItem.Text = "Все цирки";
                listItem.Value = "0";
                ddlProviders.Items.Insert(0, listItem);
            }
            //DataBindCharts(year);
        }

        protected void ddlProviders_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void DataBindCharts(int year)
        {
            group = BLL.Tickets.SumSoldTickets(0, 0, 3, year);
            ltrTitle.Text = string.Format("За <b>{0}</b> год продано <b>{1}</b> билетов на сумму <b>{2}</b>", year,
                group.Count.ToString("### ### ###"), group.Price.ToString("C"));

            StripLine stripLow = new StripLine();
            stripLow.IntervalOffset = 80000000;
            stripLow.StripWidth = 40000000;
            stripLow.BackColor = ColorTranslator.FromHtml("#DFF0D8");

            StripLine stripMed = new StripLine();
            stripMed.IntervalOffset = 40000000;
            stripMed.StripWidth = 40000000;
            stripMed.BackColor = ColorTranslator.FromHtml("#FCF8E3");

            StripLine stripHigh = new StripLine();
            stripHigh.IntervalOffset = 0;
            stripHigh.StripWidth = 40000000;
            stripHigh.BackColor = ColorTranslator.FromHtml("#F2DEDE");

            //Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripLow);
            //Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripMed);
            //Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripHigh);
        }

        
        protected void gdvTickets_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //DataRow row = ((DataRowView)e.Row.DataItem).Row;
                Group group = e.Row.DataItem as Group;
                //if (group.StartDate.Month != prevMonth)
                //{
                //    e.Row.Style.Remove("border-bottom");
                //    e.Row.Style.Add("border-bottom", "red");
                //    prevMonth = group.StartDate.Month;
                //}
                //string v = e.Row.Cells[3].Text;
                //e.Row.Cells[3].Text = v + "%";

                //if (cellIndex > -1)
                //{
                //    e.Row.Cells[cellIndex].Font.Bold = true;
                //}
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                //e.Row.Cells[0].Text = "Итого:";
                //e.Row.Cells[1].Text = group.Count.ToString("### ### ###");
                //e.Row.Cells[4].Text = group.Price.ToString("C");
            }
        }
        
        protected void gdvTickets_OnSorting(object sender, GridViewSortEventArgs e)
        {
            foreach (var column in gdvTickets.Columns)
            {
                DataControlField dcf = (DataControlField) column;
                if (dcf.SortExpression == e.SortExpression)
                {
                    cellIndex = gdvTickets.Columns.IndexOf(dcf);
                }
            }
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            //DataBindCharts(year);
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}