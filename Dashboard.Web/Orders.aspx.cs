﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dashboard.Common.Base;

namespace Dashboard.Web
{
    public partial class Orders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ltrCount.Text = string.Format("Всего <b>{0}</b> заказов", BLL.Orders.CountOrders().ToString("### ### ###"));
            }
        }

        protected void gdvOrders_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //MedItem medItem = e.Row.DataItem as MedItem;
            }
        }

        protected void ddlCities_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int year = Convert.ToInt32(ddlYears.SelectedValue);
            //DataBindCharts(year);
        }
    }
}