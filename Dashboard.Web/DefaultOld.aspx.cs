﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;

namespace Dashboard.Web
{
    public partial class DefaultOld : System.Web.UI.Page
    {
        //private int year;
        private Group group;

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            int year = dt.Year;

            if (!IsPostBack)
            {
                List<Group> groups = BLL.Stages.GroupProvidersOnShowActions(0, 3, year);
                ltrProviders.Text = string.Format("{0}", groups.Count);
                
                string programs = groups.Sum(x => x.Count).ToString("### ### ###");
                ltrShows.Text = programs;
                ltrShowActions.Text = programs;
                string events = groups.Sum(x => x.TotalCount).ToString("### ### ###");
                ltrEventDates.Text = events;
                ltrEvents.Text = events;
                string tickets = groups.Sum(x => x.CountResult).ToString("### ### ###");
                
                ltrTicketsCount.Text = tickets;
                string price = groups.Average(x => x.AveragePrice).ToString("C0");
                ltrAvgTicketPrice.Text = price;

                List<Provider> providers = BLL.Stages.GetProviderStats(3, year);
                ltrFillPercent.Text = providers.Average(x => x.Percent).ToString("##") + "%";
                
                group = BLL.Tickets.SumSoldTickets(0, 0, 3, year);
                ltrShows.Text = group.Price.ToString("C0");
                //ltrYear.Text = string.Format("{0} год", year);
                ltrRunningTotal.Text = FormatRunningTotal(year);


                chtTopShowActions.DataBind();
                DataPoint topPoint = new DataPoint();

                foreach (DataPoint point in chtTopShowActions.Series["Series1"].Points)
                {
                    if (topPoint.YValues[0] < point.YValues[0])
                    {
                        topPoint = point;
                    }
                }

                foreach (DataPoint point in chtTopShowActions.Series["Series1"].Points)
                {
                    point["Exploded"] = "false";
                    if (point.AxisLabel == topPoint.AxisLabel)
                    {
                        point["Exploded"] = "true";
                    }
                }
            }
        }

        protected string FormatRunningTotal(int year)
        {
            string output = "<ul class=\"list-group\">";
            List<Group> df = BLL.Tickets.RunningTotals(0, 3, year);
            List<Group> points = df.Where(x => x.EndDate < DateTime.Now).ToList();


            int threshhold = 0;
            int pointsCount = points.Count;
            if (pointsCount > 4)
            {
                threshhold = pointsCount - 4;
            }
            else
            {
                threshhold = pointsCount;
            }

            try
            {
            Group temp = null;
            for (int i = 0; i < pointsCount; i++)
            {
                if (i >= threshhold)
                {
                    if (temp != null)
                    {
                        output += "<li class=\"list-group-item\">";
                        Group g = points[i];
                        if (temp.Price < g.Price)
                        {
                            output += string.Format("<h6>{0}</h6>{1} - {2}<span class=\"glyphicon glyphicon-arrow-up\" style=\"color:yellowgreen;\"></span>", g.Price.ToString("C0"), g.StartDate.ToString("M"), g.EndDate.ToString("M"));
                        }
                        else
                        {
                            output += string.Format("<h6>{0}</h6>{1} - {2}<span class=\"glyphicon glyphicon-arrow-down\" style=\"color:#D35400;\"></span>", g.Price.ToString("C0"), g.StartDate.ToString("M"), g.EndDate.ToString("M"));
                        }
                        output += "</li>";
                        temp = points[i];
                    }
                    else
                    {
                        temp = points[i];
                    }
                }
            }
            }
            catch { }

            return output += "</ul>";
        }
    }
}