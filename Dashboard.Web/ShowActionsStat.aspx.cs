﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;
using Dashboard.Common.Base;

namespace Dashboard.Web
{
    public partial class ShowActionsStat : System.Web.UI.Page
    {
        private int cellIndex = -1;
        private int year;
        private Group group;
        //private int providerId;
        private int count;

        private int ProviderId
        {
            get
            {
                return (int) ViewState["ProviderId"];
            }
            set { ViewState["ProviderId"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            year = dt.Year;

            if (!IsPostBack)
            {
                if (Request.QueryString["providerId"] != null)
                {
                    string prid = Request.QueryString["providerId"];
                    int providerId = 0;
                    if (Int32.TryParse(prid, out providerId))
                    {
                        ProviderId = providerId;
                        Provider provider = BLL.Stages.GetProviderByProviderId(ProviderId);
                        ltrTitle.Text = string.Format("{0}", provider.Name);
                    }
                    
                }

                ddlYears.DataBind();
                ddlYears.SelectedValue = year.ToString();
            }

            DataBindCharts(year);
        }

        protected void DataBindCharts(int year)
        {
            group = BLL.Tickets.SumSoldTickets(ProviderId, 0, 3, year);
            count = BLL.EventDates.CountEventDates(0, ProviderId, 0, year);

            ltrSubTitle.Text = string.Format("За <b>{0}</b> год проведено <b>{1}</b> выступлений, продано <b>{2}</b> билетов на сумму <b>{3}</b>", year, count, group.Count.ToString("### ### ###"), group.Price.ToString("C"));

            StripLine stripLow = new StripLine();
            stripLow.IntervalOffset = 8000000;
            stripLow.StripWidth = 8000000;
            stripLow.BackColor = ColorTranslator.FromHtml("#DFF0D8");

            StripLine stripMed = new StripLine();
            stripMed.IntervalOffset = 4000000;
            stripMed.StripWidth = 4000000;
            stripMed.BackColor = ColorTranslator.FromHtml("#FCF8E3");

            StripLine stripHigh = new StripLine();
            stripHigh.IntervalOffset = 0;
            stripHigh.StripWidth = 4000000;
            stripHigh.BackColor = ColorTranslator.FromHtml("#F2DEDE");

            Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripLow);
            Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripMed);
            Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripHigh);
        }
        

        

        protected void gdvProviders_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string v = e.Row.Cells[6].Text;
                e.Row.Cells[6].Text = v + "%";

                if (cellIndex > -1)
                {
                    e.Row.Cells[cellIndex].Font.Bold = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Итого:";
                e.Row.Cells[3].Text = count.ToString("### ### ###");
                e.Row.Cells[4].Text = group.Count.ToString("### ### ###");
                e.Row.Cells[6].Text = group.Price.ToString("C");
            }
        }


        protected void gdvProviders_OnSorting(object sender, GridViewSortEventArgs e)
        {
            foreach (var column in gdvProviders.Columns)
            {
                DataControlField dcf = (DataControlField) column;
                if (dcf.SortExpression == e.SortExpression)
                {
                    cellIndex = gdvProviders.Columns.IndexOf(dcf);
                }
            }
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(ddlYears.SelectedValue);
            DataBindCharts(year);
        }
    }
}