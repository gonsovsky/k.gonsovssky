﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Dashboard.Common;
using Dashboard.Common.Base;

namespace Dashboard.Web
{
    public partial class MemberStat : System.Web.UI.Page
    {
       
        private int year;
       

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            year = dt.Year;

            if (!IsPostBack)
            {
                if (Request.QueryString["eventId"] != null)
                {
                    string evntId = Request.QueryString["eventId"];
                    int eventId;
                    if (Int32.TryParse(evntId, out eventId))
                    {
                        EventDate eventDate = BLL.EventDates.GetEventDateById(eventId);
                        ShowAction showAction = BLL.ShowActions.GetShowActionByActionId(eventDate.ActionId);
                        int providerId = showAction.ProviderId;
                        Provider provider = BLL.Stages.GetProviderByProviderId(showAction.ProviderId);
                        ltrTitle.Text = string.Format("{0}", provider.Name);
                        ltrSubTitle.Text = string.Format("{0}", showAction.Name);
                        ltrStat.Text = eventDate.ActionDate.ToString("D");

                        hplShowActions.Text = provider.Name;
                        hplShowActions.NavigateUrl = string.Format("ShowActionsStat.aspx?providerId={0}", providerId);

                        hplEventDates.Text = showAction.Name;
                        hplEventDates.NavigateUrl = string.Format("EventDatesStat.aspx?actionId={0}", showAction.Id);
                    }
                        
                }
            }
            DataBindCharts(year);
        }

        protected void DataBindCharts(int year)
        {
            //group = BLL.Tickets.SumSoldTickets(0, 0, 3, year);
            //ltrTitle.Text = string.Format("За <b>{0}</b> год продано <b>{1}</b> билетов на сумму <b>{2}</b>", year,
            //    group.Count.ToString("### ### ###"), group.Price.ToString("C"));
            //chtMembers
            chtMembers.Series[0]["PieDrawingStyle"] = "SoftEdge";
            chtMembers.Series[0]["PieLabelStyle"] = "Outside";

            //StripLine stripLow = new StripLine();
            //stripLow.IntervalOffset = 80000000;
            //stripLow.StripWidth = 40000000;
            //stripLow.BackColor = ColorTranslator.FromHtml("#DFF0D8");

            //StripLine stripMed = new StripLine();
            //stripMed.IntervalOffset = 40000000;
            //stripMed.StripWidth = 40000000;
            //stripMed.BackColor = ColorTranslator.FromHtml("#FCF8E3");

            //StripLine stripHigh = new StripLine();
            //stripHigh.IntervalOffset = 0;
            //stripHigh.StripWidth = 40000000;
            //stripHigh.BackColor = ColorTranslator.FromHtml("#F2DEDE");

            //Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripLow);
            //Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripMed);
            //Chart1.ChartAreas["ChartArea1"].AxisY.StripLines.Add(stripHigh);
        }
        
        protected void gdvProviders_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //DataRow row = ((DataRowView)e.Row.DataItem).Row;
                //string v = e.Row.Cells[3].Text;
                //e.Row.Cells[3].Text = v + "%";

                //if (cellIndex > -1)
                //{
                //    e.Row.Cells[cellIndex].Font.Bold = true;
                //}
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                //e.Row.Cells[0].Text = "Итого:";
                //e.Row.Cells[1].Text = group.Count.ToString("### ### ###");
                //e.Row.Cells[4].Text = group.Price.ToString("C");
            }
        }
        
        protected void gdvProviders_OnSorting(object sender, GridViewSortEventArgs e)
        {
            //foreach (var column in gdvProviders.Columns)
            //{
            //    DataControlField dcf = (DataControlField) column;
            //    if (dcf.SortExpression == e.SortExpression)
            //    {
            //        cellIndex = gdvProviders.Columns.IndexOf(dcf);
            //    }
            //}
        }

        

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}