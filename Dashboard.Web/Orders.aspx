﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master" CodeBehind="Orders.aspx.cs" Inherits="Dashboard.Web.Orders" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">
     <div class="container">
    <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
            <h2>Заказы</h2>
            <p><asp:Literal ID="ltrCount" runat="server"></asp:Literal></p>            
        </div>
    
            <asp:GridView ID="gdvOrders" CssClass="grid-view" OnRowDataBound="gdvOrders_OnRowDataBound" runat="server" DataSourceID="odsOrders" 
        DataKeyNames="Id" EnableViewState="False" AutoGenerateColumns="False" PageSize="100" PagerSettings-Position="TopAndBottom"
        PagerSettings-Mode="Numeric" PagerSettings-PageButtonCount="10" AllowPaging="True">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>#</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrId" Text='<%# Eval("Id") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Кол-во</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrCount" Text='<%# Eval("Count")  + " шт" %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>Создано</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrCreatedDate" Text='<%# Eval("CreatedDate", "{0:F}") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>Состав</HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplTickets" NavigateUrl='<%# "OrderItems.aspx?orderId=" +  Eval("Id") %>' runat="server">Состав</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                <h4><asp:Literal ID="ltrEmptyData" runat="server" Text="Нет"></asp:Literal></h4>
            </EmptyDataTemplate>
            <HeaderStyle CssClass="th-row-style" />
            <RowStyle CssClass="row-style" />
            <AlternatingRowStyle CssClass="alt-row-style" />
            <PagerStyle CssClass="pager-style"/>
        </asp:GridView>

        <asp:ObjectDataSource ID="odsOrders" EnablePaging="True" runat="server" SelectMethod="GetPagedOrders"
            SelectCountMethod="CountOrders" TypeName="Dashboard.BLL.Orders">
        </asp:ObjectDataSource>
    </div>
</asp:Content>
