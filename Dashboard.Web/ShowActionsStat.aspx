﻿<%@ Page Language="C#" Title="Статистика программ" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master" CodeBehind="ShowActionsStat.aspx.cs" Inherits="Dashboard.Web.ShowActionsStat" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">
     <div class="container">
    <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
            <h2><asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h2>
            <p><asp:Literal ID="ltrSubTitle" runat="server"></asp:Literal></p>          
        </div>
     <div class="row">
            <div class="col-md-3">
                <p>Выберите год:</p>
                <asp:DropDownList ID="ddlYears" Width="200px" runat="server" AutoPostBack="True"
                    DataSourceID="odsYears" class="btn btn-default" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="odsYears" runat="server" SelectMethod="GetAvailableYears"
                    TypeName="Dashboard.BLL.ShowActions"></asp:ObjectDataSource>
            </div>
            <div class="col-md-8">
                
            <asp:Chart ID="Chart1" DataSourceID="odsVisits" runat="server" ImageLocation="TempImages/ChartPic_#SEQ(300,3)"
            Height="200px" Width="850px" BorderWidth="2px" BackColor="WhiteSmoke" BackSecondaryColor="White"
            BackGradientStyle="TopBottom" BorderColor="#1A3B69" BorderlineDashStyle="Solid">
            <Titles>
                <asp:Title ShadowColor="32, 0, 0, 0" Text="Продажи по месяцам, млн. рублей" Font="Trebuchet MS, 14.25pt, style=Bold"
                    ShadowOffset="3" ForeColor="26, 59, 105">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Enabled="False" IsTextAutoFit="False" Name="Default" BackColor="Transparent"
                    Font="Trebuchet MS, 8.25pt, style=Bold" Title="Legend Name">
                </asp:Legend>
            </Legends>
            <BorderSkin SkinStyle="None"></BorderSkin>
            <Series>
                <asp:Series Name="Series1" ChartType="FastLine" MarkerStyle="Circle" MarkerSize="7" MarkerColor="224, 64, 10" BorderColor="180, 26, 59, 105" Color="#337AB7" BorderWidth="2" XValueMember="Name" YValueMembers="Price">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="White" BackColor="WhiteSmoke" ShadowColor="Transparent">
                    <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="False" Inclination="15" IsRightAngleAxes="True" WallWidth="0" IsClustered="False" />
                    <AxisY Enabled="True" LineColor="64, 64, 64, 64">
                        <LabelStyle Format="{0:C0}" Font="Trebuchet MS, 8.25pt, style=Bold" />
                        <MajorGrid LineColor="64, 64, 64, 64" />
                    </AxisY>
                    <AxisX IsMarginVisible="False" LineColor="64, 64, 64, 64">
                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                        <MajorGrid LineColor="64, 64, 64, 64" />
                    </AxisX>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
         <asp:ObjectDataSource ID="odsVisits" runat="server" SelectMethod="GroupTicketsOnMonths"
            TypeName="Dashboard.BLL.Tickets">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="0" Name="providerId" QueryStringField="providerId" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="actionId" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="eventId" Type="Int32" />
                <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
                <asp:ControlParameter ControlID="ddlYears" DefaultValue="0" Name="year" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
                 </div>
        </div>
        <br />
            <asp:GridView ID="gdvProviders" CssClass="grid-view" OnRowDataBound="gdvProviders_OnRowDataBound" OnSorting="gdvProviders_OnSorting" runat="server" DataSourceID="odsProviders" 
        DataKeyNames="Id" EnableViewState="False" ShowFooter="True" AutoGenerateColumns="False" PageSize="100" PagerSettings-Position="TopAndBottom" AllowSorting="True" 
        PagerSettings-Mode="Numeric" PagerSettings-PageButtonCount="10" AllowPaging="True">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Программа" SortExpression="Name" />
                <asp:BoundField DataField="StartDate" HeaderText="Начало" DataFormatString="{0:D}" SortExpression="StartDate" />
                <asp:BoundField DataField="EndDate" HeaderText="Конец" DataFormatString="{0:D}" SortExpression="EndDate" />
                <asp:BoundField DataField="TotalCount" HeaderText="Выступлений" SortExpression="TotalCount" />
                <asp:BoundField DataField="Count" HeaderText="Продано, билетов" SortExpression="Count" />
                <asp:BoundField DataField="Price" DataFormatString="{0:C0}" HeaderText="Продано, рублей" SortExpression="Price" />
                <asp:BoundField DataField="Percent" HeaderText="Заполняемость зала" SortExpression="Percent" />
                <asp:TemplateField>
                    <HeaderTemplate>Подробно</HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplTickets" NavigateUrl='<%# "EventDatesStat.aspx?actionId=" +  Eval("Id") %>' runat="server">Подробно</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                <h4><asp:Literal ID="ltrEmptyData" runat="server" Text="Программ выступлений не зарегистрировано"></asp:Literal></h4>
            </EmptyDataTemplate>
            <HeaderStyle CssClass="th-row-style" />
            <RowStyle CssClass="row-style" />
            <AlternatingRowStyle CssClass="alt-row-style" />
            <PagerStyle CssClass="pager-style"/>
            <FooterStyle CssClass="footer-style" />
        </asp:GridView>

        <asp:ObjectDataSource ID="odsProviders" runat="server" SelectMethod="GetShowActionSortableStats" TypeName="Dashboard.BLL.ShowActions">
           <SelectParameters>
               <asp:QueryStringParameter DefaultValue="0" Name="providerId" QueryStringField="providerId" Type="Int32" />
               <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
               <asp:ControlParameter ControlID="ddlYears" PropertyName="SelectedValue" DefaultValue="0" Name="year" Type="Int32" />
           </SelectParameters>
        </asp:ObjectDataSource>
        
        <ul class="nav nav-pills" role="tablist">
                   <li role="presentation"><a href="ProvidersStat.aspx">Список цирков</a></li>
                   </ul>
        <br/>
    </div>
</asp:Content>
