﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master" CodeBehind="ShowTickets.aspx.cs" Inherits="Dashboard.Web.ShowTickets" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">
     <div class="container">
     <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
         <h2>Статистика по билетам</h2>
            <h2><asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h2>
            <p><asp:Literal ID="ltrCount" runat="server"></asp:Literal></p>            
        </div>
        <div class="row">
            <div class="col-md-3">
                <p>Выберите год:</p>
                <asp:DropDownList ID="ddlYears" Width="200px" runat="server" AutoPostBack="True"
                    DataSourceID="odsYears" class="btn btn-default">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="odsYears" runat="server" SelectMethod="GetAvailableYears"
                    TypeName="Dashboard.BLL.ShowActions"></asp:ObjectDataSource>
            </div>
            <div class="col-md-8">
            </div>
            </div>
            <br/>
            
<asp:GridView ID="gdvTickets" CssClass="grid-view" OnRowDataBound="gdvTickets_OnRowDataBound" runat="server" DataSourceID="odsEventDates" 
         EnableViewState="False" ShowFooter="True" AutoGenerateColumns="False" PageSize="100" PagerSettings-Position="TopAndBottom" AllowSorting="True" 
        PagerSettings-Mode="Numeric" PagerSettings-PageButtonCount="10" AllowPaging="True">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>Название</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrName" Text='<%# Eval("Name") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Программ</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrCount" Text='<%# Eval("Count") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Выступлений</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrTotalCount" Text='<%# Eval("TotalCount") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Продано, билетов</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrCountResult" Text='<%# Eval("CountResult", "{0:### ###}") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Продано, рублей</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrPrice" Text='<%# Eval("Price", "{0:C0}") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Средняя цена билета</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="ltrAveragePrice" Text='<%# Eval("AveragePrice", "{0:C0}") %>' runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                <div class="alert alert-danger" role="alert">
                    Данные отсутствуют. 
                </div>
            </EmptyDataTemplate>
            <HeaderStyle CssClass="th-row-style" />
            <RowStyle CssClass="row-style" />
            <AlternatingRowStyle CssClass="alt-row-style" />
            <PagerStyle CssClass="pager-style"/>
            <FooterStyle CssClass="footer-style" />
            <EmptyDataRowStyle CssClass="empty-style"/>
        </asp:GridView>
            

        <asp:ObjectDataSource ID="odsEventDates" runat="server" SelectMethod="GroupTicketsOnMonths" TypeName="Dashboard.BLL.Tickets">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="providerId" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="actionId" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="eventId" Type="Int32" />
                <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
                <asp:ControlParameter ControlID="ddlYears" PropertyName="SelectedValue" DefaultValue="0" Name="year" Type="Int32" />
           </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
