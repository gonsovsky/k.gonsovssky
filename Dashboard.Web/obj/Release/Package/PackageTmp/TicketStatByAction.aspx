﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master"
    CodeBehind="TicketStatByAction.aspx.cs" Inherits="Dashboard.Web.TicketStatByAction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="Server">
    <div class="container">
        <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
            <h2><asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h2>
            <p><asp:Literal ID="ltrCount" runat="server"></asp:Literal></p>            
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>
                    Выберите год:</p>
                <asp:DropDownList ID="ddlYears" Width="200px" runat="server" AutoPostBack="True"
                    DataSourceID="odsYears" class="btn btn-default" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="odsYears" runat="server" SelectMethod="GetAvailableYears"
                    TypeName="Dashboard.BLL.ShowActions"></asp:ObjectDataSource>
            </div>
            <div class="col-md-4">
                
            </div>
        </div>
        <br />
        <asp:Chart ID="chrCountTickets" runat="server" DataSourceID="odsTickets" ImageLocation="TempImages/ChartPic_#SEQ(300,3)"
            Width="800px" BorderWidth="2px" BackColor="WhiteSmoke" BackSecondaryColor="White"
            BackGradientStyle="TopBottom" BorderColor="#1A3B69" BorderlineDashStyle="Solid">
            <Titles>
                <asp:Title ShadowColor="32, 0, 0, 0" Text="Билетов по месяцам" Font="Trebuchet MS, 14.25pt, style=Bold"
                    ShadowOffset="3" ForeColor="26, 59, 105">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Enabled="False" IsTextAutoFit="False" Name="Default" BackColor="Transparent"
                    Font="Trebuchet MS, 8.25pt, style=Bold" Title="Legend Name">
                </asp:Legend>
            </Legends>
            <BorderSkin SkinStyle="Emboss"></BorderSkin>
            <Series>
                <asp:Series Name="Series1" BorderColor="180, 26, 59, 105" Color="#4c9721" XValueMember="Name" YValueMembers="Count">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="White"
                    BackColor="WhiteSmoke" ShadowColor="Transparent">
                    <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="False" Inclination="15" IsRightAngleAxes="True"
                        WallWidth="0" IsClustered="False" />
                    <AxisY LineColor="64, 64, 64, 64">
                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                        <MajorGrid LineColor="64, 64, 64, 64" />
                    </AxisY>
                    <AxisX LineColor="64, 64, 64, 64">
                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                        <MajorGrid LineColor="64, 64, 64, 64" />
                    </AxisX>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
        

        <br />

        <asp:Chart ID="chrPriceTickets" runat="server" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)" Width="800px" 
                BorderWidth="2px" BackColor="WhiteSmoke" BackSecondaryColor="White" BackGradientStyle="TopBottom" 
                BorderColor="#1A3B69" BorderlineDashStyle="Solid" DataSourceID="odsTickets">
                <Titles>
                    <asp:Title ShadowColor="32, 0, 0, 0" Text="Сумма по месяцам" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" ForeColor="26, 59, 105">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Enabled="False" IsTextAutoFit="False" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold" Title="Name">
                    </asp:Legend>
                </Legends>
                <BorderSkin SkinStyle="Emboss"></BorderSkin>
                <Series>
                    <asp:Series Name="Series1" BorderWidth="3" IsValueShownAsLabel="True" ShadowColor="Black" BorderColor="180, 26, 59, 105" Color="220, 224, 64, 10" ShadowOffset="2" XValueMember="Name"
                     XValueType="String" YValueType="Auto" YAxisType="Primary" YValueMembers="Price" ChartType="StepLine">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="White" BackColor="WhiteSmoke" ShadowColor="Transparent">
                        <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="False" Inclination="15" IsRightAngleAxes="True"
                            WallWidth="0" IsClustered="False" />
                        <AxisY LineColor="64, 64, 64, 64">
                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                            <MajorGrid LineColor="64, 64, 64, 64" />
                        </AxisY>
                        <AxisX LineColor="64, 64, 64, 64">
                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                            <MajorGrid LineColor="64, 64, 64, 64" />
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
           <asp:ObjectDataSource ID="odsTickets" runat="server" SelectMethod="GroupTicketsOnMonthsByActionId"
            TypeName="Dashboard.BLL.Tickets">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="0" Name="actionId" QueryStringField="actionId" Type="Int32" />
                <asp:ControlParameter ControlID="ddlYears" DefaultValue="0" Name="year" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
         <br />
        <asp:HyperLink ID="hplGroupTicketsOnMonthsByActionId" class="btn btn-primary btn-lg" runat="server">Скачать этот отчет</asp:HyperLink>
        
    </div>
</asp:Content>
