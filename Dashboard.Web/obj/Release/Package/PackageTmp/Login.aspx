﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Dashboard.Web.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Вход | Росгосцирк - Ключевые показатели эффективности</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Content/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body{padding: 80px 0;background-color: #293955;color: #FFF;}
        ul{padding: 0;list-style-type: none;}
    </style>
</head>
<body>
    <div class="container">
        <div class="form-signin">
            <form class="form-signin" runat="server">
            <h2 class="form-signin-heading">
                Вход</h2>
            <asp:LoginView ID="lgvM" runat="server">
                <AnonymousTemplate>
                    <asp:Login ID="Login" runat="server" FailureText="Неверный Email или пароль" FailureAction="RedirectToLoginPage">
                        <LayoutTemplate>
                            <asp:TextBox ID="UserName" Width="320px" CssClass="form-control" placeholder="Email" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="UserName"
                                SetFocusOnError="true" Display="Dynamic" ErrorMessage="Введите email." ToolTip="Введите email."
                                ValidationGroup="Login">Введите email.</asp:RequiredFieldValidator>
                            <br />
                            <asp:TextBox ID="Password" Width="320px" CssClass="form-control" placeholder="Пароль"
                                runat="server" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="Password"
                                SetFocusOnError="true" Display="Dynamic" ErrorMessage="Введите пароль." ToolTip="Введите пароль."
                                ValidationGroup="Login">Введите пароль.</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="Password"
                                SetFocusOnError="true" Display="Dynamic" ValidationExpression="^[0-9a-яА-Яa-zA-Z''-'\s]{4,12}$"
                                ErrorMessage="Неверный пароль." ToolTip="Неверный пароль." ValidationGroup="Login">Неверный пароль.</asp:RegularExpressionValidator>
                            <div class="checkbox">
                                <label>
                                    <asp:CheckBox ID="RememberMe" Checked="True" runat="server"></asp:CheckBox>Запомнить?
                                </label>
                            </div>
                            <asp:Button ID="Submit" CommandName="Login" Text="Войти" ValidationGroup="Login"
                                CssClass="btn btn-lg btn-primary btn-block" runat="server" />
                        </LayoutTemplate>
                    </asp:Login>
                </AnonymousTemplate>
                <LoggedInTemplate>
                    <asp:LoginStatus ID="LoginStatus1" runat="server" LogoutText="Выйти" />
                    <a href="Default.aspx">На главную</a>
                </LoggedInTemplate>
            </asp:LoginView>
            </form>
        </div>
    </div>
    <script src="Scripts/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="Scripts/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
