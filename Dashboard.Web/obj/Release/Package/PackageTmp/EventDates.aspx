﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master" CodeBehind="EventDates.aspx.cs" Inherits="Dashboard.Web.EventDates" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">
     <div class="container">
     <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
         <h2>Статистика по выступлениям</h2>
            <h2><asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h2>
            <p><asp:Literal ID="ltrCount" runat="server"></asp:Literal></p>            
        </div>
        <div class="row">
            <div class="col-md-3">
                <p>Выберите год:</p>
                <asp:DropDownList ID="ddlYears" Width="200px" runat="server" AutoPostBack="True"
                    DataSourceID="odsYears" class="btn btn-default" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="odsYears" runat="server" SelectMethod="GetAvailableYears"
                    TypeName="Dashboard.BLL.ShowActions"></asp:ObjectDataSource>
            </div>
            <div class="col-md-8">
            </div>
            </div>
            <br/>
            
            <asp:GridView ID="gdvEventDates" CssClass="grid-view" OnRowDataBound="gdvEventDates_OnRowDataBound" OnSorting="gdvEventDates_OnSorting" runat="server" DataSourceID="odsEventDates" 
         EnableViewState="False" ShowFooter="True" AutoGenerateColumns="False" PageSize="100" PagerSettings-Position="TopAndBottom" AllowSorting="True" 
        PagerSettings-Mode="Numeric" PagerSettings-PageButtonCount="10" AllowPaging="True">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Название" SortExpression="Name" />
                <asp:BoundField DataField="Count" HeaderText="Выступлений" SortExpression="Count" />
                <asp:BoundField DataField="Price" DataFormatString="{0:C0}" HeaderText="Продано, рублей" SortExpression="Price" />
                <asp:BoundField DataField="AveragePrice" DataFormatString="{0:C0}" HeaderText="Средняя цена билета" SortExpression="AveragePrice" />
            </Columns>
            <EmptyDataTemplate>
                <h4><asp:Literal ID="ltrEmptyData" runat="server" Text="Нет"></asp:Literal></h4>
            </EmptyDataTemplate>
            <HeaderStyle CssClass="th-row-style" />
            <RowStyle CssClass="row-style" />
            <AlternatingRowStyle CssClass="alt-row-style" />
            <PagerStyle CssClass="pager-style"/>
            <FooterStyle CssClass="footer-style" />
        </asp:GridView>
            

        <asp:ObjectDataSource ID="odsEventDates" runat="server" SelectMethod="GetSortableEventDatesOnMonths" TypeName="Dashboard.BLL.EventDates">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="actionId" Type="Int32" />
                <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
                <asp:ControlParameter ControlID="ddlYears" PropertyName="SelectedValue" DefaultValue="0" Name="year" Type="Int32" />
           </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
