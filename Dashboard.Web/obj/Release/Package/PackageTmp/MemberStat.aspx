﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master" CodeBehind="MemberStat.aspx.cs" Inherits="Dashboard.Web.MemberStat" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">
     <div class="container">
    <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
            <h2>Статистика по кассирам</h2>
            <h3><asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h3>
            <h4><asp:Literal ID="ltrSubTitle" runat="server"></asp:Literal></h4>
            <p><asp:Literal ID="ltrStat" runat="server"></asp:Literal></p>            
        </div>
    <div class="row">
            <div class="col-md-3">
               <asp:GridView ID="gdvMembers" CssClass="grid-view" OnRowDataBound="gdvProviders_OnRowDataBound" OnSorting="gdvProviders_OnSorting" runat="server" DataSourceID="odsMembers" 
        DataKeyNames="Id" EnableViewState="False" ShowFooter="True" AutoGenerateColumns="False" PageSize="100" PagerSettings-Position="TopAndBottom" AllowSorting="True" 
        PagerSettings-Mode="Numeric" PagerSettings-PageButtonCount="10" AllowPaging="True">
            <Columns>
                <asp:BoundField DataField="Email" HeaderText="Кассир" SortExpression="Email" />
                <asp:BoundField DataField="Count" HeaderText="Продано, билетов" SortExpression="Count" />
                <asp:BoundField DataField="Price" DataFormatString="{0:C0}" HeaderText="Продано, рублей" SortExpression="Price" />
            </Columns>
            <EmptyDataTemplate>
                <h4><asp:Literal ID="ltrEmptyData" runat="server" Text="Нет"></asp:Literal></h4>
            </EmptyDataTemplate>
            <HeaderStyle CssClass="th-row-style" />
            <RowStyle CssClass="row-style" />
            <AlternatingRowStyle CssClass="alt-row-style" />
            <PagerStyle CssClass="pager-style"/>
            <FooterStyle CssClass="footer-style" />
        </asp:GridView>
            </div>
        <div class="col-md-8">
            <asp:Chart ID="chtMembers" DataSourceID="odsMembers" runat="server" Palette="BrightPastel"
                BackColor="#D3DFF0" Height="300px" Width="500px" BorderlineDashStyle="Solid"
                BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105" IsSoftShadows="False"
                ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)">
                <Legends>
                    <asp:Legend Enabled="False" TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
                        IsEquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" IsTextAutoFit="False"
                        Name="Default">
                    </asp:Legend>
                </Legends>
                <BorderSkin SkinStyle="None"></BorderSkin>
                <Series>
                    <asp:Series ChartArea="Area1" XValueMember="Email" YValueMembers="Price" XValueType="String"
                        Name="Series1" ChartType="Doughnut" Font="Trebuchet MS, 8.25pt, style=Bold"
                        MarkerStyle="Circle" BorderColor="64, 64, 64, 64" Color="180, 65, 140, 240" YValueType="Double">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="Area1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                        BackColor="Transparent" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                        <AxisY2>
                            <MajorGrid Enabled="False" />
                            <MajorTickMark Enabled="False" />
                        </AxisY2>
                        <AxisX2>
                            <MajorGrid Enabled="False" />
                            <MajorTickMark Enabled="False" />
                        </AxisX2>
                        <Area3DStyle PointGapDepth="900" Rotation="162" IsRightAngleAxes="False" WallWidth="25"
                            IsClustered="False" />
                        <AxisY LineColor="64, 64, 64, 64">
                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                            <MajorGrid LineColor="64, 64, 64, 64" Enabled="False" />
                            <MajorTickMark Enabled="False" />
                        </AxisY>
                        <AxisX LineColor="64, 64, 64, 64">
                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                            <MajorGrid LineColor="64, 64, 64, 64" Enabled="False" />
                            <MajorTickMark Enabled="False" />
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
        </div>
       
       <br/>
       
            

        <asp:ObjectDataSource ID="odsMembers" runat="server" SelectMethod="GroupMembersOnTickets" TypeName="Dashboard.BLL.Members">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="providerId" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="actionId" Type="Int32" />
                <asp:QueryStringParameter DefaultValue="0" Name="eventId" QueryStringField="eventId" Type="Int32" />
                <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
       
        <ul class="nav nav-pills" role="tablist">
            <li role="presentation"><a href="ProvidersStat.aspx">Список цирков</a></li>
            <li role="presentation"><asp:HyperLink ID="hplShowActions" runat="server"></asp:HyperLink></li>
            <li role="presentation"><asp:HyperLink ID="hplEventDates" runat="server"></asp:HyperLink></li>
            </ul>
       
                        <br/>

    </div>
</asp:Content>
