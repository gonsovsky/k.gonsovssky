﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master"
    CodeBehind="ProviderAnalytics.aspx.cs" Inherits="Dashboard.Web.ProviderAnalytics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="Server">
    <div class="container">
         <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
            <h2>Аналитика по циркам</h2>
            <p>Объем выпущенных и проданных билетов, а также остатки в количественном и рублевом эквиваленте</p>
            <p>Данная статистика показывает эффективность продажи билетов в каждом цирке</p>            
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>
                    Выберите год:</p>
                <asp:DropDownList ID="ddlYears" Width="200px" runat="server" AutoPostBack="True"
                    DataSourceID="odsYears" class="btn btn-default" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="odsYears" runat="server" SelectMethod="GetAvailableYears"
                    TypeName="Dashboard.BLL.ShowActions"></asp:ObjectDataSource>
            </div>
        </div>
        
        <asp:DropDownList ID="ChartTypeList" runat="server" AutoPostBack="True" CssClass="spaceright">
            <asp:ListItem Value="StackedBar" Selected="True">StackedBar</asp:ListItem>
            <asp:ListItem Value="StackedColumn">StackedColumn</asp:ListItem>
            <asp:ListItem Value="StackedArea">StackedArea</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Chart ID="Chart1" Height="900px" Width="900px" runat="server" ImageLocation="TempImages/ChartPic_#SEQ(300,3)" BorderWidth="2px" BackColor="WhiteSmoke" BackSecondaryColor="White"
            BackGradientStyle="TopBottom" BorderColor="#1A3B69" BorderlineDashStyle="Solid">
            
            <Legends>
                <asp:Legend Enabled="True" IsTextAutoFit="False" Name="Default" BackColor="Transparent"
                    Font="Trebuchet MS, 8.25pt, style=Bold" Title="Секторы">
                </asp:Legend>
            </Legends>
            <BorderSkin SkinStyle="Emboss"></BorderSkin>
            <Series>
                <asp:Series Name="Выпущено" ChartType="StackedArea100" BorderColor="180, 26, 59, 105"
                    Color="220, 252, 180, 65">
                </asp:Series>
                <asp:Series Name="Продано" ChartType="StackedArea100" BorderColor="180, 26, 59, 105"
                    Color="#4c9721">
                </asp:Series>
                <asp:Series Name="Непродано" ChartType="StackedArea100" BorderColor="180, 26, 59, 105"
                    Color="220, 224, 64, 10">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BorderDashStyle="Solid"
                    BackSecondaryColor="Transparent" BackColor="64, 165, 191, 228" ShadowColor="Transparent"
                    BackGradientStyle="TopBottom">
                    <Area3DStyle Rotation="10" Inclination="15" WallWidth="0" />
                    <Position Y="3" Height="92" Width="92" X="2"></Position>
                    <AxisY LineColor="64, 64, 64, 64" LabelAutoFitMaxFontSize="8">
                        <LabelStyle Font="Trebuchet MS, 10.25pt, style=Bold" />
                        <MajorGrid LineColor="64, 64, 64, 64" />
                    </AxisY>
                    <AxisX LineColor="64, 64, 64, 64" IntervalType="Auto" LabelAutoFitMaxFontSize="8">
                        <LabelStyle Font="Trebuchet MS, 10.25pt, style=Bold" />
                        <MajorGrid LineColor="64, 64, 64, 64" />
                    </AxisX>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
        
        
        <p>Данный график иллюстрирует количество выпущенных, проданных и непроданных билетов</p>
        <br />
    </div>
</asp:Content>
