﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard.Master" CodeBehind="TicketFilter.aspx.cs" Inherits="Dashboard.Web.TicketFilter" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">
     <div class="container">
     <div class="bs-callout bs-callout-info" id="callout-tabs-extends-component">
         <h2>Статистика</h2>
     </div>     
       
        <div class="row">
            <div class="col-md-4">
               <p>Выберите цирк:</p>
                <asp:DropDownList ID="ddlProviders" Width="340px" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="Id"
                    class="btn btn-default" OnSelectedIndexChanged="ddlProviders_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-4">
                <p>Выберите программу:</p>
                <asp:DropDownList ID="ddlActions" Width="340px" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="Id"
                    class="btn btn-default" OnSelectedIndexChanged="ddlActions_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-4">
               <p>Выберите выступление:</p>
                <asp:DropDownList ID="ddlEventDates" Width="340px" runat="server" AutoPostBack="True" DataTextField="ActionDate" DataValueField="Id"
                    class="btn btn-default" OnSelectedIndexChanged="ddlEventDates_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            
            </div>
             
            <br/>
            
 <asp:GridView ID="gdvTickets" CssClass="grid-view" OnRowDataBound="gdvTickets_OnRowDataBound" OnSorting="gdvTickets_OnSorting" runat="server" DataSourceID="odsEventDates" 
         EnableViewState="False" ShowFooter="True" AutoGenerateColumns="False" PageSize="100" PagerSettings-Position="TopAndBottom" AllowSorting="True" 
        PagerSettings-Mode="Numeric" PagerSettings-PageButtonCount="10" AllowPaging="True">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Название" SortExpression="Name" />
                <asp:BoundField DataField="TotalActions" HeaderText="Программ" SortExpression="TotalActions" />
                <asp:BoundField DataField="TotalEvents" HeaderText="Выступлений" SortExpression="TotalEvents" />
                <asp:BoundField DataField="TotalTickets" HeaderText="Продано, билетов" SortExpression="TotalTickets" />
                <asp:BoundField DataField="Price" DataFormatString="{0:C0}" HeaderText="Продано, рублей" SortExpression="Price" />
                <asp:BoundField DataField="AveragePrice" DataFormatString="{0:C0}" HeaderText="Средняя цена билета" SortExpression="AveragePrice" />
            </Columns>
            <EmptyDataTemplate>
                <div class="alert alert-danger" role="alert">
                    Данные по выбранным параметрам отсутствуют. Попробуйте изменить параметры запроса. 
                </div>
            </EmptyDataTemplate>
            <HeaderStyle CssClass="th-row-style" />
            <RowStyle CssClass="row-style" />
            <AlternatingRowStyle CssClass="alt-row-style" />
            <PagerStyle CssClass="pager-style"/>
            <FooterStyle CssClass="footer-style" />
            <EmptyDataRowStyle CssClass="empty-style"/>
        </asp:GridView>
            

        <asp:ObjectDataSource ID="odsEventDates" runat="server" SelectMethod="GetSortableTicketsOnMonths" TypeName="Dashboard.BLL.Tickets">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlProviders" PropertyName="SelectedValue" DefaultValue="0" Name="providerId" Type="Int32" />
                <asp:ControlParameter ControlID="ddlActions" PropertyName="SelectedValue" DefaultValue="0" Name="actionId" Type="Int32" />
                <asp:ControlParameter ControlID="ddlEventDates" PropertyName="SelectedValue" DefaultValue="0" Name="eventId" Type="Int32" />
                <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
                <asp:Parameter DefaultValue="2015" Name="year" Type="Int32" />
           </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
