﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefaultOld.aspx.cs" Inherits="Dashboard.Web.DefaultOld" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Росгосцирк: Ключевые показатели эффективности</title>
    <link rel="icon" href="~/favicon.ico" />
    <link href="~/Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="~/Content/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body{background-color: #293955;}
        .title{ margin: 0 auto;color: #FFF;text-align: center;text-shadow: 1px 1px #666666;}
        .row
        {
            padding: 0 0 0 4px;
            margin: 0 0 0 0;
            background-color: #293955;
        }
        .row a:hover{ text-decoration: none;}
        .col-md-3, .col-md-6
        {
            padding: 0;
            margin: 0 0 0 0;
            text-align: center;
        }
        .small-cell
        {
            /*width: 280px;*/
            height: 290px;
            color: #FFF;
            padding: 0;
            margin: 4px 2px 0 2px;
            opacity: 0.9;
            cursor: pointer;overflow: hidden;
        }
        .large-cell
        {
            width: 564px;
            height: 290px;
            color: #FFF;
            padding: 0;
            margin: 4px 2px 0 2px;
            opacity: 0.9;
            cursor: pointer;overflow: hidden;
        }
        .large-cell:hover, .small-cell:hover
        {
            opacity: 1.0;
            text-shadow: 1px 1px #666666;
        }
        .text-center{ margin: 0 0 0 0;padding: 10px 0 20px 0;}
        .list-group-item{ background-color: transparent;border-bottom: none;border-left: none;border-right: none;margin: 0 10px 10px 10px;}
        .list-group-item h6{ margin: 0;padding: 0;}
        .list-group-item span{ float: right;}
    </style>
    
</head>
<body>
    <div class="container">
        <div class="title">
            <h2>Росгосцирк: Ключевые показатели эффективности</h2>
        </div>
        <form id="form1" runat="server">
            <div class="row">
            <div class="col-md-3">
                <a href="ProvidersStat.aspx">
                <div class="small-cell" style="background-color: #DC5945;">
                    <h3 class="text-center">Цирки</h3>
                    <span class="glyphicon glyphicon-bookmark" style="color: #EFAD1B;"></span>
                    <h1 class="text-center">
                        <asp:Literal ID="ltrProviders" runat="server"></asp:Literal>
                    </h1>
                    <p>Онлайн цирков</p>
                    <h2 class="text-center">
                        <asp:Literal ID="ltrShows" runat="server"></asp:Literal>
                    </h2>
                    <p>Суммарный объем продаж</p>
                </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="ShowActions.aspx">
                <div class="small-cell" style="background-color: #D35400;">
                    <h3 class="text-center">Программы</h3>
                    <span class="glyphicon glyphicon-list-alt" style="color:#EFAD1B;"></span>
                    <h1 class="text-center">
                        <asp:Literal ID="ltrShowActions" runat="server"></asp:Literal>
                    </h1>
                    <p>Цирковых программ</p>
                    <h2 class="text-center">
                        <asp:Literal ID="ltrEventDates" runat="server"></asp:Literal>
                    </h2>
                    <p>Проведено выступлений</p>
                </div>
                </a>
            </div>
            <div class="col-md-3">
                 <a href="EventDates.aspx">
                <div class="small-cell" style="background-color: #00B6CF;">
                    <h3 class="text-center">Выступления</h3>
                     <span class="glyphicon glyphicon-star" style="color:#EFAD1B;"></span>
                    <h1 class="text-center">
                        <asp:Literal ID="ltrEvents" runat="server"></asp:Literal>
                    </h1>
                    <p>Проведено выступлений</p>
                    <h2 class="text-center">
                        <asp:Literal ID="ltrFillPercent" runat="server"></asp:Literal></h2>
                    <p>
                        Средняя заполняемость залов</p>
                </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="TicketFilter.aspx">
                <div class="small-cell" style="background-color: #DC5945;">
                    <h3 class="text-center">Билеты</h3>
                    <span class="glyphicon glyphicon-credit-card" style="color: #EFAD1B;"></span>
                    <h1 class="text-center">
                        <asp:Literal ID="ltrTicketsCount" runat="server"></asp:Literal></h1>
                    <p>Продано билетов за текущий год</p>
                        <h2 class="text-center">
                        <asp:Literal ID="ltrAvgTicketPrice" runat="server"></asp:Literal></h2>
                    <p>
                        Средняя цена билета</p>
                </div>
                </a>
            </div>
             </div>
            <div class="row">
                 <div class="col-md-3">
                    <a href="TicketTotals.aspx">
                    <div class="small-cell" style="background-color: #F39C12;">
                        <h3 class="text-center">Тренд</h3>
                        <asp:Literal ID="ltrRunningTotal" runat="server"></asp:Literal>
                    </div>
                    </a>
                </div>
                 <div class="col-md-3">
                     <a href="ShowActions.aspx">
                    <div class="small-cell" style="background-color:#96BF48;">
                        <asp:Chart id="chtTopShowActions" DataSourceID="odsProv" runat="server" ImageLocation="TempImages/ChartPic_#SEQ(290,3)"
                            Width="280" Height="290px" BorderWidth="0px" BackColor="#96BF48" 
                            BackGradientStyle="None" BorderColor="Transparent" BorderlineDashStyle="NotSet">
                            <Titles>
                                <asp:Title Text="10 лучших программ" Font="Trebuchet MS, 18.00pt" ShadowOffset="0" ForeColor="#FFFFFF">
                                </asp:Title>
                                </Titles>
							<legends>
								<asp:Legend Enabled="False">
								</asp:Legend>
							</legends>
							<borderskin SkinStyle="None"></borderskin>
							<series>
								<asp:Series Name="Series1" ChartArea="Area1" XValueMember="Name" XValueType="String" YValueMembers="Percent" YValueType="Int32" ChartType="Pie"
                                Font="Trebuchet MS, 10pt" CustomProperties="PieDrawingStyle=Default,PieLabelStyle=Outside" ToolTip="#VALX  #VALY%" MarkerStyle="Circle"
                                 BorderColor="64, 64, 64, 64" Color="180, 65, 140, 240" Label="#VALY%">
								</asp:Series>
							</series>
							<chartareas>
								<asp:ChartArea Name="Area1" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BackGradientStyle="TopBottom">
									
									<area3dstyle PointGapDepth="900" Rotation="162" IsRightAngleAxes="False" WallWidth="25" IsClustered="False" />
									<axisy LineColor="64, 64, 64, 64">
										<LabelStyle Font="Trebuchet MS, 10pt" />
										<MajorGrid LineColor="64, 64, 64, 64" Enabled="False" />
										<MajorTickMark Enabled="False" />
									</axisy>
									<axisx LineColor="64, 64, 64, 64">
										<LabelStyle Font="Trebuchet MS, 10pt" />
										<MajorGrid LineColor="64, 64, 64, 64" Enabled="False" />
										<MajorTickMark Enabled="False" />
									</axisx>
								</asp:ChartArea>
							</chartareas>
						</asp:Chart>

                         <asp:ObjectDataSource ID="odsProv" runat="server" SelectMethod="GetTopShowActions" TypeName="Dashboard.BLL.ShowActions">
                             <SelectParameters>
                                 <asp:Parameter DefaultValue="10" Name="top" Type="Int32" />
                                 <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
                                 <asp:Parameter DefaultValue="2015" Name="year" Type="Int32" />
                             </SelectParameters>
                          </asp:ObjectDataSource>
                          <div id="tip" style="width:40px;height:1px;margin: 0 auto"></div>
                    </div>
                    </a>
                </div>
                <div class="col-md-6"><a href="ShowTickets.aspx">
                    <div class="large-cell" style="background-color: #EFAD1B;">
                        
                        <asp:Chart ID="chrVisits" runat="server" DataSourceID="odsVisits" ImageLocation="TempImages/ChartPic_#SEQ(290,3)"
                            Width="500px" Height="280" BorderWidth="0px" BackColor="#EFAD1B" 
                            BackGradientStyle="None" BorderColor="Transparent" BorderlineDashStyle="NotSet">
                            <Titles>
                                <asp:Title Text="Билеты, шт" Font="Trebuchet MS, 18.00pt" ShadowOffset="0" ForeColor="#FFFFFF">
                                </asp:Title>
                                </Titles>
                            <BorderSkin SkinStyle="None"></BorderSkin>
                            <Series>
                                <asp:Series Name="Series1" BorderColor="180, 26, 59, 105" Color="#4c9721" XValueMember="Name"
                                    YValueMembers="Count">
                                </asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1" BackColor="Transparent">
                                    
                                    <AxisY LineColor="64, 64, 64, 64">
                                        <LabelStyle ForeColor="#293955" Font="Trebuchet MS, 8.25pt" />
                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                    </AxisY>
                                    <AxisX LineColor="64, 64, 64, 64">
                                        <LabelStyle ForeColor="#293955" Font="Trebuchet MS, 8.25pt" />
                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                    </AxisX>
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                        <asp:ObjectDataSource ID="odsVisits" runat="server" SelectMethod="GroupTicketsOnMonths"
                            TypeName="Dashboard.BLL.Tickets">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="0" Name="providerId" Type="Int32" />
                                <asp:Parameter DefaultValue="0" Name="actionId" Type="Int32" />
                                <asp:Parameter DefaultValue="0" Name="eventId" Type="Int32" />
                                <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
                                <asp:Parameter DefaultValue="2015" Name="year" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                    </a>
                </div>
               
                
            </div>
            <div class="row">
                <div class="col-md-6"><a href="ShowTickets.aspx">
                    <div class="large-cell" style="background-color: #388898;">
                        <div class="large-cell" style="background-color: #388898;margin-bottom: 4px;">
                        
                        <asp:Chart ID="Chart1" runat="server" DataSourceID="odsEventDates" ImageLocation="TempImages/ChartPic_#SEQ(290,3)"
                            Width="560px" Height="290" BorderWidth="0px" BackColor="#388898" 
                            BackGradientStyle="None" BorderColor="Transparent" BorderlineDashStyle="NotSet">
                            <Titles>
                                <asp:Title Text="Объем продаж, млн ₽" Font="Trebuchet MS, 18.00pt" ShadowOffset="0" ForeColor="#FFFFFF">
                                </asp:Title>
                                </Titles>
                            <BorderSkin SkinStyle="None"></BorderSkin>
                            <Series>
                                <asp:Series Name="Series1" ChartType="StepLine" LabelFormat="C0" Font="Trebuchet MS, 10pt" LabelForeColor="#FFFFFF" IsValueShownAsLabel="True"  MarkerStyle="Circle" MarkerSize="7" Label="#VALY" MarkerColor="224, 64, 10" BorderWidth="3" BorderColor="#DC5945" Color="#DC5945" XValueMember="Name"
                                 YValueType="Int32" YValueMembers="PriceResult">
                                </asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1" BackColor="Transparent">
                                    <AxisY LineColor="64, 64, 64, 64">
                                        <LabelStyle Format="C0" ForeColor="#FFFFFF" Font="Trebuchet MS, 10pt" />
                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                    </AxisY>
                                    <AxisX IsMarginVisible="False" LineColor="64, 64, 64, 64">
                                        <LabelStyle ForeColor="#FFFFFF" Font="Trebuchet MS, 10pt" />
                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                    </AxisX>
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GroupEventDatesOnMonths" TypeName="Dashboard.BLL.EventDates">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="0" Name="actionId" Type="Int32" />
                                <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
                                <asp:Parameter DefaultValue="2015" Name="year" Type="Int32" />
                                </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                    </div>
                    </a>
                </div>
                <div class="col-md-6"><a href="EventDates.aspx">
                    <div class="large-cell" style="background-color: #3498DB;margin-bottom: 4px;">
                        
                        <asp:Chart ID="chtEventDates" runat="server" DataSourceID="odsEventDates" ImageLocation="TempImages/ChartPic_#SEQ(290,3)"
                            Width="560px" Height="290" BorderWidth="0px" BackColor="#3498DB" 
                            BackGradientStyle="None" BorderColor="Transparent" BorderlineDashStyle="NotSet">
                            <Titles>
                                <asp:Title Text="Выступления" Font="Trebuchet MS, 18.00pt" ShadowOffset="0" ForeColor="#FFFFFF">
                                </asp:Title>
                                </Titles>
                            <BorderSkin SkinStyle="None"></BorderSkin>
                            <Series>
                                <asp:Series Name="Series1" ChartType="Spline" MarkerStyle="Circle" MarkerSize="7" Label="#VALY" LabelForeColor="#FFFFFF" MarkerColor="224, 64, 10" BorderWidth="3" BorderColor="#DC5945" Color="#DC5945" XValueMember="Name"
                                    YValueMembers="Count">
                                </asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1" BackColor="Transparent">
                                    
                                    <AxisY LineColor="64, 64, 64, 64">
                                        <LabelStyle ForeColor="#FFFFFF" Font="Trebuchet MS, 8.25pt" />
                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                    </AxisY>
                                    <AxisX IsMarginVisible="False" LineColor="64, 64, 64, 64">
                                        <LabelStyle ForeColor="#FFFFFF" Font="Trebuchet MS, 8.25pt" />
                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                    </AxisX>
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                        <asp:ObjectDataSource ID="odsEventDates" runat="server" SelectMethod="GroupEventDatesOnMonths" TypeName="Dashboard.BLL.EventDates">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="0" Name="actionId" Type="Int32" />
                                <asp:Parameter DefaultValue="4" Name="statusId" Type="Int32" />
                                <asp:Parameter DefaultValue="2015" Name="year" Type="Int32" />
                                </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                    </a>
                </div>
            </div>
        </form>
    </div>
    <br />
    <br />
    <footer class="footer">
      <div class="container">
        <p class="text-muted">&copy; 2015, Росгосцирк, ФКП «Российская государственная цирковая компания»,  109012 г. Москва, ул. Пушечная, д.4, стр. 1</p>
      </div>
    </footer>
</body>
<script src="Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="Scripts/bootstrap.min.js" type="text/javascript"></script>
   

</html>
