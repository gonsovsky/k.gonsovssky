﻿using System;
using System.Web;

namespace Dashboard.Web
{
    public class Global : HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            // in order to replace default page bug 
            var app = (HttpApplication)sender;
            if (app.Context.Request.Url.LocalPath.EndsWith("/"))
            {
                app.Context.RewritePath(string.Concat(app.Context.Request.Url.LocalPath, "default.aspx"));
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}