﻿using System.Configuration;
using System.Data;
using System.Data.Common;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Data
{
    public abstract class DataAccess
    {
        protected virtual string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["SqlServer"].ToString(); }
        }

        public static TicketManager Tickets
        {
            get { return TicketManager.Instance; }
        }

        public static FormManager Forms
        {
            get { return FormManager.Instance; }
        }

        public static ShowActionManager ShowActions
        {
            get { return ShowActionManager.Instance; }
        }
        public static EventDateManager EventDates
        {
            get { return EventDateManager.Instance; }
        }
        public static StageManager Stages
        {
            get { return StageManager.Instance; }
        }
        public static OrderManager Orders
        {
            get { return OrderManager.Instance; }
        }
        public static WebStatManager WebStats
        {
            get { return WebStatManager.Instance; }
        }
        public static MemberManager Members
        {
            get { return MemberManager.Instance; }
        }
        public static LogManager Logs
        {
            get { return LogManager.Instance; }
        }
        public static PaymentManager Payments
        {
            get { return PaymentManager.Instance; }
        }
        
        protected int ExecuteNonQuery(DbCommand cmd)
        {
            return cmd.ExecuteNonQuery();
        }

        protected IDataReader ExecuteReader(DbCommand dbCommand)
        {
            return ExecuteReader(dbCommand, CommandBehavior.Default);
        }

        protected IDataReader ExecuteReader(DbCommand cmd, CommandBehavior behavior)
        {
            return cmd.ExecuteReader(behavior);
        }

        protected object ExecuteScalar(DbCommand cmd)
        {
            return cmd.ExecuteScalar();
        }
    }
}