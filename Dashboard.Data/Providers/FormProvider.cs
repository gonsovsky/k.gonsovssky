﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dashboard.Common;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Providers
{
    public class FormProvider : FormManager
    {
        #region FormTypes

        public override List<Form> GetFormTypes()
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_GetFormTypes", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return GetFormTypeCollectionFromReader(ExecuteReader(cmd));
            }
        }

        #endregion

        #region Forms

        public override List<Form> GetFormsByType(int typeId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_GetFormsByType", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FormTypeId", SqlDbType.Int).Value = typeId;
                cn.Open();
                return GetFormCollectionFromReader(ExecuteReader(cmd));
            }
        }

        public override Form GetFormByFormId(int id)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_GetFormByFormId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetFormFromReader(reader);
                else
                    return null;
            }
        }
        
        public override int CountForms()
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_CountForms", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return (int) ExecuteScalar(cmd);
            }
        }
        
        public override int InsertForm(Form form)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_InsertForm", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("FormTypeId", SqlDbType.Int).Value = form.FormTypeId;
                cmd.Parameters.Add("Name", SqlDbType.VarChar).Value = form.Name;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (int)cmd.Parameters["@Id"].Value;
            }
        }

        /// <summary>
        /// Updates an Form
        /// </summary>
        public override bool UpdateForm(Form form)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_UpdateForm", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("Id", SqlDbType.Int).Value = form.Id;
                cmd.Parameters.Add("FormTypeId", SqlDbType.Int).Value = form.FormTypeId;
                cmd.Parameters.Add("Name", SqlDbType.VarChar).Value = form.Name;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        
        public override bool DeleteFormByFormId(int id)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_DeleteFormByFormId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region FormFields

        public override List<Form> GetFormFieldsByFormId(int formId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_GetFormFieldsByFormId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FormId", SqlDbType.Int).Value = formId;
                cn.Open();
                return GetFormFieldCollectionFromReader(ExecuteReader(cmd));
            }
        }

        public override Form GetFormFieldById(int id)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_GetFormFieldById", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetFormFieldFromReader(reader);
                else
                    return null;
            }
        }

        public override int CountFormFieldsByFormId(int formId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_CountFormFieldsByFormId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FormId", SqlDbType.Int).Value = formId;
                cn.Open();
                return (int) ExecuteScalar(cmd);
            }
        }

        public override int InsertFormField(Form form)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_InsertFormField", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("FormId", SqlDbType.Int).Value = form.FormId;
                cmd.Parameters.Add("Name", SqlDbType.NVarChar).Value = form.Name;
                cmd.Parameters.Add("Value", SqlDbType.NVarChar).Value = form.Value;
                cmd.Parameters.Add("XPos", SqlDbType.Int).Value = form.XPos;
                cmd.Parameters.Add("YPos", SqlDbType.Int).Value = form.YPos;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (int)cmd.Parameters["@Id"].Value;
            }
        }

        public override bool UpdateFormField(Form form)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_UpdateFormField", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("Id", SqlDbType.Int).Value = form.Id;
                cmd.Parameters.Add("FormId", SqlDbType.Int).Value = form.FormId;
                cmd.Parameters.Add("Name", SqlDbType.NVarChar).Value = form.Name;
                cmd.Parameters.Add("Value", SqlDbType.NVarChar).Value = form.Value;
                cmd.Parameters.Add("XPos", SqlDbType.Int).Value = form.XPos;
                cmd.Parameters.Add("YPos", SqlDbType.Int).Value = form.YPos;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public override bool DeleteFormFieldById(int id)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Forms_DeleteFormFieldById", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion
    }
}