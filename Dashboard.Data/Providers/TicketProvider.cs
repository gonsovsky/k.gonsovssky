﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Common.Domain;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Providers
{
    public class TicketProvider : TicketManager
    {
        public override Ticket GetTicketByTicketId(int orderId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_GetTicketBySeatId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SeatId", SqlDbType.Int).Value = orderId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTicketFromReader(reader);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves Tickets collection
        /// </summary>
        public override List<Ticket> GetPagedTickets(int startRowIndex, int maximumRows, int memberId, int eventId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_GetPagedSeats", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cn.Open();
                return GetTicketCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Gets Tickets By EventId
        /// </summary>
        public override List<Ticket> GetTicketsByEventId(int eventId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_GetSeatsByEventId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cn.Open();
                return GetTicketCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Counts Ticket items
        /// </summary>
        public override int CountTickets(int memberId, int eventId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_CountSeats", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Sums Sold Tickets on Count and Price
        /// </summary>
        public override Group SumSoldTickets(int providerId, int actionId, int statusId, int year)
        {
            Group group = null;
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_SumSoldSeats", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    group = new Group()
                    {
                        Count = (int)reader["Count"]
                    };

                    if (reader["Price"] != DBNull.Value)
                    {
                        group.Price = (decimal)reader["Price"];
                    }
                    if (reader["AveragePrice"] != DBNull.Value)
                    {
                        group.AveragePrice = (decimal)reader["AveragePrice"];
                    }
                }
            }
            return group;
        }

        /// <summary>
        /// Groups Tickets On Months By ActionId and By Year
        /// </summary>
        public override List<Group> GroupTicketsOnMonthsByActionId(int actionId, int year)
        {
            List<Group> months = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_GroupSeatsOnMonthsByActionId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    months.Add(new Group((int)reader["MonthId"], Group.GetMonthName((int)reader["MonthId"]), (int)reader["Count"], (decimal)reader["Price"]));
                }

                reader.Close();
                cn.Close();
            }
            return months;
        }

        /// <summary>
        /// Groups tickets average price on days
        /// </summary>
        public override List<Group> GroupTicketsOnDatesByProviderId(int providerId, int year)
        {
            List<Group> dates = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_GroupSeatsOnDatesByProviderId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Group group = new Group()
                    {
                        StartDate = (DateTime)reader["ActionDay"],
                        TotalCount = (int)reader["TotalCount"],
                        AveragePrice = (decimal)reader["AveragePrice"]
                    };
                    dates.Add(group);
                }

                reader.Close();
                cn.Close();
            }
            return dates;
        }

        /// <summary>
        /// Groups tickets on status by EventId
        /// </summary>
        /// <param name="eventId">EventId</param>
        /// <returns>List Groups</returns>
        public override List<Group> GroupTicketsOnStatusByEventId(int eventId)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_GroupSeatsOnStatusByEventId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Group group = new Group()
                    {
                        StatusId = (ItemStatus)reader["StatusId"],
                        Name = reader["Name"].ToString(),
                        Count = (int)reader["Count"],
                        Price = (decimal)reader["Price"]
                    };
                    groups.Add(group);
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        /// <summary>
        /// Groups tickets on months by providerId, actionId, eventId, statusId, and year.
        /// If StartDate and EndDate defined the selection only depends on a period.
        /// </summary>
        /// <param name="providerId">Provider Id or zero</param>
        /// <param name="actionId">Action Id or zero</param>
        /// <param name="eventId">EventId or zero</param>
        /// <param name="statusId">Status of tickets</param>
        /// <param name="year">Particular year is obligatory</param>
        /// <param name="startDate">StartDate of period otherwise MinValue</param>
        /// <param name="endDate">EndDate of period otherwise MinValue</param>
        /// <returns>List Group  as result</returns>
        public override List<Group> GroupTicketsOnMonths(int providerId, int actionId, int eventId, int statusId, int year, DateTime startDate, DateTime endDate)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_GroupSeatsOnMonths", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;

                if (startDate == DateTime.MinValue && endDate == DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                }
                else
                {
                    cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = startDate;
                    cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = endDate;
                }
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Group group = new Group()
                    {
                        Id = (int)reader["MonthId"],
                        Count = (int)reader["TotalActions"],
                        TotalCount = (int)reader["TotalEvents"],
                        CountResult = (int)reader["TotalTickets"],
                        Price = (decimal)reader["TotalPrice"],
                        AveragePrice = (decimal)reader["AveragePrice"]
                    };

                    group.Name = Group.GetMonthName(group.Id);
                    groups.Add(group);
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        /// <summary>
        /// Groups tickets on days by providerId, actionId, eventId, statusId, and year.
        /// If StartDate and EndDate defined the selection only depends on a period.
        /// </summary>
        /// <param name="providerId">Provider Id or zero</param>
        /// <param name="actionId">Action Id or zero</param>
        /// <param name="eventId">EventId or zero</param>
        /// <param name="statusId">Status of tickets</param>
        /// <param name="year">Particular year is obligatory</param>
        /// <param name="startDate">StartDate of period otherwise MinValue</param>
        /// <param name="endDate">EndDate of period otherwise MinValue</param>
        /// <returns>List Group  as result</returns>
        public override List<Group> GroupTicketsOnDays(int providerId, int actionId, int eventId, int statusId, int year, DateTime startDate, DateTime endDate)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_GroupSeatsOnDays", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;

                if (year > 0)
                {
                    cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                }

                if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
                {
                    cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = startDate;
                    cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = endDate;
                }

                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Group group = new Group()
                    {
                        CreatedDate = (DateTime)reader["SoldDate"],
                        Count = (int)reader["ActionsCount"],
                        TotalCount = (int)reader["EventsCount"],
                        CountResult = (int)reader["TicketsCount"],
                        Price = (decimal)reader["Price"],
                        AveragePrice = (decimal)reader["AveragePrice"]
                    };
                    groups.Add(group);
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        /// <summary>
        /// Groups ticket sales by weeks
        /// </summary>
        /// <param name="providerId">Ticket provider</param>
        /// <param name="statusId">Status of the ticket</param>
        /// <param name="year">Year number</param>
        /// <returns></returns>
        public override List<Group> RunningTotals(int providerId, int statusId, int year)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_RunningTotals", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Group group = new Group()
                    {
                        Id = (int)reader["Id"],
                        StartDate = (DateTime)reader["StartDate"],
                        EndDate = (DateTime)reader["EndDate"],
                        Count = (int)reader["Count"],
                        Price = (decimal)reader["Price"]
                    };
                    groups.Add(group);
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        /// <summary>
        /// Get Ticket Statuses
        /// </summary>
        public override List<Group> GetTicketStatus()
        {
            List<Group> statuses = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Tickets_GetTicketStatus", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);

                while (reader.Read())
                {
                    Group status = new Group()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString()
                    };
                    statuses.Add(status);
                }
                reader.Close();
                cn.Close();
            }
            return statuses;
        }

        /// <summary>
        /// Inserts a new Ticket
        /// </summary>
        public override int InsertTicket(Ticket ticket)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_InsertSeat", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = ticket.EventId;
                cmd.Parameters.Add("@SeatId", SqlDbType.Int).Value = ticket.SeatId;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = ticket.ActionId;
                cmd.Parameters.Add("@ActionDate", SqlDbType.SmallDateTime).Value = ticket.ActionDate;
                cmd.Parameters.Add("@Price", SqlDbType.Money).Value = ticket.Price;
                cmd.Parameters.Add("@RowNum", SqlDbType.VarChar).Value = ticket.RowNum;
                cmd.Parameters.Add("@SeatNum", SqlDbType.VarChar).Value = ticket.SeatNum;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = ticket.StatusId;
                cmd.Parameters.Add("@ReservType", SqlDbType.Int).Value = ticket.ReservType;
                cmd.Parameters.Add("@ReservAmount", SqlDbType.Int).Value = ticket.ReservAmount;
                cmd.Parameters.Add("@ReservedTill", SqlDbType.SmallDateTime).Value = ticket.ReservedTill;
                cmd.Parameters.Add("@ProhibitPrint", SqlDbType.Int).Value = ticket.ProhibitPrint;
                cmd.Parameters.Add("@SaleRuleId", SqlDbType.Int).Value = ticket.SaleRuleId;
                cmd.Parameters.Add("@BarCode", SqlDbType.NVarChar).Value = ticket.BarCode;
                cmd.Parameters.Add("@MultiStatus", SqlDbType.Int).Value = ticket.MultiStatus;
                cmd.Parameters.Add("@ExternalBarCode", SqlDbType.VarChar).Value = ticket.ExternalBarCode;
                cmd.Parameters.Add("@ExternalActionTicketId", SqlDbType.Int).Value = ticket.ExternalActionTicketId;
                cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = ticket.CreatedDate;
                cmd.Parameters.Add("@AliasId", SqlDbType.BigInt).Value = ticket.AliasId;
                cmd.Parameters.Add("@TrackId", SqlDbType.Int).Value = ticket.TrackId;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (int)cmd.Parameters["@Id"].Value;
            }
        }

        /// <summary>
        /// Updates a Ticket
        /// </summary>
        public override bool UpdateTicket(Ticket ticket)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_UpdateSeat", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = ticket.Id;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = ticket.EventId;
                cmd.Parameters.Add("@SeatId", SqlDbType.Int).Value = ticket.SeatId;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = ticket.ActionId;
                cmd.Parameters.Add("@ActionDate", SqlDbType.SmallDateTime).Value = ticket.ActionDate;
                cmd.Parameters.Add("@Price", SqlDbType.Money).Value = ticket.Price;
                cmd.Parameters.Add("@RowNum", SqlDbType.VarChar).Value = ticket.RowNum;
                cmd.Parameters.Add("@SeatNum", SqlDbType.VarChar).Value = ticket.SeatNum;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = ticket.StatusId;
                cmd.Parameters.Add("@ReservType", SqlDbType.Int).Value = ticket.ReservType;
                cmd.Parameters.Add("@ReservAmount", SqlDbType.Int).Value = ticket.ReservAmount;
                cmd.Parameters.Add("@ReservedTill", SqlDbType.SmallDateTime).Value = ticket.ReservedTill;
                cmd.Parameters.Add("@ProhibitPrint", SqlDbType.Int).Value = ticket.ProhibitPrint;
                cmd.Parameters.Add("@SaleRuleId", SqlDbType.Int).Value = ticket.SaleRuleId;
                cmd.Parameters.Add("@BarCode", SqlDbType.NVarChar).Value = ticket.BarCode;
                cmd.Parameters.Add("@MultiStatus", SqlDbType.Int).Value = ticket.MultiStatus;
                cmd.Parameters.Add("@ExternalBarCode", SqlDbType.VarChar).Value = ticket.ExternalBarCode;
                cmd.Parameters.Add("@ExternalActionTicketId", SqlDbType.Int).Value = ticket.ExternalActionTicketId;
                cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = ticket.CreatedDate;
                cmd.Parameters.Add("@AliasId", SqlDbType.BigInt).Value = ticket.AliasId;
                cmd.Parameters.Add("@TrackId", SqlDbType.Int).Value = ticket.TrackId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes Ticket By TicketId
        /// </summary>
        public override bool DeleteTicketByTicketId(int ticketId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_DeleteSeatBySeatId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TicketId", SqlDbType.Int).Value = ticketId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Delete Tickets By EventId
        /// </summary>
        public override bool DeleteTicketsByEventId(int eventId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Seats_DeleteSeatsByEventId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
    }
}