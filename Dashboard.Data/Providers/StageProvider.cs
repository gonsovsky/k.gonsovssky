﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Providers
{
    public class StageProvider : StageManager
    {
        #region City

        /// <summary>
        /// Retrieves Citys collection
        /// </summary>
        public override List<City> GetCities()
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Cities_GetCities", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return GetCityCollectionFromReader(ExecuteReader(cmd));
            }
        }

        #endregion

        #region Providers

        public override List<Provider> GetProvidersByMemberId(int memberId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Providers_GetProvidersByMemberId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cn.Open();
                return GetProviderCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Gets providers which do not enlisted in member's provider list
        /// </summary>
        public override List<Provider> GetProviders(int memberId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Providers_GetProviders", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cn.Open();
                return GetProviderCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Retrieves Providers list with show action count and event count.
        /// Note, the result is dependant on member's provider list.
        /// Groups Providers On Events By MemberId.
        /// </summary>
        /// <param name="memberId">MemberId is obligatory</param>
        /// <param name="year">Year is obligatory</param>
        /// <param name="month">Month is obligatory</param>
        /// <returns>Returns grouped list of Providers, Name, ShowActions count, Events count</returns>
        public override List<Group> GroupProvidersOnEventsByMemberId(int memberId, int year, int month)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Providers_GroupProvidersOnEventsByMemberId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = month;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    groups.Add(new Group()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Count = (int)reader["Count"],
                        TotalCount = (int)reader["TotalCount"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        public override Provider GetProviderByProviderId(int providerId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Providers_GetProviderByProviderId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetProviderFromReader(reader);
                else
                    return null;
            }
        }

        /// <summary>
        /// Groups Providers on Months by tickets 
        /// </summary>
        public override List<Group> GroupProviderOnMonths(int providerId, int statusId, int year)
        {
            List<Group> providers = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Providers_GroupProviderOnMonths", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    providers.Add(new Group()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString().Trim(),
                        ActionId = (int)reader["Month"],
                        Month = Group.GetMonthName((int)reader["Month"]),
                        Count = (int)reader["Count"],
                        Price = (decimal)reader["Price"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return providers;
        }

        /// <summary>
        /// Groups Providers on ShowActions count, EventDates count, Tickets count, and AveragePrice of a ticket within particular events
        /// </summary>
        /// <param name="providerId">Provider</param>
        /// <param name="statusId">Status of the ticket</param>
        /// <param name="year">Year</param>
        /// <returns>Returns ProviderId, ProviderName, ShowActions count, EventDates count, Tickets count</returns>
        public override List<Group> GroupProvidersOnShowActions(int providerId, int statusId, int year)
        {
            List<Group> providers = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Providers_GroupProvidersOnShowActions", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    providers.Add(new Group()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Count = (int)reader["Count"], // ShowActions
                        TotalCount = (int)reader["TotalCount"], // EventDates
                        CountResult = (int)reader["CountResult"], // Tickets
                        AveragePrice = (decimal)reader["AveragePrice"] // AveragePrice of a ticket within particular events
                    });
                }

                reader.Close();
                cn.Close();
            }
            return providers;
        }

        /// <summary>
        /// Groups Providers By Issued Tickets 
        /// </summary>
        public override List<Provider> GroupProvidersOnTickets(int statusId, int year, int month)
        {
            List<Provider> providers = new List<Provider>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Providers_GroupProvidersOnTickets", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = month;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    providers.Add(new Provider()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Count = (int)reader["Count"],
                        Price = (decimal)reader["Price"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return providers;
        }

        /// <summary>
        /// Retrieves Providers collection
        /// </summary>
        public override List<Provider> GetPagedProviders(int startRowIndex, int maximumRows, int memberId, int cityId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Providers_GetPagedProviders", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@CityId", SqlDbType.Int).Value = cityId;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cn.Open();
                return GetProviderCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Deletes Provider By ProviderId
        /// </summary>
        public override bool DeleteProviderByProviderId(int providerId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Providers_DeleteProviderByProviderId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Counts Provider items
        /// </summary>
        public override int CountProviders(int memberId, int cityId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Providers_CountProviders", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@CityId", SqlDbType.Int).Value = cityId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        #endregion

        #region Stages

        public override Stage GetStageByStageId(int stageId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetStageByStageId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StageId", SqlDbType.Int).Value = stageId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetStageFromReader(reader);
                else
                    return null;
            }
        }

        public override Stage GetSideBySideId(int sideId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetSideBySideId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SideId", SqlDbType.Int).Value = sideId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    Stage stage = new Stage()
                    {
                        SideId = (int)reader["Id"],
                        StageId = (int)reader["StageId"],
                        SideName = reader["Name"].ToString().Trim()
                    };
                    return stage;
                }
                else
                    return null;
            }
        }

        public override Stage GetSectorBySectorId(int sectorId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetSectorBySectorId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SectorId", SqlDbType.Int).Value = sectorId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    Stage stage = new Stage()
                    {
                        SectorId = (int)reader["Id"],
                        SideId = (int)reader["SideId"],
                        SectorName = reader["Name"].ToString().Trim()
                    };
                    return stage;
                }
                else
                    return null;
            }
        }

        public override Stage GetSeatBySeatId(int seatId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetSeatBySeatId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SeatId", SqlDbType.Int).Value = seatId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    Stage seat = new Stage()
                    {
                        SeatId = (int)reader["Id"],
                        SectorId = (int)reader["SectorId"],
                        RowNum = (int)reader["RowNum"],
                        SeatNum = reader["SeatNum"].ToString(),
                        Price = (decimal)reader["Price"],
                        XPos = (int)reader["XPos"],
                        YPos = (int)reader["YPos"]
                    };
                    return seat;
                }
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves Stages collection
        /// </summary>
        public override List<Stage> GetPagedStages(int startRowIndex, int maximumRows, int memberId, int providerId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetPagedStages", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cn.Open();
                return GetStageCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Counts Stage items
        /// </summary>
        public override int CountStages(int memberId, int providerId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_CountStages", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public override int CountSidesByStageId(int stageId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_CountSidesByStageId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StageId", SqlDbType.Int).Value = stageId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public override int CountSectorsBySideId(int sideId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_CountSectorsBySideId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SideId", SqlDbType.Int).Value = sideId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public override int CountSectorsByStageId(int stageId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_CountSectorsByStageId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StageId", SqlDbType.Int).Value = stageId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public override int CountSeatsBySideId(int sideId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_CountSeatsBySideId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SideId", SqlDbType.Int).Value = sideId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public override int CountSeatsByStageId(int stageId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_CountSeatsByStageId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StageId", SqlDbType.Int).Value = stageId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public override int CountSeatsBySectorId(int sectorId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_CountSeatsBySectorId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SectorId", SqlDbType.Int).Value = sectorId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Groups Stages on Sides, Sectors, and Seats
        /// The result is dependent on parameter values
        /// </summary>
        /// <param name="providerId">Optional: use ProviderId = 0 to retrieve all the stages</param>
        /// <param name="stageId">Optional: use StageId = 0 to retrieve all the stages</param>
        /// <returns>List Group</returns>
        public override List<Group> GroupStagesOnSidesSectorsSeat(int providerId, int stageId)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GroupStagesOnSidesSectorsSeats", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@StageId", SqlDbType.Int).Value = stageId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Group group = new Group()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Count = (int)reader["CountSides"],
                        TotalCount = (int)reader["CountSectors"],
                        CountResult = (int)reader["CountSeats"]
                    };

                    groups.Add(group);
                }
            }
            return groups;
        }

        /// <summary>
        /// Groups StageSides On Sectors and Seats
        /// </summary>
        /// <param name="stageId">Stageid</param>
        /// <returns>List Group</returns>
        public override List<Stage> GetStageSidesByStageId(int stageId)
        {
            List<Stage> stages = new List<Stage>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetStageSidesByStageId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StageId", SqlDbType.Int).Value = stageId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Stage stage = new Stage()
                    {
                        SideId = (int)reader["Id"],
                        StageId = (int)reader["StageId"],
                        Name = reader["Name"].ToString()
                    };

                    stages.Add(stage);
                }
            }
            return stages;
        }

        /// <summary>
        /// Groups StageSectors On Seats
        /// </summary>
        /// <param name="sideId">SideId</param>
        /// <returns>List Group</returns>
        public override List<Group> GroupStageSectorsOnSeats(int sideId)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GroupStageSectorsOnSeats", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SideId", SqlDbType.Int).Value = sideId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Group group = new Group()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Count = (int)reader["CountSeats"]
                    };

                    groups.Add(group);
                }
            }
            return groups;
        }

        public override List<Group> GroupSeatsOnPrice(int stageId)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GroupSeatsOnPrice", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StageId", SqlDbType.Int).Value = stageId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Group group = new Group()
                    {
                        Price = (decimal)reader["Price"],
                        Count = (int)reader["CountSeats"]
                    };

                    groups.Add(group);
                }
            }
            return groups;
        }

        public override List<Group> GroupTicketSeatsOnPrice(int eventId)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GroupTicketSeatsOnPrice", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Group group = new Group()
                    {
                        Price = (decimal)reader["Price"],
                        Count = (int)reader["CountSeats"]
                    };

                    groups.Add(group);
                }
            }
            return groups;
        }

        public override List<Stage> GetTicketSeatsByEventId(int eventId, int sideId, int sectorId)
        {
            List<Stage> seats = new List<Stage>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetTicketSeatsByEventId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cmd.Parameters.Add("@SideId", SqlDbType.Int).Value = sideId;
                cmd.Parameters.Add("@SectorId", SqlDbType.Int).Value = sectorId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Stage seat = new Stage()
                    {
                        Id = (int)reader["TicketId"],
                        SideId = (int)reader["SideId"],
                        SectorId = (int)reader["SectorId"],
                        SeatId = (int)reader["SeatId"],
                        SideName = reader["SideName"].ToString(),
                        SectorName = reader["SectorName"].ToString(),
                        StatusId = (ItemStatus)reader["StatusId"],
                        Price = (decimal)reader["Price"],
                        RowNum = (int)reader["RowNum"],
                        SeatNum = reader["SeatNum"].ToString(),
                        XPos = (int)reader["XPos"],
                        YPos = (int)reader["YPos"]
                    };

                    seats.Add(seat);
                }
            }
            return seats;
        }

        public override List<Stage> GetSidesByStageId(int stageId)
        {
            List<Stage> stages = new List<Stage>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetSidesByStageId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StageId", SqlDbType.Int).Value = stageId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Stage stage = new Stage()
                    {
                        Id = (int)reader["Id"],
                        StageId = (int)reader["StageId"],
                        Name = reader["Name"].ToString()
                    };

                    stages.Add(stage);
                }
            }
            return stages;
        }

        public override List<Stage> GetSeatsByStageId(int stageId)
        {
            List<Stage> seats = new List<Stage>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetSeatsByStageId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StageId", SqlDbType.Int).Value = stageId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Stage seat = new Stage()
                    {
                        SideId = (int)reader["SideId"],
                        SectorId = (int)reader["SectorId"],
                        SeatId = (int)reader["SeatId"],
                        SideName = reader["SideName"].ToString(),
                        SectorName = reader["SectorName"].ToString(),
                        Price = (decimal)reader["Price"],
                        RowNum = (int)reader["RowNum"],
                        SeatNum = reader["SeatNum"].ToString()
                    };

                    seats.Add(seat);
                }
            }
            return seats;
        }

        public override List<Stage> GetSectorsBySideId(int sideId)
        {
            List<Stage> stages = new List<Stage>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetSectorsBySideId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SideId", SqlDbType.Int).Value = sideId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Stage stage = new Stage()
                    {
                        SectorId = (int)reader["Id"],
                        SideId = (int)reader["SideId"],
                        Name = reader["Name"].ToString()
                    };

                    stages.Add(stage);
                }
            }
            return stages;
        }

        public override List<Stage> GetSeatsByIds(string ids)
        {
            List<Stage> stages = new List<Stage>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetSeatsByIds", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Ids", SqlDbType.VarChar).Value = ids;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Stage stage = new Stage()
                    {
                        Id = (int)reader["Id"],
                        SideName = reader["SideName"].ToString(),
                        SectorName = reader["SectorName"].ToString(),
                        SectorId = (int)reader["SectorId"],
                        RowNum = (int)reader["RowNum"],
                        SeatNum = reader["SeatNum"].ToString(),
                        Price = (decimal)reader["Price"],
                        LeftPos = reader["LeftPos"].ToString(),
                        TopPos = reader["TopPos"].ToString()
                    };

                    stages.Add(stage);
                }
            }
            return stages;
        }

        public override List<Stage> GetRowsBySectorId(int sectorId)
        {
            List<Stage> stages = new List<Stage>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetRowsBySectorId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SectorId", SqlDbType.Int).Value = sectorId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Stage stage = new Stage()
                    {
                        SeatId = (int)reader["RowNum"],
                        Count = (int)reader["SeatCount"],
                        Price = (decimal)reader["PriceSum"],
                        AveragePrice = (decimal)reader["AveragePrice"]
                    };

                    stages.Add(stage);
                }
            }
            return stages;
        }

        public override List<Stage> GetSeatsByRowNum(int sectorId, int rowNum)
        {
            List<Stage> stages = new List<Stage>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetSeatsByRowId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SectorId", SqlDbType.Int).Value = sectorId;
                cmd.Parameters.Add("@RowNum", SqlDbType.Int).Value = rowNum;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Stage stage = new Stage()
                    {
                        SeatId = (int)reader["Id"],
                        SectorId = (int)reader["SectorId"],
                        RowNum = (int)reader["RowNum"],
                        SeatNum = reader["SeatNum"].ToString(),
                        Price = (decimal)reader["Price"],
                        XPos = (int)reader["XPos"],
                        YPos = (int)reader["YPos"]
                    };

                    stages.Add(stage);
                }
            }
            return stages;
        }

        public override List<Stage> GetSeatPointsBySectorId(int sectorId)
        {
            List<Stage> stages = new List<Stage>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GetSeatPointsBySectorId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SectorId", SqlDbType.Int).Value = sectorId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Stage stage = new Stage()
                    {
                        SeatId = (int)reader["Id"],
                        SectorId = (int)reader["SectorId"],
                        RowNum = (int)reader["RowNum"],
                        SeatNum = reader["SeatNum"].ToString(),
                        Price = (decimal)reader["Price"],
                        LeftPos = reader["LeftPos"].ToString(),
                        TopPos = reader["TopPos"].ToString()
                    };
                    stages.Add(stage);
                }

                reader.Close();
                cn.Close();
            }
            return stages;
        }

        public override List<Stage> GroupTicketSeatsByEventId(int eventId)
        {
            List<Stage> seats = new List<Stage>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Stages_GroupTicketSeatsByEventId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Stage seat = new Stage()
                    {
                        SideId = (int)reader["SideId"],
                        SideName = reader["SideName"].ToString(),
                        SectorId = (int)reader["SectorId"],
                        SectorName = reader["SectorName"].ToString(),
                        Price = (decimal)reader["Price"],
                        Count = (int)reader["Count"]
                    };
                    seats.Add(seat);
                }

                reader.Close();
                cn.Close();
            }
            return seats;
        }


        #endregion
    }
}