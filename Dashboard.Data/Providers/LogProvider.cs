﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Providers
{
    public class LogProvider : LogManager
    {
        public override List<Log> GetPagedLogs(int startRowIndex, int maximumRows, int memberId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Logs_GetPagedLogs", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cn.Open();
                return GetLogCollectionFromReader(ExecuteReader(cmd));
            }
        }
        
        public override int CountLogs(int memberId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Logs_CountLogs", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cn.Open();
                return (int) ExecuteScalar(cmd);
            }
        }

        public override DateTime GetUpdatedTime()
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT MAX(CreatedDate) FROM [ETICKET].[dbo].[Tracks]", cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                return (DateTime)ExecuteScalar(cmd);
            }
        }

        public override int InsertLog(Log log)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Logs_InsertLog", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("MemberId", SqlDbType.Int).Value = log.MemberId;
                if (log.TicketId > 0)
                {
                    cmd.Parameters.Add("TicketId", SqlDbType.Int).Value = log.TicketId;
                }
                cmd.Parameters.Add("LogText", SqlDbType.NVarChar).Value = log.LogText;
                cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = log.CreatedDate;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (int) cmd.Parameters["@Id"].Value;
            }
        }
    }
}