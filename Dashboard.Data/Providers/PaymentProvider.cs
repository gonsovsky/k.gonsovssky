﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dashboard.Common;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Providers
{
    public class PaymentProvider : PaymentManager
    {
        public override Payment GetPaymentById(int id)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Payments_GetPaymentById", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetPaymentFromReader(reader);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves Payments collection
        /// </summary>
        public override List<Payment> GetPagedPayments(int startRowIndex, int maximumRows)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Payments_GetPagedPayments", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cn.Open();
                return GetPaymentCollectionFromReader(ExecuteReader(cmd));
            }
        }
        /// <summary>
        /// Counts Payment items
        /// </summary>
        public override int CountPayments()
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Payments_CountPayments", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Inserts a new Payment
        /// </summary>
        public override int InsertPayment(Payment payment)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Payments_InsertReturnPayment", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = payment.MemberId;
                cmd.Parameters.Add("@OrderId", SqlDbType.Int).Value = payment.OrderId;
                cmd.Parameters.Add("@InvoiceId", SqlDbType.NVarChar).Value = payment.InvoiceId;
                cmd.Parameters.Add("@Amount", SqlDbType.Money).Value = payment.Amount;
                cmd.Parameters.Add("@Reason", SqlDbType.NVarChar).Value = payment.Reason;
                cmd.Parameters.Add("@RequestDate", SqlDbType.DateTime).Value = payment.RequestDate;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (int) cmd.Parameters["@Id"].Value;
            }
        }

        /// <summary>
        /// Updates a Payment
        /// </summary>
        public override bool UpdatePayment(Payment payment)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Payments_UpdateReturnPayment", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = payment.Id;
                cmd.Parameters.Add("@PaymentStatus", SqlDbType.NVarChar).Value = payment.PaymentStatus;
                cmd.Parameters.Add("@Error", SqlDbType.NVarChar).Value = payment.Error;
                cmd.Parameters.Add("@TechMessage", SqlDbType.NVarChar).Value = payment.TechMessage;
                cmd.Parameters.Add("@ResponseDate", SqlDbType.DateTime).Value = payment.ResponseDate;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

    }
}