﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Providers
{
    public class EventDateProvider : EventDateManager
    {
        /// <summary>
        /// Gets EventDates By ActionId
        /// </summary>
        public override List<EventDate> GetEventDatesByActionId(int actionId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("EventDates_GetEventDatesByActionId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cn.Open();
                return GetEventDateCollectionFromReader(ExecuteReader(cmd));
            }
        }

        public override EventDate GetEventDateById(int eventId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("EventDates_GetEventDateByEventId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetEventDateFromReader(reader);
                else
                    return null;
            }
        }

        /// <summary>
        /// Groups EventDates On Tickets 
        /// </summary>
        public override List<EventDate> GroupEventDatesOnTickets(int actionId, int statusId, int year)
        {
            List<EventDate> eventDates = new List<EventDate>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("EventDates_GroupEventDatesOnTickets", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    eventDates.Add(new EventDate()
                    {
                        Id = (int)reader["Id"],
                        ActionDate = (DateTime)reader["ActionDate"],
                        Count = (int)reader["Count"],
                        Price = (decimal)reader["Price"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return eventDates;
        }

        /// <summary>
        /// Retrieves EventDates collection
        /// </summary>
        public override List<EventDate> GetPagedEventDates(int startRowIndex, int maximumRows, int memberId, int providerId, int actionId, int year)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("EventDates_GetPagedEventDates", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cn.Open();
                return GetEventDateCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Counts EventDates by providerId, actionId, year
        /// </summary>
        public override int CountEventDates(int memberId, int providerId, int actionId, int year)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("EventDates_CountEventDates", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public override int CountEventDatesByActionId(int actionId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("EventDates_CountEventDatesByActionId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Groups EventDates on months, events count, total price for tickets 
        /// </summary>
        /// <param name="actionId">ShowAction Id or zero</param>
        /// <param name="statusId">StatusId Id or zero</param>
        /// <param name="year">Particular year - obligatory</param>
        /// <returns>List Group as result</returns>
        public override List<Group> GroupEventDatesOnMonths(int actionId, int statusId, int year)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("EventDates_GroupEventDatesOnMonths", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Group group = new Group()
                    {
                        ActionId = (int)reader["MonthId"],
                        Count = (int)reader["Count"],
                        TotalCount = (int)reader["TotalCount"],
                        Price = (decimal)reader["Price"],
                        AveragePrice = (decimal)reader["AveragePrice"]
                    };

                    group.Name = Group.GetMonthName(group.ActionId);
                    groups.Add(group);
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        /// <summary>
        /// Retrieves EventDates list with ticket count.
        /// Note, the result is dependant on member's provider list.
        /// Groups EventDates On Tickets By MemberId.
        /// </summary>
        /// <param name="actionId">Optional: use ActionId = 0 to retrieve all the EventDates</param>
        /// <param name="memberId">MemberId is obligatory</param>
        /// <param name="statusId">StatusId is obligatory</param>
        /// <param name="year">Year is obligatory</param>
        /// <param name="month">Month is obligatory</param>
        /// <returns>Returns grouped list of EventDateId, Name, Ticket count</returns>
        public override List<Group> GroupEventDatesOnTicketsByMemberId(int actionId, int memberId, int statusId, int year, int month)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("EventDates_GroupEventDatesOnTicketsByMemberId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = month;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    groups.Add(new Group()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString(),
                        StartDate = (DateTime)reader["ActionDate"],
                        TotalCount = (int)reader["TotalCount"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        /// <summary>
        /// Gets Available Months
        /// </summary>
        public override List<Group> GetAvailableMonths(int year)
        {
            List<Group> months = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("EventDates_GetAvailableMonths", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);

                while (reader.Read())
                {
                    int monthId = (int)reader["Month"];
                    months.Add(new Group(monthId, Group.GetMonthName(monthId)));
                }
                reader.Close();
                cn.Close();
            }
            return months;
        }
    }
}