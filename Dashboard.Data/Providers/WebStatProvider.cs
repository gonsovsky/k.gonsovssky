﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Providers
{
    public class WebStatProvider : WebStatManager
    {
        #region Sessions

        public override List<WebStat> GetSessionsByDeviceType(DeviceType deviceType)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetSessionsByDeviceType", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                if (deviceType != DeviceType.None)
                {
                    cmd.Parameters.Add("@DeviceType", SqlDbType.Int).Value = deviceType;
                }
                cn.Open();
                return GetSessionCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Gets Sessions by single date
        /// </summary>
        public override List<WebStat> GetSessionsByDate(DateTime date)
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetSessionsByDate", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StartDay", SqlDbType.DateTime).Value = date.Date;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    WebStat stat = new WebStat()
                    {
                        Id = (int) reader["Id"],
                        CreatedDate = (DateTime) reader["CreatedDate"],
                        IPAddress = reader["IPAddress"].ToString(),
                        UserAgent = reader["UserAgent"].ToString()
                    };

                    if (reader["BotId"] != DBNull.Value)
                    {
                        stat.BotId = (int) reader["BotId"];
                    }
                    stats.Add(stat);
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        /// <summary>
        /// Gets Sessions by month ahead
        /// </summary>
        public override List<WebStat> GetSessionsByMonth(int month, int year)
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetSessionsByMonth", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = month;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    WebStat stat = new WebStat()
                        {
                            Id = (int)reader["Id"],
                            CreatedDate = (DateTime)reader["CreatedDate"],
                            IPAddress = reader["IPAddress"].ToString(),
                            UserAgent = reader["UserAgent"].ToString()
                        };

                    if (reader["BotId"] != DBNull.Value)
                    {
                        stat.BotId = (int)reader["BotId"];
                    }
                    stats.Add(stat);
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        public override List<WebStat> GroupUserDevices()
        {
            List<WebStat> groups = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GroupUserDevices", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    WebStat group = new WebStat();
                    group.DeviceType = (DeviceType) reader["DeviceType"];
                    group.Count = (int)reader["Count"];
                    group.AveragePrice = (decimal)reader["Percentage"];
                    groups.Add(group);
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        public override List<WebStat> GetBots()
        {
            List<WebStat> groups = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetBots", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    WebStat group = new WebStat();
                    group.Id = (int)reader["Id"];
                    group.UserAgent = reader["UserAgent"].ToString();
                    groups.Add(group);
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        public override bool UpdateSession(WebStat stat)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_UpdateSession", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SessionId", SqlDbType.Int).Value = stat.SessionId;
                cmd.Parameters.Add("@DeviceType", SqlDbType.Int).Value = stat.DeviceType;
                cmd.Parameters.Add("@BotId", SqlDbType.Int).Value = stat.BotId;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        #endregion

        #region Requests

        public override List<WebStat> GetPagedSales(int startRowIndex, int maximumRows)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetPagedSales", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cn.Open();
                return GetRequestCollectionFromReader(ExecuteReader(cmd));
            }
        }

        public override int CountSales()
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_CountSales", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }


        /// <summary>
        /// Gets Requests By Month ahead
        /// </summary>
        public override List<WebStat> GetRequestsByMonth(int month, int year)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetRequestsByMonth", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = month;
                cn.Open();
                return GetRequestCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Gets Site By SiteId
        /// </summary>
        public override WebStat GetSiteBySiteId(int siteId)
        {
            WebStat stat = new WebStat();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetSiteBySiteId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = siteId;
                cn.Open();
                IDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                    stat = new WebStat()
                    {
                        SiteId = (int) reader["SiteId"],
                        UrlReferrer = reader["UrlReferrer"].ToString()
                    };

            }
            return stat;
        }

        /// <summary>
        /// Gets Requests By SessionId
        /// </summary>
        public override List<WebStat> GetRequestsBySessionId(int sessionId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetRequestsBySessionId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SessionId", SqlDbType.Int).Value = sessionId;
                cn.Open();
                return GetRequestCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Gets Requests By Date
        /// </summary>
        public override List<WebStat> GetRequestsByDate(DateTime date, int sessionId)
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetRequestsByDate", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                if (date != DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = date;
                }
                else
                {
                    cmd.Parameters.Add("@SessionId", SqlDbType.Int).Value = sessionId;
                }
                cn.Open();
                return GetRequestCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Counts Requests by date
        /// </summary>
        public override int CountRequestsByDate(DateTime date)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_CountRequestsByDate", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = date;
                cn.Open();
                return (int) cmd.ExecuteScalar();
            }
        }

        public override int CountSessionsByDate(DateTime date)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_CountSessionsByDate", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = date;
                cn.Open();
                return (int)cmd.ExecuteScalar();
            }
        }

        /// <summary>
        /// Gets Page By PageId
        /// </summary>
        public override WebStat GetPageByPageId(int pageId)
        {
            WebStat stat = new WebStat();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetPageByPageId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PageId", SqlDbType.Int).Value = pageId;
                cn.Open();
                IDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                    stat = new WebStat() {PageId = (int) reader["PageId"], PageUrl = reader["PageUrl"].ToString()};

            }
            return stat;
        }

        /// <summary>
        /// Counts Requests by SessionId
        /// </summary>
        public override int CountRequestsBySessionId(int sessionId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_CountRequestsBySessionId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SessionId", SqlDbType.Int).Value = sessionId;
                cn.Open();
                return (int) cmd.ExecuteScalar();
            }
        }

        /// <summary>
        /// Gets Available Years
        /// </summary>
        public override List<int> GetAvailableYears()
        {
            List<int> years = new List<int>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetAvailableYears", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    years.Add((int) reader["Year"]);
                }
                reader.Close();
                cn.Close();
            }
            return years;
        }

        /// <summary>
        /// Gets Available Months
        /// </summary>
        public override List<Group> GetAvailableMonths(int year)
        {
            List<Group> months = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetAvailableMonths", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int monthId = (int) reader["Month"];
                    months.Add(new Group(monthId, Group.GetMonthName(monthId)));
                }
                reader.Close();
                cn.Close();
            }
            return months;
        }

        /// <summary>
        /// Groups KeyWords by year
        /// </summary>
        public override List<WebStat> GroupKeyWordsByYear(int year)
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GroupKeyWordsByYear", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    stats.Add(new WebStat()
                    {
                        KeyWord = reader["KeyWord"].ToString(),
                        TotalCount = (int)reader["TotalCount"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        /// <summary>
        /// Groups By KeyWord
        /// </summary>
        public override List<WebStat> GroupByKeyWord()
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GroupByKeyWord", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    stats.Add(new WebStat() { KeyWord = reader["KeyWord"].ToString(), TotalCount = (int)reader["TotalCount"] });
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        /// <summary>
        /// Groups sessions by bot for year
        /// </summary>
        public override List<WebStat> GroupByBot(int year)
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GroupByBot", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    stats.Add(new WebStat()
                    {
                        BotId = (int) reader["BotId"],
                        BotName = reader["BotName"].ToString(),
                        TotalCount = (int)reader["TotalCount"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        /// <summary>
        /// Counts sessions by BotId for month
        /// </summary>
        public override int CountBotsByMonth(int botId, int month, int year)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_CountBotsByMonth", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@BotId", SqlDbType.Int).Value = botId;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = month;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                return (int) cmd.ExecuteScalar();
            }
        }

        /// <summary>
        /// Groups sessions by visitor' IP addresses for every single month for a year
        /// </summary>
        public override List<Group> GroupByVisitors(int year)
        {
            List<Group> months = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GroupByVisitors", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    months.Add(new Group((int) reader["MonthId"], Group.GetMonthName((int) reader["MonthId"]), (int) reader["Total"]));
                }

                reader.Close();
                cn.Close();
            }
            return months;
        }

        /// <summary>
        /// Groups total Sessions By Day
        /// </summary>
        public override List<WebStat> GroupSessionsByDay(int month, int year)
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GroupSessionsByDay", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = month;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    stats.Add(new WebStat() { Id = (int)reader["Id"], TotalCount = (int)reader["TotalCount"] });
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        /// <summary>
        /// Groups total Requests By Day
        /// </summary>
        public override List<WebStat> GroupRequestsByDay(int month, int year)
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GroupRequestsByDay", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = month;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    stats.Add(new WebStat() { Id = (int)reader["Id"], TotalCount = (int)reader["TotalCount"] });
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        /// <summary>
        /// Groups site members by month
        /// </summary>
        public override List<Group> GroupMembersByMonth(int year)
        {
            List<Group> stats = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GroupMembersByMonth", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    stats.Add(new Group((int) reader["MonthId"], Group.GetMonthName((int) reader[0]), (int) reader[1]));
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        /// <summary>
        /// Groups data By VisitorWebStatus
        /// </summary>
        public override List<WebStat> GroupByVisitorWebStatus(int year)
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GroupByVisitorWebStatus", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    stats.Add(new WebStat()
                    {
                        //WebStatText = reader[0].ToString(),
                        //TotalCount = (int)reader[1]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        /// <summary>
        /// Groups data By WaitTime
        /// </summary>
        public override List<WebStat> GroupByWaitTime(int year)
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GroupByWaitTime", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    stats.Add(new WebStat()
                    {
                        //WebStatText = reader[0].ToString(), 
                        //TotalCount = (int)reader[1]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        /// <summary>
        /// Groups data By VisitType
        /// </summary>
        public override List<WebStat> GroupByVisitType(int year)
        {
            List<WebStat> stats = new List<WebStat>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GroupByVisitType", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    stats.Add(new WebStat()
                    {
                        //WebStatText = reader[0].ToString(),
                        //TotalCount = (int)reader[1]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return stats;
        }

        /// <summary>
        /// Gets WebStat By WebStatId
        /// </summary>
        public override WebStat GetWebStatByWebStatId(int statId)
        {
            WebStat stat = new WebStat();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_GetWebStatByWebStatId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@WebStatId", SqlDbType.Int).Value = statId;
                cn.Open();
                IDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                    stat = new WebStat(
                        //(int)reader["WebStatId"],
                        //(int)reader["VisitYear"],
                        //(int)reader["VisitorAge"],
                        //(int)reader["Reason"],
                        //(int)reader["Gender"],
                        //(int)reader["Registry"],
                        //(int)reader["VisitorWebStatus"],
                        //(int)reader["WaitTime"],
                        //(int)reader["VisitType"],
                        //reader["VisitorComment"].ToString(),
                        //(DateTime)reader["CreatedDate"]
                        );

            }
            return stat;
        }

        /// <summary>
        /// Deletes KeyWord By KeyWord
        /// </summary>
        public override bool DeleteKeyWordByKeyWord(string keyWord)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_DeleteKeyWordByKeyWord", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@KeyWord", SqlDbType.NVarChar).Value = keyWord;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes KeyWord By KeyWordId
        /// </summary>
        public override bool DeleteKeyWordByKeyWordId(int keywordId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_DeleteKeyWordByKeyWordId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@KeywordId", SqlDbType.Int).Value = keywordId;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes WebStat By WebStatId
        /// </summary>
        public override bool DeleteWebStatByWebStatId(int statId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_DeleteWebStatByWebStatId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@WebStatId", SqlDbType.Int).Value = statId;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new WebStat
        /// </summary>
        public override int InsertWebStat(WebStat stat)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_InsertWebStat", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("@VisitYear", SqlDbType.Int).Value = stat.VisitYear;
                //cmd.Parameters.Add("@VisitorAge", SqlDbType.Int).Value = stat.VisitorAge;
                //cmd.Parameters.Add("@Reason", SqlDbType.Int).Value = stat.Reason;
                //cmd.Parameters.Add("@Gender", SqlDbType.Int).Value = stat.Gender;
                //cmd.Parameters.Add("@Registry", SqlDbType.Int).Value = stat.Registry;
                //cmd.Parameters.Add("@VisitorWebStatus", SqlDbType.Int).Value = stat.VisitorWebStatus;
                //cmd.Parameters.Add("@WaitTime", SqlDbType.Int).Value = stat.WaitTime;
                //cmd.Parameters.Add("@VisitType", SqlDbType.Int).Value = stat.VisitType;
                //cmd.Parameters.Add("@VisitorComment", SqlDbType.NVarChar).Value = stat.VisitorComment;
                //cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = stat.CreatedDate;
                cmd.Parameters.Add("@WebStatId", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (int) cmd.Parameters["@WebStatId"].Value;
            }
        }

        /// <summary>
        /// Updates WebStat
        /// </summary>
        public override bool UpdateWebStat(WebStat stat)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_UpdateWebStat", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("@WebStatId", SqlDbType.Int).Value = stat.WebStatId;
                //cmd.Parameters.Add("@VisitYear", SqlDbType.Int).Value = stat.VisitYear;
                //cmd.Parameters.Add("@VisitorAge", SqlDbType.Int).Value = stat.VisitorAge;
                //cmd.Parameters.Add("@Reason", SqlDbType.Int).Value = stat.Reason;
                //cmd.Parameters.Add("@Gender", SqlDbType.Int).Value = stat.Gender;
                //cmd.Parameters.Add("@Registry", SqlDbType.Int).Value = stat.Registry;
                //cmd.Parameters.Add("@VisitorWebStatus", SqlDbType.Int).Value = stat.VisitorWebStatus;
                //cmd.Parameters.Add("@WaitTime", SqlDbType.Int).Value = stat.WaitTime;
                //cmd.Parameters.Add("@VisitType", SqlDbType.Int).Value = stat.VisitType;
                //cmd.Parameters.Add("@VisitorComment", SqlDbType.NVarChar).Value = stat.VisitorComment;
                //cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = stat.CreatedDate;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        #endregion
    }
}