﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Providers
{
    public class OrderProvider : OrderManager
    {
        #region Orders

        public override Order GetOrderByOrderId(int orderId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_GetOrderByOrderId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderId", SqlDbType.Int).Value = orderId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetOrderFromReader(reader);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves Orders collection
        /// </summary>
        public override List<Order> GetPagedOrders(int startRowIndex, int maximumRows)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_GetPagedOrders", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cn.Open();
                return GetOrderCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Counts Order items
        /// </summary>
        public override int CountOrders()
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_CountOrders", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return (int) ExecuteScalar(cmd);
            }
        }
        
        /// <summary>
        /// Inserts a new Order
        /// </summary>
        public override int InsertOrder(Order order)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_InsertOrder", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("TicketId", SqlDbType.Int).Value = order.TicketId;
                cmd.Parameters.Add("MemberId", SqlDbType.Int).Value = order.MemberId;
                cmd.Parameters.Add("BarCode", SqlDbType.VarChar).Value = order.BarCode;
                cmd.Parameters.Add("ControlDigit", SqlDbType.Int).Value = order.ControlDigit;
                cmd.Parameters.Add("OrderId", SqlDbType.Int).Value = order.OrderId;
                cmd.Parameters.Add("SaleComission", SqlDbType.Money).Value = order.SaleComission;
                cmd.Parameters.Add("BookComission", SqlDbType.Money).Value = order.BookComission;
                cmd.Parameters.Add("ExtraCharge", SqlDbType.Money).Value = order.ExtraCharge;
                cmd.Parameters.Add("ComissionPercent", SqlDbType.Int).Value = order.ComissionPercent;
                cmd.Parameters.Add("ExtraChargePercent", SqlDbType.Int).Value = order.ExtraChargePercent;
                cmd.Parameters.Add("FreeIssue", SqlDbType.Bit).Value = order.FreeIssue;
                cmd.Parameters.Add("DiscountCardId", SqlDbType.Int).Value = order.DiscountCardId;
                cmd.Parameters.Add("DecreaseDeposit", SqlDbType.Money).Value = order.DecreaseDeposit;
                cmd.Parameters.Add("Comission", SqlDbType.Money).Value = order.Comission;
                cmd.Parameters.Add("SalerCommision", SqlDbType.Money).Value = order.SalerCommision;
                cmd.Parameters.Add("Price", SqlDbType.Money).Value = order.Price;
                cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = order.CreatedDate;
                cmd.Parameters.Add("AliasId", SqlDbType.BigInt).Value = order.AliasId;
                cmd.Parameters.Add("TrackId", SqlDbType.Int).Value = order.TrackId;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (int)cmd.Parameters["@Id"].Value;
            }
        }

        /// <summary>
        /// Updates an Order
        /// </summary>
        public override bool UpdateOrder(Order order)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_UpdateOrder", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("Id", SqlDbType.Int).Value = order.Id;
                cmd.Parameters.Add("TicketId", SqlDbType.Int).Value = order.TicketId;
                cmd.Parameters.Add("MemberId", SqlDbType.Int).Value = order.MemberId;
                cmd.Parameters.Add("BarCode", SqlDbType.VarChar).Value = order.BarCode;
                cmd.Parameters.Add("ControlDigit", SqlDbType.Int).Value = order.ControlDigit;
                cmd.Parameters.Add("OrderId", SqlDbType.Int).Value = order.OrderId;
                cmd.Parameters.Add("SaleComission", SqlDbType.Money).Value = order.SaleComission;
                cmd.Parameters.Add("BookComission", SqlDbType.Money).Value = order.BookComission;
                cmd.Parameters.Add("ExtraCharge", SqlDbType.Money).Value = order.ExtraCharge;
                cmd.Parameters.Add("ComissionPercent", SqlDbType.Int).Value = order.ComissionPercent;
                cmd.Parameters.Add("ExtraChargePercent", SqlDbType.Int).Value = order.ExtraChargePercent;
                cmd.Parameters.Add("FreeIssue", SqlDbType.Bit).Value = order.FreeIssue;
                cmd.Parameters.Add("DiscountCardId", SqlDbType.Int).Value = order.DiscountCardId;
                cmd.Parameters.Add("DecreaseDeposit", SqlDbType.Money).Value = order.DecreaseDeposit;
                cmd.Parameters.Add("Comission", SqlDbType.Money).Value = order.Comission;
                cmd.Parameters.Add("SalerCommision", SqlDbType.Money).Value = order.SalerCommision;
                cmd.Parameters.Add("Price", SqlDbType.Money).Value = order.Price;
                cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = order.CreatedDate;
                cmd.Parameters.Add("AliasId", SqlDbType.BigInt).Value = order.AliasId;
                cmd.Parameters.Add("TrackId", SqlDbType.Int).Value = order.TrackId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes Order with underlying OrderItems By OrderId 
        /// </summary>
        public override bool DeleteOrderByOrderId(int orderId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_DeleteOrderByOrderId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderId", SqlDbType.Int).Value = orderId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region OrderItems

        public override List<Order> GetPagedOrderItems(int startRowIndex, int maximumRows)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_GetPagedOrderItems", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cn.Open();
                return GetOrderItemCollectionFromReader(ExecuteReader(cmd));
            }
        }

        public override List<Order> GroupOrderItemsOnDay(int providerId, int year)
        {
            List<Order> orders = new List<Order>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_GroupOrderItemsOnDay", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Order order = new Order()
                    {
                        CreatedDate = (DateTime)reader["CreatedDate"],
                        Count = (int)reader["Count"],
                        Price = (decimal)reader["Price"]
                    };
                    orders.Add(order);
                }
            }
            return orders;
        }

        /// <summary>
        /// Retrieves Orders collection
        /// </summary>
        public override List<Order> GetOrderItemsByOrderId(int orderId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_GetOrderItemsByOrderId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderId", SqlDbType.Int).Value = orderId;
                cn.Open();
                return GetOrderItemCollectionFromReader(ExecuteReader(cmd));
            }
        }

        public override Order GetOrderItemByTicketId(int ticketId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_GetOrderItemByTicketId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TicketId", SqlDbType.Int).Value = ticketId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetOrderItemFromReader(reader);
                else
                    return null;
            }
        }

        /// <summary>
        /// Counts Order items
        /// </summary>
        public override int CountOrderItems(int orderId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_CountOrderItems", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderId", SqlDbType.Int).Value = orderId;
                cn.Open();
                return (int) ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Inserts a new OrderItem
        /// </summary>
        public override int InsertOrderItem(Order order)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_InsertOrderItem", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("TicketId", SqlDbType.Int).Value = order.TicketId;
                cmd.Parameters.Add("MemberId", SqlDbType.Int).Value = order.MemberId;
                cmd.Parameters.Add("BarCode", SqlDbType.VarChar).Value = order.BarCode;
                cmd.Parameters.Add("ControlDigit", SqlDbType.Int).Value = order.ControlDigit;
                cmd.Parameters.Add("OrderId", SqlDbType.Int).Value = order.OrderId;
                cmd.Parameters.Add("SaleComission", SqlDbType.Money).Value = order.SaleComission;
                cmd.Parameters.Add("BookComission", SqlDbType.Money).Value = order.BookComission;
                cmd.Parameters.Add("ExtraCharge", SqlDbType.Money).Value = order.ExtraCharge;
                cmd.Parameters.Add("ComissionPercent", SqlDbType.Int).Value = order.ComissionPercent;
                cmd.Parameters.Add("ExtraChargePercent", SqlDbType.Int).Value = order.ExtraChargePercent;
                cmd.Parameters.Add("FreeIssue", SqlDbType.Bit).Value = order.FreeIssue;
                cmd.Parameters.Add("DiscountCardId", SqlDbType.Int).Value = order.DiscountCardId;
                cmd.Parameters.Add("DecreaseDeposit", SqlDbType.Money).Value = order.DecreaseDeposit;
                cmd.Parameters.Add("Comission", SqlDbType.Money).Value = order.Comission;
                cmd.Parameters.Add("SalerCommision", SqlDbType.Money).Value = order.SalerCommision;
                cmd.Parameters.Add("Price", SqlDbType.Money).Value = order.Price;
                cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = order.CreatedDate;
                cmd.Parameters.Add("AliasId", SqlDbType.BigInt).Value = order.AliasId;
                cmd.Parameters.Add("TrackId", SqlDbType.Int).Value = order.TrackId;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (int)cmd.Parameters["@Id"].Value;
            }
        }

        /// <summary>
        /// Updates an OrderItem
        /// </summary>
        public override bool UpdateOrderItem(Order order)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_UpdateOrderItem", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("Id", SqlDbType.Int).Value = order.Id;
                cmd.Parameters.Add("TicketId", SqlDbType.Int).Value = order.TicketId;
                cmd.Parameters.Add("MemberId", SqlDbType.Int).Value = order.MemberId;
                cmd.Parameters.Add("BarCode", SqlDbType.VarChar).Value = order.BarCode;
                cmd.Parameters.Add("ControlDigit", SqlDbType.Int).Value = order.ControlDigit;
                cmd.Parameters.Add("OrderId", SqlDbType.Int).Value = order.OrderId;
                cmd.Parameters.Add("SaleComission", SqlDbType.Money).Value = order.SaleComission;
                cmd.Parameters.Add("BookComission", SqlDbType.Money).Value = order.BookComission;
                cmd.Parameters.Add("ExtraCharge", SqlDbType.Money).Value = order.ExtraCharge;
                cmd.Parameters.Add("ComissionPercent", SqlDbType.Int).Value = order.ComissionPercent;
                cmd.Parameters.Add("ExtraChargePercent", SqlDbType.Int).Value = order.ExtraChargePercent;
                cmd.Parameters.Add("FreeIssue", SqlDbType.Bit).Value = order.FreeIssue;
                cmd.Parameters.Add("DiscountCardId", SqlDbType.Int).Value = order.DiscountCardId;
                cmd.Parameters.Add("DecreaseDeposit", SqlDbType.Money).Value = order.DecreaseDeposit;
                cmd.Parameters.Add("Comission", SqlDbType.Money).Value = order.Comission;
                cmd.Parameters.Add("SalerCommision", SqlDbType.Money).Value = order.SalerCommision;
                cmd.Parameters.Add("Price", SqlDbType.Money).Value = order.Price;
                cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = order.CreatedDate;
                cmd.Parameters.Add("AliasId", SqlDbType.BigInt).Value = order.AliasId;
                cmd.Parameters.Add("TrackId", SqlDbType.Int).Value = order.TrackId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes OrderItems By TicketId
        /// </summary>
        public override bool DeleteOrderItemsByTicketId(int ticketId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Orders_DeleteOrderItemsByTicketId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TicketId", SqlDbType.Int).Value = ticketId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion
    }
}