﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Providers
{
    public class ShowActionProvider : ShowActionManager
    {
        public override List<Group> GroupShowActionsOnEventsByDate(DateTime date)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_GroupShowActionsOnEventsByDate", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ActionDate", SqlDbType.DateTime).Value = date;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    groups.Add(new Group()
                    {
                        Id = (int)reader["ShowActionId"],
                        Name = reader["Name"].ToString(),
                        Description = reader["Description"].ToString(),
                        ActionId = (int)reader["EventDateId"],
                        StartDate = (DateTime)reader["ActionDate"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        public override ShowAction GetShowActionByActionId(int actionId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_GetShowActionByActionId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetShowActionFromReader(reader);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves ShowActions collection
        /// </summary>
        public override List<ShowAction> GetPagedShowActions(int startRowIndex, int maximumRows, int memberId, int providerId, int year)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_GetPagedShowActions", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cn.Open();
                return GetShowActionCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Gets ShowActions By ProviderId
        /// </summary>
        public override List<ShowAction> GetShowActionsByProviderId(int providerId, int year)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_GetShowActionsByProviderId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                return GetShowActionCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Groups ShowActions On Tickets 
        /// </summary>
        public override List<ShowAction> GroupShowActionsOnTickets(int providerId, int statusId, int year)
        {
            List<ShowAction> eventDates = new List<ShowAction>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_GroupShowActionsOnTickets", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    eventDates.Add(new ShowAction()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString(),
                        StartDate = (DateTime)reader["StartDate"],
                        EndDate = (DateTime)reader["EndDate"],
                        Count = (int)reader["Count"],
                        Price = (decimal)reader["Price"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return eventDates;
        }

        public override List<Group> GroupShowActionsOnEvents(int providerId, int statusId, int year)
        {
            List<Group> eventDates = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_GroupShowActionsOnEvents", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    eventDates.Add(new Group()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Description = reader["Description"].ToString(),
                        StartDate = (DateTime)reader["ActionDate"],
                        Count = (int)reader["Count"],
                        Price = (decimal)reader["Price"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return eventDates;
        }

        /// <summary>
        /// Groups ShowActions On Price 
        /// </summary>
        public override List<Group> GroupShowActionsOnPrice(int statusId, int year)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_GroupShowActionsOnPrice", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    groups.Add(new Group()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Count = (int)reader["Count"],
                        Price = (decimal)reader["Price"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        /// <summary>
        /// Retrieves ShowActions list with event count and ticket count.
        /// Note, the result is dependant on member's provider list.
        /// Groups ShowActions On Events By MemberId.
        /// </summary>
        /// <param name="providerId">Optional: use ProviderId = 0 to retrieve all the ShowActions</param>
        /// <param name="memberId">MemberId is obligatory</param>
        /// <param name="statusId">StatusId is obligatory</param>
        /// <param name="year">Year is obligatory</param>
        /// <param name="month">Month is obligatory</param>
        /// <returns>Returns grouped list of ShowActionId, Name, Events count, Ticket count</returns>
        public override List<Group> GroupShowActionsOnEventsByMemberId(int providerId, int memberId, int statusId, int year, int month)
        {
            List<Group> groups = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_GroupShowActionsOnEventsByMemberId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = month;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    groups.Add(new Group()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"].ToString(),
                        Count = (int)reader["Count"],
                        TotalCount = (int)reader["TotalCount"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return groups;
        }

        /// <summary>
        /// Counts ShowAction items
        /// </summary>
        public override int CountShowActions(int memberId, int providerId, int year)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_CountShowActions", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public override List<Group> GroupShowActionsOnMonthsByYear(int year)
        {
            List<Group> months = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_GroupShowActionsOnMonthsByYear", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    months.Add(new Group((int)reader["MonthId"], Group.GetMonthName((int)reader["MonthId"]), (int)reader["Total"]));
                }

                reader.Close();
                cn.Close();
            }
            return months;
        }

        /// <summary>
        /// Gets Available Years
        /// </summary>
        public override List<int> GetAvailableYears()
        {
            List<int> years = new List<int>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("ShowActions_GetAvailableYears", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);

                while (reader.Read())
                {
                    years.Add((int)reader["Year"]);
                }
                reader.Close();
                cn.Close();
            }
            return years;
        }
    }
}