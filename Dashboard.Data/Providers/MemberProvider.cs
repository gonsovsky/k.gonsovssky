﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Managers;

namespace Dashboard.Data.Providers
{
    public class MemberProvider : MemberManager
    {
        #region Roles
        
        public override List<Member> GetRoles()
        {
            List<Member> roles = new List<Member>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GetRoles", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    Member role = new Member()
                    {
                        Id = (int)reader["Id"],
                        Name = reader["RoleName"].ToString(),
                        Description = reader["Description"].ToString()
                    };
                    roles.Add(role);
                }

                reader.Close();
                cn.Close();
            }
            return roles;
        }

        public override Member GetRoleByName(string roleName)
        {
            Member role = new Member();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GetRoleByName", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = roleName;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    role.Id = (int) reader["Id"];
                    role.Name = reader["RoleName"].ToString();
                    role.Description = reader["Description"].ToString();
                }
                reader.Close();
                cn.Close();
            }
            return role;
        }

        public override Member GetRoleById(int roleId)
        {
            Member role = null;
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GetRoleById", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@RoleId", SqlDbType.NVarChar).Value = roleId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    role = new Member
                    {
                        Id = (int) reader["Id"],
                        Name = reader["RoleName"].ToString(),
                        Description = reader["Description"].ToString()
                    };
                }
                reader.Close();
                cn.Close();
            }
            return role;
        }

        #endregion

        #region Members

        public override Member GetMemberByMemberId(int memberId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GetMemberByMemberId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMemberFromReader(reader);
                else
                    return null;
            }
        }

        public override Member GetParentMemberByMemberId(int memberId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GetParentMemberByMemberId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMemberFromReader(reader);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves an ASP Member by login.
        /// ASP Member must be IsApproved and not IsLockedOut
        /// Used for remote authentication
        /// </summary>
        /// <param name="login">Member's Login, UserName, or Email </param>
        /// <returns>Member object</returns>
        public override Member GetMemberByLogin(string login)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GetMemberByLogin", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Login", SqlDbType.NVarChar).Value = login;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMemberFromReader(reader);
                else
                    return null;
            }
        }

        /// <summary>
        /// Groups Members On Tickets 
        /// </summary>
        public override List<Member> GroupMembersOnTickets(int providerId, int actionId, int eventId, int statusId)
        {
            List<Member> members = new List<Member>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GroupMembersOnTickets", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = providerId;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = actionId;
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = eventId;
                cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = statusId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    members.Add(new Member()
                    {
                        Id = (int)reader["Id"],
                        Email = reader["Email"].ToString(),
                        Count = (int)reader["Count"],
                        Price = (decimal)reader["Price"]
                    });
                }

                reader.Close();
                cn.Close();
            }
            return members;
        }

        /// <summary>
        /// Get paged member list
        /// </summary>
        /// <param name="startRowIndex">Page number</param>
        /// <param name="maximumRows">Page rows</param>
        /// <param name="roleId">RoleId or zero if all roles selected</param>
        /// <param name="parentId">ParentId or zero if all users selected</param>
        /// <returns>Member List</returns>
        public override List<Member> GetPagedMembers(int startRowIndex, int maximumRows, int roleId, int parentId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GetPagedMembers", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = startRowIndex;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@RoleId", SqlDbType.Int).Value = roleId;
                cmd.Parameters.Add("@ParentId", SqlDbType.Int).Value = parentId;
                cn.Open();
                return GetMemberCollectionFromReader(ExecuteReader(cmd));
            }
        }

        public override List<Member> GetChildMembers(int parentId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GetChildMembers", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ParentId", SqlDbType.Int).Value = parentId;
                cn.Open();
                return GetMemberCollectionFromReader(ExecuteReader(cmd));
            }
        }

        public override int CountChildMembers(int parentId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_CountChildMembers", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ParentId", SqlDbType.Int).Value = parentId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Counts Members by roleId or/and parentId
        /// </summary>
        public override int CountMembers(int roleId, int parentId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_CountMembers", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@RoleId", SqlDbType.Int).Value = roleId;
                cmd.Parameters.Add("@ParentId", SqlDbType.Int).Value = parentId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public override List<Group> GroupMembersOnMonthsByYear(int year)
        {
            List<Group> months = new List<Group>();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GroupMembersOnMonthsByYear", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = year;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    months.Add(new Group((int)reader["MonthId"], Group.GetMonthName((int)reader["MonthId"]), (int)reader["Total"]));
                }

                reader.Close();
                cn.Close();
            }
            return months;
        }
        
        public override int InsertMember(Member member)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_InsertMember", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ParentId", SqlDbType.Int).Value = member.ParentId;
                cmd.Parameters.Add("@RoleId", SqlDbType.Int).Value = member.RoleId;
                cmd.Parameters.Add("@Pass", SqlDbType.VarChar).Value = member.Pass;
                cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = member.FirstName;
                cmd.Parameters.Add("@MiddleName", SqlDbType.NVarChar).Value = member.MiddleName;
                cmd.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = member.LastName;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = member.Phone;
                cmd.Parameters.Add("@About", SqlDbType.VarChar).Value = member.About;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = member.Email;
                cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = member.CreatedDate;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (int)cmd.Parameters["@Id"].Value;
            }
        }

        public override bool UpdateMember(Member member)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_UpdateMember", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = member.Id;
                cmd.Parameters.Add("@ParentId", SqlDbType.Int).Value = member.ParentId;
                cmd.Parameters.Add("@RoleId", SqlDbType.Int).Value = member.RoleId;
                cmd.Parameters.Add("@Pass", SqlDbType.VarChar).Value = member.Pass;
                cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = member.FirstName;
                cmd.Parameters.Add("@MiddleName", SqlDbType.NVarChar).Value = member.MiddleName;
                cmd.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = member.LastName;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = member.Phone;
                cmd.Parameters.Add("@About", SqlDbType.VarChar).Value = member.About;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = member.Email;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public override bool DeleteMemberByMemberId(int memberId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_DeleteMemberByMemberId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region Member Providers

        public override List<Member> GetMemberProviders(int memberId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GetMemberProviders", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cn.Open();
                return GetMemberProviderCollectionFromReader(ExecuteReader(cmd));
            }
        }

        public override Member GetMemberProviderByMemberId(int memberId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_GetMemberProviderByMemberId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMemberProviderFromReader(reader);
                else
                    return null;
            }
        }

        public override int CountMemberProviders(int memberId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_CountMemberProviders", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = memberId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public override int InsertMemberProvider(Member member)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_InsertMemberProvider", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MemberId", SqlDbType.Int).Value = member.Id;
                cmd.Parameters.Add("@ProviderId", SqlDbType.Int).Value = member.ProviderId;
                if (member.StageId > 0)
                {
                    cmd.Parameters.Add("@StageId", SqlDbType.Int).Value = member.StageId;
                }
                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (int)cmd.Parameters["@Id"].Value;
            }
        }

        public override bool DeleteMemberProviderById(int id)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Members_DeleteMemberProviderById", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion
    }
}