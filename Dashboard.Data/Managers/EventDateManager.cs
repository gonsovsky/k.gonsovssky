﻿using System;
using System.Collections.Generic;
using System.Data;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;
using Dashboard.Data.Providers;

namespace Dashboard.Data.Managers
{
    public abstract class EventDateManager : DataAccess
    {
        private static EventDateManager _instance;

        public static EventDateManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new EventDateProvider();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Groups EventDates On Tickets 
        /// </summary>
        public abstract List<EventDate> GroupEventDatesOnTickets(int actionId, int statusId, int year);
        public abstract List<EventDate> GetPagedEventDates(int startRowIndex, int maximumRows, int memberId, int providerId, int actionId, int year);
        /// <summary>
        /// Counts EventDates by providerId, actionId, year
        /// </summary>
        public abstract int CountEventDates(int memberId, int providerId, int actionId, int year);

        public abstract int CountEventDatesByActionId(int actionId);
        public abstract EventDate GetEventDateById(int eventId);
        public abstract List<Group> GetAvailableMonths(int year);
        /// <summary>
        /// Retrieves EventDates list with ticket count.
        /// Note, the result is dependant on member's provider list.
        /// Groups EventDates On Tickets By MemberId.
        /// </summary>
        /// <param name="actionId">Optional: use ActionId = 0 to retrieve all the EventDates</param>
        /// <param name="memberId">MemberId is obligatory</param>
        /// <param name="statusId">StatusId is obligatory</param>
        /// <param name="year">Year is obligatory</param>
        /// <param name="month">Month is obligatory</param>
        /// <returns>Returns grouped list of EventDateId, Name, Ticket count</returns>
        public abstract List<Group> GroupEventDatesOnTicketsByMemberId(int actionId, int memberId, int statusId, int year, int month);

        /// <summary>
        /// Groups EventDates on months, events count, total price for tickets 
        /// </summary>
        /// <param name="actionId">ShowAction Id or zero</param>
        /// <param name="statusId">StatusId Id or zero</param>
        /// <param name="year">Particular year - obligatory</param>
        /// <returns>List Group as result</returns>
        public abstract List<Group> GroupEventDatesOnMonths(int actionId, int statusId, int year);

        /// <summary>
        /// Gets EventDates By ActionId
        /// </summary>
        public abstract List<EventDate> GetEventDatesByActionId(int actionId);

        protected virtual EventDate GetEventDateFromReader(IDataReader reader)
        {
            EventDate eventDate = new EventDate()
            {
                Id = (int)reader["Id"],
                ActionId = (int)reader["ActionId"],
                ActionDate = (DateTime)reader["ActionDate"],
                StageId = (int)reader["StageId"],
                EventInfo = reader["EventInfo"].ToString(),
                StatusId = (ItemStatus)reader["StatusId"],
                CreatedDate = (DateTime)reader["CreatedDate"]
            };

            if (reader["EndSaleDate"] != DBNull.Value)
            {
                eventDate.EndDate = (DateTime)reader["EndSaleDate"];
            }
            if (reader["EndESaleDate"] != DBNull.Value)
            {
                eventDate.EndESaleDate = (DateTime)reader["EndESaleDate"];
            }

            if (reader["AliasId"] != DBNull.Value)
            {
                eventDate.AliasId = Convert.ToInt64(reader["AliasId"]);
            }

            if (reader["TrackId"] != DBNull.Value)
            {
                eventDate.TrackId = (int)reader["TrackId"];
            }

            return eventDate;
        }

        /// <summary>
        /// Returns a collection of EventDate objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<EventDate> GetEventDateCollectionFromReader(IDataReader reader)
        {
            List<EventDate> m = new List<EventDate>();
            while (reader.Read())
                m.Add(GetEventDateFromReader(reader));
            return m;
        }

    }
}