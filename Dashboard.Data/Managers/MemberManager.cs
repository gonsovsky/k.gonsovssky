﻿using System;
using System.Collections.Generic;
using System.Data;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;
using Dashboard.Data.Providers;

namespace Dashboard.Data.Managers
{
    public abstract class MemberManager : DataAccess
    {
        private static MemberManager _instance;

        public static MemberManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MemberProvider();
                }
                return _instance;
            }
        }

        #region Roles

        public abstract List<Member> GetRoles();
        public abstract Member GetRoleByName(string roleName);
        public abstract Member GetRoleById(int roleId);

        #endregion

        #region Members

        /// <summary>
        /// Groups Members On Tickets 
        /// </summary>
        public abstract List<Member> GroupMembersOnTickets(int providerId, int actionId, int eventId, int statusId);

        /// <summary>
        /// Get paged member list
        /// </summary>
        /// <param name="startRowIndex">Page number</param>
        /// <param name="maximumRows">Page rows</param>
        /// <param name="roleId">RoleId or zero if all roles selected</param>
        /// <param name="parentId">ParentId or zero if all users selected</param>
        /// <returns>Member List</returns>
        public abstract List<Member> GetPagedMembers(int startRowIndex, int maximumRows, int roleId, int parentId);
        public abstract List<Member> GetChildMembers(int parentId);
        public abstract int CountChildMembers(int parentId);
        /// <summary>
        /// Counts Members by roleId or/and parentId
        /// </summary>
        public abstract int CountMembers(int roleId, int parentId);
        public abstract Member GetMemberByMemberId(int memberId);
        public abstract Member GetParentMemberByMemberId(int memberId);
        public abstract bool UpdateMember(Member member);
        public abstract int InsertMember(Member member);

        /// <summary>
        /// Retrieves an ASP Member by login.
        /// ASP Member must be IsApproved and not IsLockedOut
        /// Used for remote authentication
        /// </summary>
        /// <param name="login">Member's Login, UserName, or Email </param>
        /// <returns>Member object</returns>
        public abstract Member GetMemberByLogin(string login);

        public abstract List<Group> GroupMembersOnMonthsByYear(int year);

        protected virtual Member GetMemberFromReader(IDataReader reader)
        {
            Member member = new Member()
            {
                Id = (int) reader["Id"],
                ParentId = (int)reader["ParentId"],
                RoleId = (int)reader["RoleId"],
                Pass = reader["Pass"].ToString(),
                FirstName = reader["FirstName"].ToString(),
                MiddleName = reader["MiddleName"].ToString(),
                LastName = reader["LastName"].ToString(),
                Phone = reader["Phone"].ToString(),
                About = reader["About"].ToString(),
                Email = reader["Email"].ToString(),
                CreatedDate = (DateTime)reader["CreatedDate"]
            };
            
            return member;
        }

        /// <summary>
        /// Returns a collection of Member objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Member> GetMemberCollectionFromReader(IDataReader reader)
        {
            List<Member> m = new List<Member>();
            while (reader.Read())
                m.Add(GetMemberFromReader(reader));
            return m;
        }

        #endregion

        #region Member Providers

        public abstract List<Member> GetMemberProviders(int memberId);
        public abstract Member GetMemberProviderByMemberId(int memberId);
        public abstract int CountMemberProviders(int memberId);
        public abstract int InsertMemberProvider(Member member);
        public abstract bool DeleteMemberProviderById(int id);
        public abstract bool DeleteMemberByMemberId(int memberId);

        protected virtual Member GetMemberProviderFromReader(IDataReader reader)
        {
            Member member = new Member()
            {
                Id = (int)reader["Id"],
                ProviderId = (int)reader["ProviderId"]
            };

            if (reader["StageId"] != DBNull.Value)
            {
                member.StageId = (int)reader["StageId"];
            }

            return member;
        }

        /// <summary>
        /// Returns a collection of Member objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Member> GetMemberProviderCollectionFromReader(IDataReader reader)
        {
            List<Member> m = new List<Member>();
            while (reader.Read())
                m.Add(GetMemberProviderFromReader(reader));
            return m;
        }


        #endregion
    }
}