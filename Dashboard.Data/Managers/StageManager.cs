﻿using System;
using System.Collections.Generic;
using System.Data;
using Dashboard.Common;
using Dashboard.Data.Data;
using Dashboard.Data.Providers;

namespace Dashboard.Data.Managers
{
    public abstract class StageManager : DataAccess
    {
        private static StageManager _instance;

        public static StageManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StageProvider();
                }
                return _instance;
            }
        }

        #region City

        public abstract List<City> GetCities();


        protected virtual City GetCityFromReader(IDataReader reader)
        {
            City ticket = new City()
            {
                Id = (int)reader["Id"],
                Name = reader["Name"].ToString(),
                Sort = (int)reader["Sort"],
                Visible = (int)reader["Visible"]
            };

            if (reader["ParentId"] != DBNull.Value)
            {
                ticket.ParentId = (int)reader["ParentId"];
            }
            return ticket;
        }

        /// <summary>
        /// Returns a collection of City objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<City> GetCityCollectionFromReader(IDataReader reader)
        {
            List<City> m = new List<City>();
            while (reader.Read())
                m.Add(GetCityFromReader(reader));
            return m;
        }

        #endregion

        #region Providers

        public abstract List<Provider> GetProvidersByMemberId(int memberId);

        public abstract Provider GetProviderByProviderId(int providerId);

        /// <summary>
        /// Gets providers which do not enlisted in member's provider list
        /// </summary>
        public abstract List<Provider> GetProviders(int memberId);

        /// <summary>
        /// Retrieves Providers list with show action count and event count.
        /// Note, the result is dependant on member's provider list.
        /// Groups Providers On Events By MemberId.
        /// </summary>
        /// <param name="memberId">MemberId is obligatory</param>
        /// <param name="year">Year is obligatory</param>
        /// <param name="month">Month is obligatory</param>
        /// <returns>Returns grouped list of Providers, Name, ShowActions count, Events count</returns>
        public abstract List<Group> GroupProvidersOnEventsByMemberId(int memberId, int year, int month);

        /// <summary>
        /// Groups Providers on Tickets by status and year
        /// </summary>
        public abstract List<Provider> GroupProvidersOnTickets(int statusId, int year, int month);

        /// <summary>
        /// Groups Providers on Months by tickets 
        /// </summary>
        public abstract List<Group> GroupProviderOnMonths(int providerId, int statusId, int year);

        /// <summary>
        /// Retrieves Providers collection
        /// </summary>
        public abstract List<Provider> GetPagedProviders(int startRowIndex, int maximumRows, int memberId, int cityId);

        /// <summary>
        /// Groups Providers on ShowActions count, EventDates count, Tickets count, and AveragePrice of a ticket within particular events
        /// </summary>
        /// <param name="providerId">Provider</param>
        /// <param name="statusId">Status of the ticket</param>
        /// <param name="year">Year</param>
        /// <returns>Returns ProviderId, ProviderName, ShowActions count, EventDates count, Tickets count</returns>
        public abstract List<Group> GroupProvidersOnShowActions(int providerId, int statusId, int year);

        /// <summary>
        /// Counts Provider items
        /// </summary>
        public abstract int CountProviders(int memberId, int cityId);

        

        /// <summary>
        /// Deletes Provider By ProviderId
        /// </summary>
        public abstract bool DeleteProviderByProviderId(int providerId);
        
        protected virtual Provider GetProviderFromReader(IDataReader reader)
        {
            Provider provider = new Provider()
            {
                Id = (int)reader["Id"],
                Name = reader["Name"].ToString(),
                Description = reader["FullName"].ToString(),
                Address = reader["Address"].ToString(),
                CityId = (int)reader["CityId"],
                StatusId = (ItemStatus)reader["StatusId"],
                CreatedDate = (DateTime)reader["CreatedDate"]
            };
            return provider;
        }

        /// <summary>
        /// Returns a collection of Provider objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Provider> GetProviderCollectionFromReader(IDataReader reader)
        {
            List<Provider> m = new List<Provider>();
            while (reader.Read())
                m.Add(GetProviderFromReader(reader));
            return m;
        }

        #endregion

        #region Stages

        public abstract Stage GetStageByStageId(int sideId);
        public abstract Stage GetSideBySideId(int sideId);
        public abstract Stage GetSectorBySectorId(int sectorId);
        public abstract Stage GetSeatBySeatId(int seatId);
        public abstract List<Stage> GetPagedStages(int startRowIndex, int maximumRows, int memberId, int providerId);
        public abstract int CountStages(int memberId, int providerId);
        public abstract int CountSidesByStageId(int stageId);
        public abstract int CountSectorsBySideId(int sideId);
        public abstract int CountSectorsByStageId(int sideId);
        public abstract int CountSeatsBySideId(int sideId);
        public abstract int CountSeatsByStageId(int stageId);
        public abstract int CountSeatsBySectorId(int sectorId);
        public abstract List<Stage> GetSidesByStageId(int stageId);
        public abstract List<Stage> GetSeatsByStageId(int stageId);
        public abstract List<Stage> GetSectorsBySideId(int sideId);
        public abstract List<Stage> GetRowsBySectorId(int sectorId);
        public abstract List<Stage> GetSeatsByRowNum(int sectorId, int rowNum);
        public abstract List<Stage> GetSeatPointsBySectorId(int sectorId);
        public abstract List<Stage> GroupTicketSeatsByEventId(int eventId);
        
        /// <summary>
        /// Groups Stages on Sides, Sectors, and Seats
        /// The result is dependent on parameter values
        /// </summary>
        /// <param name="providerId">Optional: use ProviderId = 0 to retrieve all the stages</param>
        /// <param name="stageId">Optional: use StageId = 0 to retrieve all the stages</param>
        /// <returns>List Group</returns>
        public abstract List<Group> GroupStagesOnSidesSectorsSeat(int providerId, int stageId);

        /// <summary>
        /// Groups StageSides On Sectors and Seats
        /// </summary>
        /// <param name="stageId">StageId</param>
        /// <returns>List Group</returns>
        public abstract List<Stage> GetStageSidesByStageId(int stageId);

        /// <summary>
        /// Groups StageSectors On Seats
        /// </summary>
        /// <param name="sideId">SideId</param>
        /// <returns>List Group</returns>
        public abstract List<Group> GroupStageSectorsOnSeats(int sideId);

        public abstract List<Stage> GetSeatsByIds(string ids);

        public abstract List<Group> GroupSeatsOnPrice(int stageId);
        public abstract List<Group> GroupTicketSeatsOnPrice(int eventId);
        public abstract List<Stage> GetTicketSeatsByEventId(int eventId, int sideId, int sectorId);
        
        protected virtual Stage GetStageFromReader(IDataReader reader)
        {
            Stage stage = new Stage()
            {
                StageId = (int)reader["Id"],
                ProviderId = (int)reader["ProviderId"],
                Name = reader["Name"].ToString().Trim(),
                Address = reader["Address"].ToString().Trim(),
                ImageMap = reader["ImageMap"].ToString().Trim(),
                MapWidth = (int)reader["MapWidth"],
                MapHeight = (int)reader["MapHeight"],
                Latitude = Convert.ToDouble(reader["Latitude"]),
                Longitude = Convert.ToDouble(reader["Longitude"])
            };
            return stage;
        }

        /// <summary>
        /// Returns a collection of Stage objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Stage> GetStageCollectionFromReader(IDataReader reader)
        {
            List<Stage> stages = new List<Stage>();
            while (reader.Read())
                stages.Add(GetStageFromReader(reader));
            return stages;
        }

        #endregion

    }
}
