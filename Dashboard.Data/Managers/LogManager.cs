﻿using System;
using System.Collections.Generic;
using System.Data;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;
using Dashboard.Data.Providers;

namespace Dashboard.Data.Managers
{
    public abstract class LogManager : DataAccess
    {
        private static LogManager _instance;

        public static LogManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LogProvider();
                }
                return _instance;
            }
        }

        public abstract List<Log> GetPagedLogs(int startRowIndex, int maximumRows, int memberId);
        public abstract int CountLogs(int memberId);
        public abstract int InsertLog(Log log);
        public abstract DateTime GetUpdatedTime();
        protected virtual Log GetLogFromReader(IDataReader reader)
        {
            Log log = new Log()
            {
                Id = (int) reader["Id"],
                MemberId = (int)reader["MemberId"],
                LogText = reader["LogText"].ToString(),
                CreatedDate = (DateTime)reader["CreatedDate"]
            };

            if (reader["TicketId"] != DBNull.Value)
            {
                log.TicketId = (int)reader["TicketId"];
            }
            
            return log;
        }

        /// <summary>
        /// Returns a collection of Log objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Log> GetLogCollectionFromReader(IDataReader reader)
        {
            List<Log> m = new List<Log>();
            while (reader.Read())
                m.Add(GetLogFromReader(reader));
            return m;
        }
        
    }
}