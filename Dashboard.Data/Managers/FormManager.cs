﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using Dashboard.Common;
using Dashboard.Data.Data;
using Dashboard.Data.Providers;

namespace Dashboard.Data.Managers
{
    public abstract class FormManager : DataAccess
    {
        private static FormManager _instance;
        public static FormManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new FormProvider();
                }
                return _instance;
            }
        }

        #region FormTypes

        public abstract List<Form> GetFormTypes();

        protected virtual Form GetFormTypeFromReader(IDataReader reader)
        {
            Form form = new Form()
            {
                Id = (int)reader["Id"],
                Name = reader["Name"].ToString()
            };
            return form;
        }

        /// <summary>
        /// Returns a collection of FormType objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Form> GetFormTypeCollectionFromReader(IDataReader reader)
        {
            List<Form> forms = new List<Form>();
            while (reader.Read())
                forms.Add(GetFormFromReader(reader));
            return forms;
        } 

        #endregion

        #region Forms

        public abstract List<Form> GetFormsByType(int typeId);
        public abstract Form GetFormByFormId(int id);
        public abstract int CountForms();
        public abstract int InsertForm(Form form);
        public abstract bool UpdateForm(Form form);
        public abstract bool DeleteFormByFormId(int formId);

        protected virtual Form GetFormFromReader(IDataReader reader)
        {
            Form form = new Form()
            {
                Id = (int)reader["Id"],
                FormTypeId = (int)reader["FormTypeId"],
                Name = reader["Name"].ToString(),
                ImageUrl = reader["ImageUrl"].ToString()
            };

            return form;
        }

        /// <summary>
        /// Returns a collection of Form objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Form> GetFormCollectionFromReader(IDataReader reader)
        {
            List<Form> forms = new List<Form>();
            while (reader.Read())
                forms.Add(GetFormFromReader(reader));
            return forms;
        } 
        
        #endregion

        #region FormFields

        public abstract List<Form> GetFormFieldsByFormId(int formId);
        public abstract Form GetFormFieldById(int id);
        public abstract int CountFormFieldsByFormId(int formId);
        public abstract int InsertFormField(Form form);
        public abstract bool UpdateFormField(Form form);
        public abstract bool DeleteFormFieldById(int id);

        protected virtual Form GetFormFieldFromReader(IDataReader reader)
        {
            Form form = new Form()
            {
                Id = (int)reader["Id"],
                FormId = (int)reader["FormId"],
                Name = reader["Name"].ToString(),
                FontFamily = reader["FontFamily"].ToString(),
                Value = reader["Value"].ToString(),
                FontSize = (int)reader["FontSize"],
                FontColor = ColorTranslator.FromHtml(reader["FontColor"].ToString()),
                XPos = (int)reader["XPos"],
                YPos = (int)reader["YPos"]
            };

            FontStyle fontStyle = FontStyle.Regular;
            Enum.TryParse(reader["FontStyle"].ToString(), true, out fontStyle);
            form.FontStyle = fontStyle;

            StringFormatFlags stringFormat = StringFormatFlags.DisplayFormatControl;
            Enum.TryParse(reader["StringFormat"].ToString(), true, out stringFormat);
            form.StringFormat = stringFormat;

            return form;
        }

        /// <summary>
        /// Returns a collection of FormField objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Form> GetFormFieldCollectionFromReader(IDataReader reader)
        {
            List<Form> formItems = new List<Form>();
            while (reader.Read())
                formItems.Add(GetFormFieldFromReader(reader));
            return formItems;
        } 

        #endregion

    }
}