﻿using System;
using System.Collections.Generic;
using System.Data;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Common.Domain;
using Dashboard.Data.Data;
using Dashboard.Data.Providers;

namespace Dashboard.Data.Managers
{
    public abstract class TicketManager : DataAccess
    {
        private static TicketManager _instance;

        public static TicketManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TicketProvider();
                }
                return _instance;
            }
        }

        public abstract Ticket GetTicketByTicketId(int ticketId);
        public abstract List<Ticket> GetPagedTickets(int startRowIndex, int maximumRows, int memberId, int eventId);
        public abstract int CountTickets(int memberId, int eventId);

        /// <summary>
        /// Groups Tickets On Months By ActionId and By Year
        /// </summary>
        public abstract List<Group> GroupTicketsOnMonthsByActionId(int actionId, int year);

        /// <summary>
        /// Groups tickets average price on days
        /// </summary>
        public abstract List<Group> GroupTicketsOnDatesByProviderId(int providerId, int year);

        /// <summary>
        /// Gets Tickets By EventId
        /// </summary>
        public abstract List<Ticket> GetTicketsByEventId(int eventId);

        /// <summary>
        /// Groups tickets on months by providerId, actionId, eventId, statusId, and year.
        /// If StartDate and EndDate defined the selection only depends on a period.
        /// </summary>
        /// <param name="providerId">Provider Id or zero</param>
        /// <param name="actionId">Action Id or zero</param>
        /// <param name="eventId">EventId or zero</param>
        /// <param name="statusId">Status of tickets</param>
        /// <param name="year">Particular year is obligatory</param>
        /// <param name="startDate">StartDate of period otherwise MinValue</param>
        /// <param name="endDate">EndDate of period otherwise MinValue</param>
        /// <returns>List Group  as result</returns>
        public abstract List<Group> GroupTicketsOnDays(int providerId, int actionId, int eventId, int statusId, int year, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Groups tickets on days by providerId, actionId, eventId, statusId, and year.
        /// If StartDate and EndDate defined the selection only depends on a period.
        /// </summary>
        /// <param name="providerId">Provider Id or zero</param>
        /// <param name="actionId">Action Id or zero</param>
        /// <param name="eventId">EventId or zero</param>
        /// <param name="statusId">Status of tickets</param>
        /// <param name="year">Particular year is obligatory</param>
        /// <param name="startDate">StartDate of period otherwise MinValue</param>
        /// <param name="endDate">EndDate of period otherwise MinValue</param>
        /// <returns>List Group  as result</returns>
        public abstract List<Group> GroupTicketsOnMonths(int providerId, int actionId, int eventId, int statusId, int year, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Groups tickets on status by EventId
        /// </summary>
        /// <param name="eventId">EventId</param>
        /// <returns>List Groups</returns>
        public abstract List<Group> GroupTicketsOnStatusByEventId(int eventId);
        /// <summary>
        /// Groups ticket sales by weeks
        /// </summary>
        /// <param name="providerId">Ticket provider</param>
        /// <param name="statusId">Status of the ticket</param>
        /// <param name="year">Year number</param>
        /// <returns></returns>
        public abstract List<Group> RunningTotals(int providerId, int statusId, int year);
        /// <summary>
        /// Sums Sold Tickets on Count and Price
        /// </summary>
        public abstract Group SumSoldTickets(int providerId, int actionId, int statusId, int year);
        /// <summary>
        /// Get Ticket Statuses
        /// </summary>
        public abstract List<Group> GetTicketStatus();

        /// <summary>
        /// Inserts a new Ticket
        /// </summary>
        public abstract int InsertTicket(Ticket ticket);

        /// <summary>
        /// Updates a Ticket
        /// </summary>
        public abstract bool UpdateTicket(Ticket ticket);

        /// <summary>
        /// Deletes Ticket By TicketId
        /// </summary>
        public abstract bool DeleteTicketByTicketId(int ticketId);

        /// <summary>
        /// Delete Tickets By EventId
        /// </summary>
        public abstract bool DeleteTicketsByEventId(int eventId);

        protected virtual Ticket GetTicketFromReader(IDataReader reader)
        {
            Ticket ticket = new Ticket()
            {
                Id = (int)reader["Id"],
                EventId = (int)reader["EventId"],
                SeatId = (int)reader["SeatId"],
                ActionId = (int)reader["ActionId"],
                ActionDate = (DateTime)reader["ActionDate"],
                Price = (decimal)reader["Price"],
                RowNum = reader["RowNum"].ToString(),
                SeatNum = reader["SeatNum"].ToString(),
                StatusId = (ItemStatus)reader["StatusId"],
                ReservType = (int)reader["ReservType"],
                ReservAmount = (int)reader["ReservAmount"],
                ReservedTill = (DateTime)reader["ReservedTill"],
                ProhibitPrint = (int)reader["ProhibitPrint"],
                MultiStatus = (int)reader["MultiStatus"],
                CreatedDate = (DateTime)reader["CreatedDate"]
            };

            if (reader["SaleRuleId"] != DBNull.Value)
            {
                ticket.SaleRuleId = (int)reader["SaleRuleId"];
            }
            if (reader["BarCode"] != DBNull.Value)
            {
                ticket.BarCode = reader["BarCode"].ToString();
            }
            if (reader["ExternalBarCode"] != DBNull.Value)
            {
                ticket.ExternalBarCode = reader["ExternalBarCode"].ToString();
            }
            if (reader["ExternalActionTicketId"] != DBNull.Value)
            {
                ticket.ExternalActionTicketId = (int)reader["ExternalActionTicketId"];
            }
            if (reader["AliasId"] != DBNull.Value)
            {
                ticket.AliasId = Convert.ToInt64(reader["AliasId"]);
            }
            if (reader["TrackId"] != DBNull.Value)
            {
                ticket.TrackId = (int)reader["TrackId"];
            }
            return ticket;
        }

        /// <summary>
        /// Returns a collection of Ticket objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Ticket> GetTicketCollectionFromReader(IDataReader reader)
        {
            List<Ticket> m = new List<Ticket>();
            while (reader.Read())
                m.Add(GetTicketFromReader(reader));
            return m;
        }

    }
}