﻿using System;
using System.Collections.Generic;
using System.Data;
using Dashboard.Common;
using Dashboard.Data.Data;
using Dashboard.Data.Providers;

namespace Dashboard.Data.Managers
{
    public abstract class PaymentManager : DataAccess
    {
        private static PaymentManager _instance;

        public static PaymentManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PaymentProvider();
                }
                return _instance;
            }
        }

        public abstract Payment GetPaymentById(int id);
        public abstract List<Payment> GetPagedPayments(int startRowIndex, int maximumRows);
        public abstract int CountPayments();
        /// <summary>
        /// Inserts a new Payment
        /// </summary>
        public abstract int InsertPayment(Payment payment);

        /// <summary>
        /// Updates a Payment
        /// </summary>
        public abstract bool UpdatePayment(Payment payment);

        protected virtual Payment GetPaymentFromReader(IDataReader reader)
        {
            Payment payment = new Payment()
            {
                Id = (int) reader["Id"],
                MemberId = (int)reader["MemberId"],
                OrderId = (int)reader["OrderId"],
                InvoiceId = reader["InvoiceId"].ToString(),
                Amount = (decimal)reader["Amount"],
                Reason = reader["Reason"].ToString(),
                RequestDate = (DateTime)reader["RequestDate"]
            };

            if (reader["PaymentStatus"] != DBNull.Value)
            {
                payment.PaymentStatus = reader["PaymentStatus"].ToString();
            }
            if (reader["Error"] != DBNull.Value)
            {
                payment.Error = reader["Error"].ToString();
            }
            if (reader["TechMessage"] != DBNull.Value)
            {
                payment.TechMessage = reader["TechMessage"].ToString();
            }
            if (reader["ResponseDate"] != DBNull.Value)
            {
                payment.RequestDate = (DateTime)reader["ResponseDate"];
            }
            return payment;
        }

        /// <summary>
        /// Returns a collection of Payment objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Payment> GetPaymentCollectionFromReader(IDataReader reader)
        {
            List<Payment> m = new List<Payment>();
            while (reader.Read())
                m.Add(GetPaymentFromReader(reader));
            return m;
        }
        
    }
}