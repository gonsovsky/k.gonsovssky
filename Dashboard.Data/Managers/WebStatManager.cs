﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;
using Dashboard.Data.Providers;

namespace Dashboard.Data.Managers
{
    public abstract class WebStatManager : DataAccess
    {
        protected override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["StatServer"].ToString(); }
        }

        private static WebStatManager _instance;

        public static WebStatManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new WebStatProvider();
                }
                return _instance;
            }
        }

        #region Sessions

        public abstract List<WebStat> GetSessionsByDeviceType(DeviceType deviceType);
        

        /// <summary>
        /// Gets Sessions by single date
        /// </summary>
        public abstract List<WebStat> GetSessionsByDate(DateTime date);

        /// <summary>
        /// Gets Sessions by month ahead
        /// </summary>
        public abstract List<WebStat> GetSessionsByMonth(int month, int year);

        public abstract List<WebStat> GroupUserDevices();
        public abstract List<WebStat> GetBots();
        public abstract bool UpdateSession(WebStat stat);

        protected virtual WebStat GetSessionFromReader(IDataReader reader)
        {
            WebStat stat = new WebStat()
            {
                SessionId = (int)reader["Id"],
                CreatedDate = (DateTime)reader["CreatedDate"],
                IPAddress = reader["IPAddress"].ToString()
            };

            if (reader["UserAgent"] != DBNull.Value)
            {
                stat.UserAgent = reader["UserAgent"].ToString();
            }
            if (reader["BotId"] != DBNull.Value)
            {
                stat.BotId = (int)reader["BotId"];
            }
            if (reader["DeviceType"] != DBNull.Value)
            {
                stat.DeviceType = (DeviceType)reader["DeviceType"];
            }

            return stat;
        }

        /// <summary>
        /// Returns a collection of Session objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<WebStat> GetSessionCollectionFromReader(IDataReader reader)
        {
            List<WebStat> m = new List<WebStat>();
            while (reader.Read())
                m.Add(GetSessionFromReader(reader));
            return m;
        }

        #endregion

        #region Requests

        public abstract List<WebStat> GetPagedSales(int startRowIndex, int maximumRows);
        public abstract int CountSales();

        /// <summary>
        /// Gets Requests By Month ahead
        /// </summary>
        public abstract List<WebStat> GetRequestsByMonth(int month, int year);

        /// <summary>
        /// Gets Site By SiteId
        /// </summary>
        public abstract WebStat GetSiteBySiteId(int siteId);

        /// <summary>
        /// Gets Requests By SessionId
        /// </summary>
        public abstract List<WebStat> GetRequestsBySessionId(int sessionId);

        /// <summary>
        /// Gets Requests By Date
        /// </summary>
        public abstract List<WebStat> GetRequestsByDate(DateTime date, int sessionId);

        /// <summary>
        /// Counts Requests by date
        /// </summary>
        public abstract int CountRequestsByDate(DateTime date);

        public abstract int CountSessionsByDate(DateTime date);

        /// <summary>
        /// Gets Page By PageId
        /// </summary>
        public abstract WebStat GetPageByPageId(int pageId);

        /// <summary>
        /// Counts Requests by SessionId
        /// </summary>
        public abstract int CountRequestsBySessionId(int sessionId);

        /// <summary>
        /// Gets Available Years
        /// </summary>
        public abstract List<int> GetAvailableYears();

        /// <summary>
        /// Gets Available Months
        /// </summary>
        public abstract List<Group> GetAvailableMonths(int year);

        /// <summary>
        /// Groups By KeyWord
        /// </summary>
        public abstract List<WebStat> GroupKeyWordsByYear(int year);

        /// <summary>
        /// Groups KeyWords
        /// </summary>
        public abstract List<WebStat> GroupByKeyWord();

        /// <summary>
        /// Groups sessions by bot for year
        /// </summary>
        public abstract List<WebStat> GroupByBot(int year);

        /// <summary>
        /// Counts sessions by BotId for month
        /// </summary>
        public abstract int CountBotsByMonth(int botId, int month, int year);

        /// <summary>
        /// Groups sessions by visitor' IP addresses for every single month for a year
        /// </summary>
        public abstract List<Group> GroupByVisitors(int year);

        /// <summary>
        /// Groups total Sessions By Day
        /// </summary>
        public abstract List<WebStat> GroupSessionsByDay(int month, int year);

        /// <summary>
        /// Groups total Requests By Day
        /// </summary>
        public abstract List<WebStat> GroupRequestsByDay(int month, int year);

        /// <summary>
        /// Groups site members by month
        /// </summary>
        public abstract List<Group> GroupMembersByMonth(int year);

        /// <summary>
        /// Groups data By VisitorWebStatus
        /// </summary>
        public abstract List<WebStat> GroupByVisitorWebStatus(int year);

        /// <summary>
        /// Groups data By WaitTime
        /// </summary>
        public abstract List<WebStat> GroupByWaitTime(int year);

        /// <summary>
        /// Groups data By VisitType
        /// </summary>
        public abstract List<WebStat> GroupByVisitType(int year);

        /// <summary>
        /// Gets WebStat By WebStatId
        /// </summary>
        public abstract WebStat GetWebStatByWebStatId(int statId);

        /// <summary>
        /// Inserts a new WebStat
        /// </summary>
        public abstract int InsertWebStat(WebStat stat);

        /// <summary>
        /// Inserts a new WebStat
        /// </summary>
        public abstract bool UpdateWebStat(WebStat stat);

        /// <summary>
        /// Deletes KeyWord By KeyWord
        /// </summary>
        public abstract bool DeleteKeyWordByKeyWord(string keyWord);

        /// <summary>
        /// Deletes KeyWord By KeyWordId
        /// </summary>
        public abstract bool DeleteKeyWordByKeyWordId(int keywordId);

        /// <summary>
        /// Deletes WebStat By WebStatId
        /// </summary>
        public abstract bool DeleteWebStatByWebStatId(int statId);

        protected virtual WebStat GetRequestFromReader(IDataReader reader)
        {
            WebStat stat = new WebStat()
            {
                RequestId = (int) reader["Id"],
                SessionId = (int) reader["SessionId"],
                Uri = reader["Uri"].ToString(),
                HttpMethod = reader["HttpMethod"].ToString(),
                UrlReferrerHost = reader["UrlReferrerHost"].ToString(),
                CreatedDate = (DateTime) reader["CreatedDate"]
            };

            if (reader["UserName"] != DBNull.Value)
            {
                stat.UserName = reader["UserName"].ToString();
            }
            if (reader["InputData"] != DBNull.Value)
            {
                stat.InputData = reader["InputData"].ToString();
            }
            if (reader["OutputData"] != DBNull.Value)
            {
                stat.InputData = reader["OutputData"].ToString();
            }
            if (reader["UrlReferrer"] != DBNull.Value)
            {
                stat.UrlReferrer = reader["UrlReferrer"].ToString();
            }

            return stat;
        }

        /// <summary>
        /// Returns a collection of Request objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<WebStat> GetRequestCollectionFromReader(IDataReader reader)
        {
            List<WebStat> m = new List<WebStat>();
            while (reader.Read())
                m.Add(GetRequestFromReader(reader));
            return m;
        }

        #endregion
    }
}