﻿using System;
using System.Collections.Generic;
using System.Data;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;
using Dashboard.Data.Providers;

namespace Dashboard.Data.Managers
{
    public abstract class ShowActionManager : DataAccess
    {
        private static ShowActionManager _instance;

        public static ShowActionManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ShowActionProvider();
                }
                return _instance;
            }
        }

        public abstract List<Group> GroupShowActionsOnEventsByDate(DateTime date);
        public abstract ShowAction GetShowActionByActionId(int actionId);
        public abstract List<ShowAction> GetPagedShowActions(int startRowIndex, int maximumRows, int memberId, int providerId, int year);
        public abstract int CountShowActions(int memberId, int providerId, int year);
        public abstract List<int> GetAvailableYears();
        /// <summary>
        /// Gets ShowActions By ProviderId
        /// </summary>
        public abstract List<ShowAction> GetShowActionsByProviderId(int providerId, int year);
        /// <summary>
        /// Groups ShowActions On Tickets 
        /// </summary>
        public abstract List<ShowAction> GroupShowActionsOnTickets(int providerId, int statusId, int year);

        public abstract List<Group> GroupShowActionsOnEvents(int providerId, int statusId, int year);

        /// <summary>
        /// Retrieves ShowActions list with event count and ticket count.
        /// Note, the result is dependant on member's provider list.
        /// Groups ShowActions On Events By MemberId.
        /// </summary>
        /// <param name="providerId">Optional: use ProviderId = 0 to retrieve all the ShowActions</param>
        /// <param name="memberId">MemberId is obligatory</param>
        /// <param name="statusId">StatusId is obligatory</param>
        /// <param name="year">Year is obligatory</param>
        /// <param name="month">Month is obligatory</param>
        /// <returns>Returns grouped list of ShowActionId, Name, Events count, Ticket count</returns>
        public abstract List<Group> GroupShowActionsOnEventsByMemberId(int providerId, int memberId, int statusId, int year, int month);

        public abstract List<Group> GroupShowActionsOnMonthsByYear(int year);

        /// <summary>
        /// Groups ShowActions On Price 
        /// </summary>
        public abstract List<Group> GroupShowActionsOnPrice(int statusId, int year);

        protected virtual ShowAction GetShowActionFromReader(IDataReader reader)
        {
            ShowAction showAction = new ShowAction()
            {
                Id = (int)reader["Id"],
                ProviderId = (int)reader["ProviderId"],
                StartDate = (DateTime)reader["StartDate"],
                EndDate = (DateTime)reader["EndDate"],
                Name = reader["Name"].ToString(),
                Description = reader["Description"].ToString(),
                Info = reader["Info"].ToString(),
                AgeLimit = reader["AgeLimit"].ToString(),
                StatusId = (ItemStatus)reader["StatusId"],
                CreatedDate = (DateTime)reader["CreatedDate"]
            };

            if (reader["AliasId"] != DBNull.Value)
            {
                showAction.AliasId = (int)reader["AliasId"];
            }
            return showAction;
        }

        /// <summary>
        /// Returns a collection of ShowAction objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<ShowAction> GetShowActionCollectionFromReader(IDataReader reader)
        {
            List<ShowAction> m = new List<ShowAction>();
            while (reader.Read())
                m.Add(GetShowActionFromReader(reader));
            return m;
        }

    }
}