﻿using System;
using System.Collections.Generic;
using System.Data;
using Dashboard.Common;
using Dashboard.Common.Base;
using Dashboard.Data.Data;
using Dashboard.Data.Providers;

namespace Dashboard.Data.Managers
{
    public abstract class OrderManager : DataAccess
    {
        private static OrderManager _instance;
        public static OrderManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new OrderProvider();
                }
                return _instance;
            }
        }

        #region Orders

        public abstract Order GetOrderByOrderId(int orderId);
        public abstract List<Order> GetPagedOrders(int startRowIndex, int maximumRows);
        public abstract int CountOrders();

        /// <summary>
        /// Inserts a new Order
        /// </summary>
        public abstract int InsertOrder(Order order);

        /// <summary>
        /// Updates a Order
        /// </summary>
        public abstract bool UpdateOrder(Order order);

        /// <summary>
        /// Deletes Order with underlying OrderItems By OrderId 
        /// </summary>
        public abstract bool DeleteOrderByOrderId(int orderId);

        protected virtual Order GetOrderFromReader(IDataReader reader)
        {
            Order order = new Order()
            {
                Id = (int)reader["Id"],
                CreatedDate = (DateTime)reader["CreatedDate"]
            };

            if (reader.FieldCount>2)
            {
                if (reader["Count"] != DBNull.Value)
                {
                    order.Count = (int)reader["Count"];
                }
            }

            return order;
        }

        /// <summary>
        /// Returns a collection of Order objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Order> GetOrderCollectionFromReader(IDataReader reader)
        {
            List<Order> orders = new List<Order>();
            while (reader.Read())
                orders.Add(GetOrderFromReader(reader));
            return orders;
        } 
        
        #endregion

        #region OrderItems

        public abstract List<Order> GetOrderItemsByOrderId(int orderId);
        public abstract List<Order> GetPagedOrderItems(int startRowIndex, int maximumRows);
        public abstract List<Order> GroupOrderItemsOnDay(int providerId, int year);
        public abstract int CountOrderItems(int orderId);
        public abstract Order GetOrderItemByTicketId(int ticketId);

        /// <summary>
        /// Inserts a new OrderItem
        /// </summary>
        public abstract int InsertOrderItem(Order order);

        /// <summary>
        /// Updates an OrderItem
        /// </summary>
        public abstract bool UpdateOrderItem(Order order);

        /// <summary>
        /// Deletes OrderItems By TicketId
        /// </summary>
        public abstract bool DeleteOrderItemsByTicketId(int ticketId);

        protected virtual Order GetOrderItemFromReader(IDataReader reader)
        {
            Order orderItem = new Order()
            {
                Id = (int)reader["Id"],
                TicketId = (int)reader["TicketId"],
                MemberId = (int)reader["MemberId"],
                DecreaseDeposit = (decimal)reader["DecreaseDeposit"],
                SalerCommision = (decimal)reader["SalerCommision"],
                Price = (decimal)reader["Price"],
                CreatedDate = (DateTime)reader["CreatedDate"]
            };

            if (reader["BarCode"] != DBNull.Value)
            {
                orderItem.BarCode = reader["BarCode"].ToString();
            }

            if (reader["OrderId"] != DBNull.Value)
            {
                orderItem.OrderId = (int)reader["OrderId"];
            }

            if (reader["ControlDigit"] != DBNull.Value)
            {
                orderItem.ControlDigit = (int)reader["ControlDigit"];
            }

            if (reader["SaleComission"] != DBNull.Value)
            {
                orderItem.SaleComission = (decimal)reader["SaleComission"];
            }

            if (reader["BookComission"] != DBNull.Value)
            {
                orderItem.SaleComission = (decimal)reader["BookComission"];
            }

            if (reader["ExtraCharge"] != DBNull.Value)
            {
                orderItem.SaleComission = (decimal)reader["ExtraCharge"];
            }

            if (reader["ComissionPercent"] != DBNull.Value)
            {
                orderItem.ComissionPercent = (int)reader["ComissionPercent"];
            }

            if (reader["ExtraChargePercent"] != DBNull.Value)
            {
                orderItem.ExtraChargePercent = (int)reader["ExtraChargePercent"];
            }

            if (reader["FreeIssue"] != DBNull.Value)
            {
                orderItem.FreeIssue = (bool)reader["FreeIssue"];
            }

            if (reader["DiscountCardId"] != DBNull.Value)
            {
                orderItem.DiscountCardId = (int)reader["DiscountCardId"];
            }

            if (reader["Comission"] != DBNull.Value)
            {
                orderItem.Comission = (decimal)reader["Comission"];
            }

            if (reader["AliasId"] != DBNull.Value)
            {
                orderItem.AliasId = Convert.ToInt64(reader["AliasId"]);
            }
            if (reader["TrackId"] != DBNull.Value)
            {
                orderItem.TrackId = (int)reader["TrackId"];
            }
            return orderItem;
        }

        /// <summary>
        /// Returns a collection of OrderItems objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<Order> GetOrderItemCollectionFromReader(IDataReader reader)
        {
            List<Order> orderItems = new List<Order>();
            while (reader.Read())
                orderItems.Add(GetOrderItemFromReader(reader));
            return orderItems;
        } 

        #endregion

    }
}