﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Configuration;

namespace Dashboard.Http
{
    public abstract class DataManager
    {
        protected static string ConnectionString = ConfigurationManager.ConnectionStrings["StatServer"].ToString();
        protected static string UrlReferrer = WebConfigurationManager.AppSettings["UrlReferrer"];

        private static DataProvider _instance;

        public static DataProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DataProvider();
                }
                return _instance;
            }
        }

        internal abstract void InsertAsyncSession(HttpContext context, int sessionId, DateTime dt);
        internal abstract int InsertSession(HttpContext httpContext, int botId, DateTime dt);
        internal abstract List<Bot> GetBotList();
        internal abstract int InsertRequest(HttpContext context, int sessionId, DateTime dt);
        internal abstract bool UpdateRequest(int requestId, string outputData);
        internal abstract int LogBotActivity(int botId, DateTime dateTime);


    }
}