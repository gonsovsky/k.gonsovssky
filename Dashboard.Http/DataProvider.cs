﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

namespace Dashboard.Http
{
    public class DataProvider : DataManager
    {
        internal override List<Bot> GetBotList()
        {
            List<Bot> bots = new List<Bot>();
            using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Sessions_GetBots", sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlConnection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    bots.Add(new Bot((int)reader["Id"], reader["UserAgent"].ToString()));
                }
                reader.Close();
            }

            return bots;
        }

        /// <summary>
        /// Make sure Asynchronous Processing=true is defined in the connection string
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sessionId"></param>
        /// <param name="dt"></param>
        internal override void InsertAsyncSession(HttpContext context, int sessionId, DateTime dt)
        {
            return;
            HttpRequest request = context.Request;
            using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_InsertAsyncSession", sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                if (sessionId != 0)
                {
                    cmd.Parameters.AddWithValue("@SessionId", sessionId);
                }

                cmd.Parameters.AddWithValue("@IPAddress", context.Request.UserHostAddress);
                cmd.Parameters.AddWithValue("@UserAgent", context.Request.UserAgent);
                if (request.IsAuthenticated)
                {
                    string userName = context.User.Identity.Name;
                    cmd.Parameters.AddWithValue("@UserName", userName);
                }
                cmd.Parameters.AddWithValue("@Uri", context.Server.UrlDecode(request.Url.AbsoluteUri));

                var bytes = new byte[request.InputStream.Length];
                request.InputStream.Read(bytes, 0, bytes.Length);
                request.InputStream.Position = 0;
                string inputData = Encoding.UTF8.GetString(bytes);
                if (!string.IsNullOrEmpty(inputData))
                {
                    cmd.Parameters.Add("@InputData", SqlDbType.NVarChar).Value = inputData;
                }

                cmd.Parameters.Add("@HttpMethod", SqlDbType.NVarChar).Value = request.HttpMethod;

                if (request.UrlReferrer != null)
                {
                    string urlReferrer = context.Server.UrlDecode(request.UrlReferrer.ToString());
                    if (urlReferrer != null)
                    {
                        cmd.Parameters.AddWithValue("@UrlReferrer", urlReferrer);
                        cmd.Parameters.AddWithValue("@UrlReferrerHost", request.UrlReferrer.Host);
                    }
                }
                else
                {
                    cmd.Parameters.Add("@UrlReferrer", SqlDbType.NVarChar).Value = UrlReferrer;
                }
                cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = dt;

                sqlConnection.Open();
                IAsyncResult result = cmd.BeginExecuteNonQuery();
                cmd.EndExecuteNonQuery(result);
            }
        }

        /// <summary>
        /// Inserts a new session for the current request
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="botId">BotId if determinated else 0</param>
        /// <returns></returns>
        internal override int InsertSession(HttpContext httpContext, int botId, DateTime dt)
        {
            int sessionId = 0;
            using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Sessions_InsertSession", sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = dt;
                sqlCommand.Parameters.AddWithValue("@IPAddress", httpContext.Request.UserHostAddress);
                sqlCommand.Parameters.AddWithValue("@UserAgent", httpContext.Request.UserAgent);
                if (botId != 0)
                {
                    sqlCommand.Parameters.AddWithValue("@BotId", botId);
                }
                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sessionId = (int)sqlCommand.Parameters["@Id"].Value;
            }

            return sessionId;
        }
        
        internal override int InsertRequest(HttpContext context, int sessionId, DateTime dt)
        {
            int requestId = 0;
            HttpRequest request = context.Request;
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_InsertRequest", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SessionId", sessionId);

                if (request.IsAuthenticated)
                {
                    string userName = context.User.Identity.Name;
                    cmd.Parameters.AddWithValue("@UserName", userName);
                }
                
                cmd.Parameters.AddWithValue("@Uri", context.Server.UrlDecode(request.Url.AbsoluteUri));

                var bytes = new byte[request.InputStream.Length];
                request.InputStream.Read(bytes, 0, bytes.Length);
                request.InputStream.Position = 0;
                string inputData = Encoding.UTF8.GetString(bytes);
                if (!string.IsNullOrEmpty(inputData))
                {
                    cmd.Parameters.Add("@InputData", SqlDbType.NVarChar).Value = inputData;
                }

                cmd.Parameters.Add("@HttpMethod", SqlDbType.NVarChar).Value = request.HttpMethod;

                if (request.UrlReferrer != null)
                {
                    string urlReferrer = context.Server.UrlDecode(request.UrlReferrer.ToString());
                    if (urlReferrer != null)
                    {
                        cmd.Parameters.AddWithValue("@UrlReferrer", urlReferrer);
                        cmd.Parameters.AddWithValue("@UrlReferrerHost", request.UrlReferrer.Host);
                    }
                }
                else
                {
                    cmd.Parameters.Add("@UrlReferrer", SqlDbType.NVarChar).Value = UrlReferrer;
                }
                cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = dt;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                cmd.ExecuteNonQuery();
                requestId = (int)cmd.Parameters["@Id"].Value;
            }
            return requestId;
        }

        internal override bool UpdateRequest(int requestId, string outputData)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_UpdateRequest", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@RequestId", SqlDbType.Int).Value = requestId;
                
                if (outputData.Length > 256)
                {
                    outputData = outputData.Substring(0, 256);
                }

                cmd.Parameters.Add("@OutputData", SqlDbType.NVarChar).Value = outputData;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return ret != 0;
            }
        }

        internal override int LogBotActivity(int botId, DateTime dateTime)
        {
            int returnValue = 0;
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Sessions_LogBotActivity", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@BotId", SqlDbType.Int).Value = botId;
                cmd.Parameters.Add("@StartDay", SqlDbType.DateTime).Value = dateTime.Date;
                cmd.Parameters.Add("@SessionId", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                cmd.ExecuteNonQuery();
                returnValue = (int)cmd.Parameters["@SessionId"].Value;
            }
            return returnValue;
        }
    }
}