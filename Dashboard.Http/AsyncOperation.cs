﻿using System;
using System.Threading;
using System.Web;

namespace Dashboard.Http
{
    internal class AsyncOperation : IAsyncResult
    {
        private bool _completed;
        private readonly object _state;
        private readonly AsyncCallback _callback;
        private readonly HttpContext _context;

        bool IAsyncResult.IsCompleted
        {
            get { return _completed; }
        }

        WaitHandle IAsyncResult.AsyncWaitHandle
        {
            get { return null; }
        }

        object IAsyncResult.AsyncState
        {
            get { return _state; }
        }

        bool IAsyncResult.CompletedSynchronously
        {
            get { return false; }
        }

        internal AsyncOperation(AsyncCallback callback, HttpContext context, object state)
        {
            _callback = callback;
            _context = context;
            _state = state;
            _completed = false;
        }

        internal void StartAsyncTask()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(StartPushRequest), null);

            //if (_context.Request.Url.AbsoluteUri.Contains("aspx"))
            //{
            //    ThreadPool.QueueUserWorkItem(new WaitCallback(StartPushRequest), null);
            //}
            //else
            //{
            //    _completed = true;
            //    _callback(this);
            //}
        }

        private void StartPushRequest(object workItemState)
        {
            LogRequest logRequest = new LogRequest();
            logRequest.ProcessRequest(_context);
            _completed = true;
            _callback(this);
        }
    }
}