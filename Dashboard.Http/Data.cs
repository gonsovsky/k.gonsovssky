﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Dashboard.Http
{
    public class Data
    {
        #region Request

        internal static void InsertAsyncSession(HttpContext httpContext, int sessionId, DateTime dt)
        {
            DataManager.Instance.InsertAsyncSession(httpContext, sessionId, dt);
        }

        /// <summary>
        /// Inserts a new session for the current request
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="botId">BotId if determinated else 0</param>
        /// <returns></returns>
        internal static int InsertSession(HttpContext httpContext, int botId, DateTime dt)
        {
            return DataManager.Instance.InsertSession(httpContext, botId, dt);
        }

        internal static List<Bot> GetBotList()
        {
            List<Bot> bots = new List<Bot>();
            bots = DataManager.Instance.GetBotList();
            return bots;
        }

        internal static int InsertRequest(HttpContext context, int sessionId, DateTime dt)
        {
            return DataManager.Instance.InsertRequest(context, sessionId, dt);
        }

        internal static bool UpdateRequest(int requestId, string outputData)
        {
            Func<int, string, bool> updateAction = DataManager.Instance.UpdateRequest;
            IAsyncResult ar = updateAction.BeginInvoke(requestId, outputData, null, null);
            return updateAction.EndInvoke(ar);
        }

        internal static int LogBotActivity(int botId, DateTime dateTime)
        {
            return DataManager.Instance.LogBotActivity(botId, dateTime);
        }

        #endregion
    }
}