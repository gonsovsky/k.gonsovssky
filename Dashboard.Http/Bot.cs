﻿namespace Dashboard.Http
{
    internal class Bot
    {
        internal Bot()
        {

        }

        internal Bot(int botId, string userAgent)
        {
            BotId = botId;
            UserAgent = userAgent;
        }

        internal int BotId { get; set; }
        internal string UserAgent { get; set; }
    }
}