﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace Dashboard.Http
{
    internal class LogRequest
    {
        #region Variables

        private static List<Bot> bots = new List<Bot>();

        #endregion

        internal LogRequest()
        {
            //if (bots.Count == 0)
            //{
            //    bots = Data.GetBotList();
            //}
        }

        /// <summary>
        /// Define SessionId and insert request into database
        /// </summary>
        internal void ProcessRequest(HttpContext context)
        {
            DateTime dt = DateTime.Now;
            //int sessionId = ReadCookie(context);
            int sessionId = 0;// we assume the user might spend less than hour on the website
            Data.InsertAsyncSession(context, sessionId, dt);

            //if (sessionId == 0)
            //{
            //    sessionId = RegisterSession(context, dt);
            //    WriteCookie(sessionId, context, dt);
            //}

            //RegisterRequest(sessionId, context, dt);
        }

        /// <summary>
        /// Assign SessionId to a new session
        /// </summary>
        private int RegisterSession(HttpContext context, DateTime dt)
        {
            string userAgent = context.Request.UserAgent;
            int sessionId;
            int botId = 0;
            //At first we try to determine if the request from search engine bot
            //botId = (from bot in bots where userAgent == bot.UserAgent select bot.BotId).FirstOrDefault();

            //if so log bot activity for the current day
            //if (botId > 0)
            //{
            //    // determine if bot has already started its session
            //    sessionId = Data.LogBotActivity(botId, dt);
            //    // if bot has already started its session we just insert a new request and terminate
            //    if (sessionId > 0)
            //    {
            //        return sessionId;
            //    }
            //}
            //if the current request is not identified in sessions, insert a new session
            sessionId = Data.InsertSession(context, botId, dt);
            return sessionId;
        }

        private void RegisterRequest(int sessionId, HttpContext context, DateTime dt)
        {
            //int requestId = Data.InsertRequest(context, sessionId, dt);
            //string dateTime = DateTime.Now.ToString("O");
            //Data.UpdateRequest(requestId, dateTime);
        }

        /// <summary>
        /// Reads SessionId from HttpCookie 
        /// </summary>
        /// <param name="context">HttpContext.Current.Request</param>
        /// <returns>if not exists returns 0</returns>
        private int ReadCookie(HttpContext context)
        {
            int sessionId = 0;
            HttpCookie httpCookie = context.Request.Cookies["SessionId"];
            if (httpCookie != null)
            {
                if (Int32.TryParse(httpCookie.Value, out sessionId))
                {
                    return sessionId;
                }
            }
            return sessionId;
        }

        private void WriteCookie(int sessionId, HttpContext context, DateTime dt)
        {
            var sessionCookie = new HttpCookie("SessionId") { Value = sessionId.ToString() };
            int addHours = 24 - dt.Hour;
            sessionCookie.Expires = dt.AddHours(addHours).ToUniversalTime();
            context.Response.Cookies.Add(sessionCookie);
        }
    }
}