﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Dashboard.BLL;
using Dashboard.Common;

namespace Dashboard.Reports
{
    internal class ServiceDiscovery : Target
    {
        private DelegateTypeFactory _delegateTypeFactory;

        internal void Initialize(Dictionary<string, RestConfiguration> typeMappings)
        {
            _delegateTypeFactory = new DelegateTypeFactory();

            foreach (KeyValuePair<string, RestConfiguration> pair in typeMappings)
            {
                EnlistType(pair);
            }

            var d = TypeList;

            foreach (KeyValuePair<string, Type> keyValuePair in TypeList)
            {
                CreateInstance(keyValuePair);
            }

            var f = Methods;
        }

        private void EnlistType(KeyValuePair<string, RestConfiguration> pair)
        {
            Assembly[] sdfg = AppDomain.CurrentDomain.GetAssemblies();
            
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                string name = a.FullName;
                if (a.FullName.Contains(pair.Value.AssemblyName))
                {
                    Assembly assembly = Assembly.Load(a.FullName);
                    IEnumerable<Type> types = assembly.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IReport)));

                    foreach (Type type in types)
                    {
                        if (!TypeList.ContainsKey(type.Name))
                        {
                            TypeList.Add(type.Name, type);
                        }
                    }

                    break;
                }
            }
        }

        private void CreateInstance(KeyValuePair<string, Type> keyValuePair)
        {
            Type type = keyValuePair.Value;

            //instance = ((ObjectHandle)instance).Unwrap();
            MethodInfo[] methodArray =
                type.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            foreach (MethodInfo mi in methodArray)
            {
                Type fg = mi.ReturnType;
                //Object instance = Activator.CreateInstance(fg);
                var df = _delegateTypeFactory.CreateDelegateType(mi);
                Delegate del = Delegate.CreateDelegate(df, null, mi);
                string key = string.Format("{0}.{1}", keyValuePair.Key, mi.Name);
                //string key = mi.Name;

                Methods.Add(key, del);
                object[] attr = mi.GetCustomAttributes(typeof (RestAttribute), false);
                Attributes.Add(key, ConvertToRestAttribute(attr));

            }
        }

        private RestAttribute ConvertToRestAttribute(IEnumerable<object> attributes)
        {
            RestAttribute restAttribute = new RestAttribute();
            foreach (object attribute in attributes)
            {
                restAttribute = (RestAttribute)attribute;
            }
            return restAttribute;
        }


    }
}