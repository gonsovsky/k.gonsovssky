﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dashboard.Reports
{
    public class RestConfiguration
    {
        public string FullName { get; set; }
        public string ClassName { get; set; }
        public string FullQualifiedName { get; set; }
        public string AliasName { get; set; }
        private string assemblyName;
        private string assemblyNameForEvaluation;
        public string AssemblyName
        {
            get
            {
                return this.assemblyName;
            }
            set
            {
                this.assemblyName = value;
                if (value.Length == 0)
                    this.assemblyNameForEvaluation = string.Empty;
                else
                    this.assemblyNameForEvaluation = value + ",";
            }
        }

        /// <summary>
        /// Gets or sets boolean value indicated if "." character in namespace should be replaced with "/" path separator when url is formed.
        /// </summary>
        public bool ReplaceDotWithPathSeparator { get; set; }
    }
}
