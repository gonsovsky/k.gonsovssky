﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Dashboard.Reports
{
    public abstract class BaseReport
    {
        public static ReportProvider _instance;
        public static BaseReport Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ReportProvider();
                }

                return _instance;
            }
           
        }

        public abstract byte[] CreateReport(NameValueCollection nvc);
    }

}
