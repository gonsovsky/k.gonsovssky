﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;

namespace Dashboard.Reports
{
    internal class Settings : IConfigurationSectionHandler
    {
        public Dictionary<string, RestConfiguration> TypeMappings { get; private set; }
        public Dictionary<string, string> NamedReports { get; private set; }
        public string FolderPath { get; private set; }

        public static Settings Current
        {
            get
            {
                return ((Settings) ConfigurationManager.GetSection("reports"));
            }
        }

        private Settings()
        {
            var df = this;
        }

        private Settings(XmlElement xmlData)
        {
            FolderPath = xmlData.Attributes.GetNamedItem("folderPath").Value.Trim();
            NamedReports = new Dictionary<string, string>();
            XmlNodeList reportNodes = xmlData.SelectNodes("namedReports/add");
            if (reportNodes != null)
            {
                foreach (XmlNode node in reportNodes)
                {
                    string name = node.Attributes.GetNamedItem("name").Value.Trim();
                    string file = node.Attributes.GetNamedItem("file").Value.Trim();
                    NamedReports.Add(name, file);
                }
            }

            TypeMappings = new Dictionary<string, RestConfiguration>();
            XmlNodeList typeNodes = xmlData.SelectNodes("dependentAssembly/add");
            if (typeNodes != null)
            {
                foreach (XmlNode node in typeNodes)
                {
                    string type = node.Attributes.GetNamedItem("type").Value.Trim();
                    string alias = node.Attributes.GetNamedItem("alias").Value.Trim();
                    MapRestConfiguration(alias, type, true);
                }
            }

            ServiceDiscovery serviceDiscovery = new ServiceDiscovery();
            serviceDiscovery.Initialize(TypeMappings);
        }
        
        private void MapRestConfiguration(string alias, string type, bool replaceDotWithPathSeparator)
        {
            string[] types = type.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
            string assemblyName = string.Empty;
            string fullName = ExtractFullTypeName(type, out assemblyName);
            if (types.Length >= 2)
            {
                string[] names = types[0].Split(new []{'.'});

                RestConfiguration restConfiguration = new RestConfiguration()
                {
                    ClassName = names[names.Length - 1],
                    AliasName = alias,
                    AssemblyName = assemblyName.Substring(0, assemblyName.IndexOf(',')),
                    FullName = fullName,
                    FullQualifiedName = type,
                    ReplaceDotWithPathSeparator = replaceDotWithPathSeparator
                };
                TypeMappings.Add(alias, restConfiguration);
            }
            else
            {
                throw new RestException("ServiceType configuration is not valid");
            }
        }

        public static string ExtractFullTypeName(string fullQualifiedTypeName, out string assemblyName)
        {
            assemblyName = string.Empty;
            string fullName = string.Empty;
            int pos = fullQualifiedTypeName.IndexOf(",");
            if (pos > 0)
            {
                assemblyName = fullQualifiedTypeName.Substring(pos + 1).Trim();
                fullName = fullQualifiedTypeName.Substring(0, pos).Trim();
            }
            else fullName = fullQualifiedTypeName;
            return fullName;
        }

        public object Create(object parent, object configContext, XmlNode section)
        {
            XmlElement root = (XmlElement) section;
            return new Settings(root);
        }
    }
}
