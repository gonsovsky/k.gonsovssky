﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Runtime.Hosting;
using Dashboard.Common;
using System.Data;
using System.Web;
using NPOI.HSSF.UserModel;
using NPOI.OpenXml4Net.OPC;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.XWPF.UserModel;

namespace Dashboard.Reports
{
    public class ReportProvider : BaseReport
    {
        private Settings setting;
        public ReportProvider()
        {
            if (setting == null)
            {
                setting = Settings.Current;
            }
        }

        public override byte[] CreateReport(NameValueCollection nvc)
        {
            Parser parser = new Parser();
            Argument inputData = parser.ParseQueryString(nvc);
            object fg = Target.InvokeMethod(inputData);
            return BuildReport(fg, inputData.MethodName);
        }

        private byte[] BuildReport(object result, string name)
        {
            byte[] output = null;
            List<Group> months = result as List<Group>;
            if (months != null)
            {
                output = BuildGroupReport(months, name);
            }

            DataTable data = result as DataTable;
            if (data != null)
            {
                output = BuildTableReport(data, name);
            }

            return output;
        }

        private byte[] BuildTableReport(DataTable data, string name)
        {
            string folder = setting.FolderPath;
            string report = setting.NamedReports[name];
            string path = HttpContext.Current.Server.MapPath(string.Format("{0}/{1}", folder, report));
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);

            IWorkbook workbook = WorkbookFactory.Create(file);
            ISheet sheet1 = workbook.GetSheet("Билеты");
            sheet1.CreateRow(5).CreateCell(3).SetCellValue("Отчет по билетам");
            int indent = 6;
            int count = 0;
            decimal price = 0;
            for (int i = 0; i < data.Rows.Count; i++)
            {
                indent ++;
                IRow row = sheet1.CreateRow(indent);
                row.CreateCell(1).SetCellValue(((DateTime)data.Rows[i]["CreatedDate"]).ToString("M"));
                row.CreateCell(2).SetCellValue(data.Rows[i]["TotalActions"].ToString());
                row.CreateCell(3).SetCellValue(data.Rows[i]["TotalEvents"].ToString());
                int c = (int)data.Rows[i]["TotalTickets"];
                row.CreateCell(4).SetCellValue(c.ToString("### ### ### ###"));
                decimal p = (decimal) data.Rows[i]["Price"];
                row.CreateCell(5).SetCellValue(p.ToString("C0"));
                row.CreateCell(7).SetCellValue(((decimal)data.Rows[i]["AveragePrice"]).ToString("C0"));
                count += c;
                price += p;
            }
            IRow rw = sheet1.CreateRow(indent += 1);
            var style1 = workbook.CreateCellStyle();
            style1.BorderBottom = BorderStyle.Medium;
            style1.FillPattern = FillPattern.SolidForeground;
            style1.FillForegroundColor = IndexedColors.Green.Index;
            style1.FillPattern = FillPattern.LessDots;
            rw.CreateCell(1).CellStyle = style1;
            rw.CreateCell(2).CellStyle = style1;
            rw.CreateCell(3).CellStyle = style1;
            rw.CreateCell(4).CellStyle = style1;
            rw.CreateCell(5).CellStyle = style1;
            rw.CreateCell(6).CellStyle = style1;
            rw.CreateCell(7).CellStyle = style1;

            sheet1.CreateRow(indent += 1).CreateCell(1);
            sheet1.CreateRow(indent += 1).CreateCell(1).SetCellValue("Всего: " + count.ToString("### ### ### ###") + " билетов на общую сумму " + price.ToString("C0") + " рублей"); ;


            byte[] bytes;
            //using (MemoryStream memoryStream = new MemoryStream("test.xlsx", FileMode.Open, FileAccess.Read))
            using (MemoryStream memoryStream = new MemoryStream())
            {
                workbook.Write(memoryStream);
                bytes = memoryStream.ToArray();
                //memoryStream.Position = 0;
                //bytes = new byte[memoryStream.Length];
                //memoryStream.Read(bytes, 0, bytes.Length);
            }
            return bytes;
        }

        private byte[] BuildGroupReport(List<Group> months, string name)
        {

            IWorkbook workbook = new XSSFWorkbook();
            ISheet sheet1 = workbook.CreateSheet("Отчет");
            sheet1.CreateRow(0).CreateCell(3).SetCellValue("Отчет по билетам");
            sheet1.CreateRow(1);
            
            //int x = 1;
            int i;
            int count = 0;
            decimal price = 0;
            for (i = 0; i < months.Count; i++)
            {
                IRow row = sheet1.CreateRow(i+2);
                row.CreateCell(1).SetCellValue(months[i].Name);
                row.CreateCell(3).SetCellValue(months[i].Count.ToString("### ### ### ###"));
                row.CreateCell(5).SetCellValue(months[i].Price.ToString("C"));
                count += months[i].Count;
                price += months[i].Price;
            }
            sheet1.CreateRow(i + 3).CreateCell(1);

            sheet1.CreateRow(i + 4).CreateCell(0).SetCellValue("Всего: " + count.ToString("### ### ### ###") + " билетов за " + months.Count + " месяцев на общую сумму " + price.ToString("C") + " рублей"); ;


            byte[] bytes;
            //using (MemoryStream memoryStream = new MemoryStream("test.xlsx", FileMode.Open, FileAccess.Read))
            using (MemoryStream memoryStream = new MemoryStream())
            {
                workbook.Write(memoryStream);
                bytes = memoryStream.ToArray();
                //memoryStream.Position = 0;
                //bytes = new byte[memoryStream.Length];
                //memoryStream.Read(bytes, 0, bytes.Length);
            }
            return bytes;
        }
    }
}