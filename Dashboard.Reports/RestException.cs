﻿using System;
using System.Runtime.Serialization;

namespace Dashboard.Reports
{
    public class RestException : Exception
    {
        public RestException() : base("RestService process thrown an exception.")
        {
        }

        public RestException(string message) : base(message)
        {
        }

        public RestException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public RestException(string format, object arg0, Exception innerException)
            : this(string.Format(format, arg0), innerException)
        {
        }

        public RestException(string format, object arg0) : this(string.Format(format, arg0))
        {
        }

        public RestException(string format, object arg0, object arg1, Exception innerException)
            : this(string.Format(format, arg0, arg1), innerException)
        {
        }

        public RestException(string format, object arg0, object arg1) : this(string.Format(format, arg0, arg1))
        {
        }

        public RestException(string format, object arg0, object arg1, object arg2, Exception innerException)
            : this(string.Format(format, arg0, arg1, arg2), innerException)
        {
        }

        public RestException(string format, object arg0, object arg1, object arg2)
            : this(string.Format(format, arg0, arg1, arg2))
        {
        }

        protected RestException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}