﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Dashboard.Reports
{
    public class Parser
    {
        Dictionary<string, Func<int, byte[]>> methods = new Dictionary<string, Func<int, byte[]>>();
        public Parser()
        {
            methods.Add("GroupTicketsOnMonths", GroupTicketsOnMonths);
        }

        public Argument ParseQueryString(NameValueCollection nvc)
        {
            Argument argument = new Argument();
            argument.MethodName = nvc["type"];
            argument = Target.GetParams(argument);
            argument.ParamsObjects = ReadGetParameters(nvc, argument.Parameters);
            return argument;
        }

        private object[] ReadGetParameters(NameValueCollection nvc, ParameterInfo[] pi)
        {
            NameValueCollection requestParams = new NameValueCollection(nvc);
            requestParams.Remove("type");
            int piLength = pi.Length;
            if (requestParams.Count != piLength)
            {
                throw new RestException("Source parameters do not match the target signature");
            }

            List<object> pr = new List<object>();
            for (int i = 0; i < piLength; i++)
            {
                string name = pi[i].Name;
                pr.Add(Convert.ChangeType(requestParams[name], pi[i].ParameterType));
            }
            return pr.ToArray();
        }

        public byte[] GroupTicketsOnMonths(int year)
        {
            IWorkbook workbook = new XSSFWorkbook();
            ISheet sheet1 = workbook.CreateSheet("Отчет");
            sheet1.CreateRow(0).CreateCell(0).SetCellValue("This is a Sample");
            int x = 1;
            for (int i = 1; i <= 15; i++)
            {
                IRow row = sheet1.CreateRow(i);
                for (int j = 0; j < 15; j++)
                {
                    row.CreateCell(j).SetCellValue(x++);
                }
            }

            byte[] bytes;
            using (FileStream memoryStream = new FileStream("test.xlsx", FileMode.Open, FileAccess.Read))
            {
                workbook.Write(memoryStream);
                memoryStream.Position = 0;
                bytes = new byte[memoryStream.Length];
                memoryStream.Read(bytes, 0, bytes.Length);
            }
            return bytes;
        } 
    }
}