﻿using System;
using System.Collections.Generic;
using Dashboard.Common;

namespace Dashboard.Reports
{
    public abstract class Target
    {
        protected static readonly Dictionary<string, Delegate> Methods = new Dictionary<string, Delegate>();
        protected static readonly Dictionary<string, Type> TypeList = new Dictionary<string, Type>();
        protected static readonly Dictionary<string, RestAttribute> Attributes = new Dictionary<string, RestAttribute>();

        public static object InvokeMethod(Argument argument)
        {
            return Methods[argument.MethodName].DynamicInvoke(argument.ParamsObjects);
        }

        public static Argument GetParams(Argument argument)
        {
            argument.Parameters = Methods[argument.MethodName].Method.GetParameters();
            argument.ReturnType = Methods[argument.MethodName].Method.ReturnType;
            return argument;
        }

        public static Type GetReturnType(string method)
        {
            return Methods[method].Method.ReturnType;
        }

        //public static RestAttribute GetAttributes(string method)
        //{
        //    return Attributes[method];
        //}

        //private ServiceDiscovery _serviceDiscovery;

        //public static Target Current
        //{
        //    get
        //    {
        //        if (_serviceDiscovery == null)
        //        {
        //            throw new RestException("ServiceDiscovery is not initialized");
        //        }
        //        return _instance;
        //    }
        //}
    }
}